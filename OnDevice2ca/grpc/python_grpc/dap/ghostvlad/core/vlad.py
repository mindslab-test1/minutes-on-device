import torch
import torch.nn as nn
import torch.nn.functional as F


class VladPooling(nn.Module):
    def __init__(self, hp):
        super(VladPooling, self).__init__()
        self.hp = hp
        cluster = torch.empty(
            hp.model.cluster.K, hp.model.dim)
        nn.init.orthogonal_(cluster) # [K, D]
        cluster = cluster.view(1, cluster.size(0), cluster.size(1), 1, 1)
        self.cluster = nn.Parameter(cluster, requires_grad=True) # [1, K, D, 1, 1]

    def forward(self, x_fc, x_k_center):
        # x_fc: [B, D, H, W], D=512
        # x_k_center: [B, K+G, H, W]
        A = F.softmax(x_k_center, dim=1) # [B, K+G, H, W]
        A = A[:, :self.hp.model.cluster.K, :, :] # [B, K, H, W]
        A = A.unsqueeze(2) # [B, K, 1, H, W]
        feat_broadcast = x_fc.unsqueeze(1) # [B, 1, D, H, W]
        feat_res = feat_broadcast - self.cluster # [B, K, D, H, W]
        weighted_res = A * feat_res # [B, K, D, H, W]
        cluster_res = torch.sum(weighted_res, dim=[-2, -1]) # [B, K, D]
        cluster_res = cluster_res / torch.norm(cluster_res, p=2, dim=-1, keepdim=True) # [B, K, D]
        cluster_res = cluster_res.flatten(start_dim=1) # [B, K*D]
        return cluster_res
