import torch
import librosa
import numpy as np
from scipy.io.wavfile import read

from ..core.model import VGGSR
from ...coreutils.hparams import load_hparam_str
from ..core.logger import log_error

from ...dap_pb2_grpc import GhostVLADServicer
from ...dap_pb2 import Embedding, EmbConfig


def load_checkpoint(chkpt_path):
    checkpoint = torch.load(chkpt_path, map_location='cpu')
    model = checkpoint['model']
    hp_str = checkpoint['hp_str']
    hp = load_hparam_str(hp_str)

    return hp, model


class WavBinaryWrapper(object):
    def __init__(self, data):
        super(WavBinaryWrapper, self).__init__()
        self.data = data
        self.position = 0

    def read(self, n=-1):
        if n == -1:
            data = self.data[self.position:]
            self.position = len(self.data)
            return data
        else:
            data = self.data[self.position:self.position+n]
            self.position += n
            return data

    def seek(self, offset, whence=0):
        if whence == 0:
            self.position = offset
        elif whence == 1:
            self.position += offset
        elif whence == 2:
            self.position = len(self.data) + offset
        else:
            raise RuntimeError('test')
        return self.position

    def tell(self):
        return self.position

    def close(self):
        pass


class GhostVLADInference(GhostVLADServicer):
    def __init__(self, chkpt_path, args, logger):
        self.logger = logger
        self.logger.info("Loading model from checkpoint file %s" % chkpt_path)
        hp, state_dict = load_checkpoint(chkpt_path)
        self.hp = hp

        torch.cuda.set_device(args.device)
        self.device = args.device
        self.model = VGGSR(hp).cuda()
        self.model.load_state_dict(state_dict)
        self.model.eval()

        self.emb_config = EmbConfig(
            emb_dim=hp.model.dim
        )

        del hp
        del state_dict
        torch.cuda.empty_cache()

    @log_error
    def GetDvectorFromWav(self, wav_binary_iterator, context):
        torch.cuda.set_device(self.device)
        with torch.no_grad():
            wav = bytearray()
            for wav_binary in wav_binary_iterator:
                wav.extend(wav_binary.bin)
            wav = WavBinaryWrapper(wav)
            sampling_rate, wav = read(wav)
        
            if len(wav.shape) == 2:
                wav = wav[:, 0]
        
            if wav.dtype == np.int16:
                wav = wav / 32768.0
            elif wav.dtype == np.int32:
                wav = wav / 2147483648.0
            elif wav.dtype == np.uint8:
                wav = (wav - 128) / 128.0
        
            if sampling_rate != self.hp.audio.sr:
                wav = librosa.resample(wav, sampling_rate,
                                       self.hp.audio.sr)
                wav = np.clip(wav, -1.0, 1.0)

            wav = wav.astype(np.float32)
            wav = torch.from_numpy(wav).cuda()
            wav = wav.unsqueeze(0)
            result = self.model.inference(wav)[0]
            result = result.float().view(-1).cpu().detach().tolist()
            result = Embedding(data=result)
            return result

    @log_error
    def GetEmbConfig(self, empty, context):
        return self.emb_config
