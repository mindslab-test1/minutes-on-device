import torch
import librosa
import numpy as np


class Audio():
    def __init__(self, sr, n_fft, num_mels, hop_length, win_length):
        self.mel_basis = librosa.filters.mel(sr=sr, n_fft=n_fft, n_mels=num_mels)
        self.n_fft = n_fft
        self.hop_length = hop_length
        self.win_length = win_length

    def get_mel(self, y):
        y = self.stft(y)
        magnitudes = np.abs(y) ** 2
        mel = np.log10(np.dot(self.mel_basis, magnitudes) + 1e-6)
        mel = torch.from_numpy(mel)
        return mel

    def stft(self, y):
        return librosa.core.stft(y=y, n_fft=self.n_fft,
                                 hop_length=self.hop_length,
                                 win_length=self.win_length,
                                 window='hann')
