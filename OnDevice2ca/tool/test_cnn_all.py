#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import traceback
import argparse
import grpc
import time
import os

from maum.brain.stt import stt_pb2
from maum.brain.stt import stt_pb2_grpc
from maum.brain.w2l.w2l_pb2_grpc import SpeechToTextStub
from maum.brain.w2l.w2l_pb2 import Speech

def bytes_from_file(filename, chunksize=10000):
	with open(filename, "rb") as f:
		while True:
			chunk = f.read(chunksize)
			if chunk:
				speech = stt_pb2.Speech()
				speech.bin = chunk
				yield speech
			else:
				break

# DNN, LSTM
class DnnClient :
	def __init__(self, remote_addr, lang, model, samplerate) :
		self.channel = grpc.insecure_channel('{}'.format(remote_addr))
		self.metadata = {(b'in.lang', b'{}'.format(lang)),(b'in.model', '{}'.format(model)), (b'in.samplerate', '{}'.format(samplerate))}
		self.stub = stt_pb2_grpc.SpeechToTextServiceStub(self.channel)
		print("Use dnn [{}]" .format(remote_addr))
		print("meta[{}]" .format(self.metadata))

	def simple_recognize(self, filepath):
		result = self.stub.SimpleRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return result

	def detail_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segments

	def stream_recognize(self, filepath):
		segments = self.stub.StreamRecognize(bytes_from_file(filepath), metadata=self.metadata)
		return segments

	#def stream_recognize(self, pcm_binary):
	#	pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
	#	return self.stub.StreamRecognize(pcm_binary, metadata=self.metadata)

	def _generate_wav_binary_iterator(self, wav_binary):
		for idx in range(0, len(wav_binary), self.chunk_size):
			binary = wav_binary[idx:idx+self.chunk_size]
			yield Speech(bin=binary)


class W2lClient(object):
    def __init__(self, remote='10.122.64.44:15011', chunk_size=1024):
        channel = grpc.insecure_channel(remote)
        self.stub = SpeechToTextStub(channel)
        self.chunk_size = chunk_size

    def recognize(self, wav_binary):
        wav_binary = self._generate_wav_binary_iterator(wav_binary)
        return self.stub.Recognize(wav_binary)

    def stream_recognize(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.StreamRecognize(pcm_binary)

    def _generate_wav_binary_iterator(self, wav_binary):
        for idx in range(0, len(wav_binary), self.chunk_size):
            binary = wav_binary[idx:idx+self.chunk_size]
            yield Speech(bin=binary)


if __name__ == '__main__':
	parser = argparse.ArgumentParser(
		description='w2l client')
	parser.add_argument('-r', '--remote',
						nargs='?',
						dest='remote',
						help='grpc: ip:port',
						type=str,
						#default='127.0.0.1:15001') 
						default='10.122.64.203:15001') 
						#default='10.122.64.198:9801')
						#default='10.122.64.169:15001')
						#default='182.162.19.12:15001')
						#default='125.132.250.203:15001')
	parser.add_argument('-i', '--input',
						nargs='?',
						help='input wav file',
						type=str,
						required=True)
	args = parser.parse_args()

	client = W2lClient(args.remote) #CNN
	#client = DnnClient(args.remote, 'eng', 'airport', '16000') #LSTM
	try :
		for path, directory, files in os.walk(args.input):
			for filename in files:
				ext = os.path.splitext(filename)[-1]
				if ext == '.wav' :
					#########################################
					full_path = os.path.join(path, filename)
					result_path=full_path[:-4] +'.result'
				elif ext == ".txt" :
					continue
				else :
					tmp_path = os.path.join(path, filename)
					full_path=tmp_path.split('.')[0] + '_conv.wav'
					#command="echo 'Y' | ffmpeg -i " + filename + ' -vn -ac 1 -ar 8000 ' + output_wav + ' > /dev/null  2>&1'
					command="echo 'Y' | ffmpeg -i " + tmp_path + ' -vn -ac 1 -ar 8000 ' + full_path  + ' > /dev/null  2>&1'
					process = os.popen(command)
					result=process.read()
					#########################################
					#full_path = os.path.join(path, output_wav)
					result_path=tmp_path[:-4] +'.result'

				print("1. recognize [{}]".format(full_path))
				with open(full_path, 'rb') as rf:
					#result_path=full_path[:-4] +'.result'
					with open(result_path, 'w+') as f:
						wav_binary = rf.read()
						segment = client.stream_recognize(wav_binary)
						#print(segment)
						for seg in segment :
							#f.write("{} ~{} | {}\n".format(int(seg.start)/100, int(seg.end)/100, seg.txt.encode('utf-8')))
							#print("{} ~{} | {}".format(seg.start, seg.end, seg.txt.encode('utf-8')))
							print("{} ~{} | {}".format(seg.start, seg.end, seg.txt))
							f.write("{} ".format(seg.txt.encode('utf-8')),)
						#wav_binary = rf.read()
						#text = client.recognize(wav_binary)
						#f.write("{}".format(text.txt.encode('utf-8')))

	except Exception as e:
		print("thread: exception raise fail. <%s>", e)
		print(traceback.format_exc())

