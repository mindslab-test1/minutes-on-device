#! /usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys, time, traceback
import wave, contextlib
from datetime import datetime

#8K 기준 초당 패킷 수 = 16000byte
#8K 기준 1/100 초당 패킷 수 = 160byte
#16K 기준 초당 패킷 수 = 32000byte
#16K 기준 1/100 초당 패킷 수 = 320byte
#6000 == 1초
#6 == 1/1000초

def calculate_play_time(wav_file) :
	#REC_TIME(PLAY_TIME) 계산
	try :
		with contextlib.closing(wave.open(wav_file, 'r')) as f :
			frames = f.getnframes()
			rate = f.getframerate()
			duration = frames/float(rate)
		return duration
	except Exception as e:
		print("thread: exception raise fail. <%s>", e)
		print(traceback.format_exc())
		return -1

def time_to_bytesize(srate, msec) :
	#SampleRate * (BitDepth / 8) * ChannelCount * (msec/1000)
	#초당 패킷수 16000
	size = (int(srate)/1000) * (16 /8) * 1 * (int(msec))
	#size=msec * ((srate/1000 * 16 * 1)/8)
	return int(size)
	
def make_nonewav_bytes(srate, msec) :
	size=time_to_bytesize(srate, msec)
	x=bytearray(int(size))
	for i in range(len(x)) :
		x[i]=0xff
	return bytes(x), size

class pcm_record_control :
	def __init__ (self, log, srate, file_full_path, start_ts=None, restart_correction=0.3) :
		self.log=log
		#Server Time 기준
		if start_ts :
			self.server_start_ts=start_ts
		else :
			self.server_start_ts=time.time()

		self.server_last_ts=self.server_start_ts

		#RTP Sequence 기준
		self.start_seq = None
		self.last_seq = None
		
		# RTP로 전송된 Sequence 총합
		self.collect_seq_total = 0
		# 서버에서 수신하지 못한 RTP 총합 (실제 수신 seq 숫자 = collection_seq_total - missing_seq_total)
		self.missing_seq_total = 0
		# 묵음 시간 보정
		self.ts_correction=0
		self.restart_correction=restart_correction

		self.srate=srate
		self.file_full_path=file_full_path
		self.fd=None

	def __enter__(self) :
		self.OpenPCM(self.file_full_path)
		return self

	def __exit__(self, type, value, traceback) :
		self.ClosePCM()
		return self

	def OpenPCM(self, file_full_path) :
		if self.fd == None :
			self.fd=open(file_full_path, 'wb+')
		else :
			if self.log :
				old_path=self.file_full_path
				new_path=file_full_path
				self.log.info("Recording File Change [{}] -> [{}]".format(old_path, new_path))
			self.ClosePCM()
			self.fd=open(file_full_path, 'wb+')

	def WritePCM(self, audio_buf) :
		self.fd.write(audio_buf,)
		self.fd.flush()

	def ClosePCM(self) :
		if self.fd :
			cur_ts=time.time()
			str_cur_ts = datetime.fromtimestamp(cur_ts).strftime('%Y-%m-%d %H:%M:%S.%f')
			str_last_ts = datetime.fromtimestamp(self.server_last_ts).strftime('%Y-%m-%d %H:%M:%S.%f')
			restart_interval = int((cur_ts - self.server_last_ts) * 1000)
			#if self.start_seq == None :
			#	pass
			#else :
			#	self.WriteNoneWave(msec=restart_interval)
			self.WriteNoneWave(msec=restart_interval)

		self.fd.close()
		self.fd=None
		self.file_full_path=None

	def WriteNoneWave(self, msec)  :
		if self.fd == None :
			return

		#묵음 시간 보정
		self.ts_correction=self.ts_correction + msec/1000 + self.restart_correction

		x, xlen = make_nonewav_bytes(self.srate, msec + self.restart_correction * 1000)
		self.WritePCM(x,)

		self.log.debug("Write nonwav: {}(msec)".format(msec))
		return  x, xlen
		

	def WriteRTP(self, audio_buf, ts, seq) :
		cur_ts=time.time()

		#최초 접속
		if self.start_seq == None :

			str_start_ts=datetime.fromtimestamp(self.server_start_ts).strftime('%Y-%m-%d %H:%M:%S.%f')
			str_cur_ts=datetime.fromtimestamp(cur_ts).strftime('%Y-%m-%d %H:%M:%S.%f')

			#최초 회의 접속 Delay Time 셋팅
			start_interval = int((cur_ts - self.server_start_ts) * 1000)
			self.log.debug("Connecting [START:{} / CUR:{}] StartInterval:{}(ms)".format(str_start_ts, str_cur_ts, start_interval))

			# NoneWave 등록
			self.WriteNoneWave(msec=start_interval)

			#최초 회의 접속sequence 셋팅
			self.start_seq = seq
			self.last_seq= seq


		#단절 후 재접속
		elif seq > self.last_seq + 50 or seq < self.last_seq -50 :
			str_cur_ts = datetime.fromtimestamp(cur_ts).strftime('%Y-%m-%d %H:%M:%S.%f')
			str_last_ts = datetime.fromtimestamp(self.server_last_ts).strftime('%Y-%m-%d %H:%M:%S.%f')
			restart_interval = int((cur_ts - self.server_last_ts) * 1000)
			self.log.debug("cur_seq={} but lase_seq={} Interval={}(ms)(Check NoneWave Length)".format(seq,self.last_seq,restart_interval))
			# NoneWave 등록
			self.WriteNoneWave(msec=restart_interval)

			#이전에 저장한 sequence 카운트 를 collect_Seq에 등록
			self.collect_seq_total = self.collect_seq_total + (self.last_seq - self.start_seq)
			#연결 종료 후 재접속  sequence 셋팅
			self.start_seq = seq
			self.last_seq= seq


		else :
			if seq > self.last_seq :
				self.last_seq = seq 

		self.server_last_ts=cur_ts

		#SEQ MAX
		if seq == 0 :
			if self.last_seq == 65535 :
				self.collect_seq = self.collect_seq + (self.last_seq - self.start_seq)
				self.start_seq=seq
				self.last_seq=seq

		self.WritePCM(audio_buf)

		return audio_buf
