#!/usr/bin/python
# -*- coding: utf-8 -*-
#import sys, os, logging, time
#from common.dblib import DBLib
import traceback

# STT_META 테이블 STATUS 정의
META_STAT_START=0
META_STAT_STOP=1
META_STAT_DELETE=9
META_STAT_COMPLETE=10
META_STAT_WORKING=11
META_STAT_STOPING=12
META_STAT_EXPIRE=19
META_STAT_GENERAL_ERROR=20

# STT_META 테이블 TYPE 정의
META_TYPE_UPLOAD=0
META_TYPE_REALTIME=1

#[WL_DEVICE]
#DEVICE_STATUS_NONE = '0'
#DEVICE_STATUS_INIT = '1'
#DEVICE_STATUS_CONNECTING = '2'
#DEVICE_STATUS_CONNECTED = '3'
#DEVICE_STATUS_FAILED = '4'
#DEVICE_STATUS_RELEASE = '5'
SPLIT_STATUS_NONE = '0'
SPLIT_STATUS_INIT = '1'
SPLIT_STATUS_CONNECTING = '2'
SPLIT_STATUS_CONNECTED = '3'
SPLIT_STATUS_FAILED = '4'
SPLIT_STATUS_RELEASE = '5'

MEETINGROOM_STATUS_NONE = '0'
MEETINGROOM_STATUS_USED= '1'

MIC_STATUS_NONE = '0'
MIC_STATUS_USED= '1'

#[TEXT RANK]
TEXT_RANK_STATUS_NONE = '0'
TEXT_RANK_STATUS_ING = '1'
TEXT_RANK_STATUS_END = '2'
#[SUMMARIZE]
SUMMARIZE_STATUS_NONE = '0'
SUMMARIZE_STATUS_START= '1'
SUMMARIZE_STATUS_ING = '2'
SUMMARIZE_STATUS_END = '3'
SUMMARIZE_STATUS_ERROR= '4'


#[STT_META] Table

#select_meta_query= """
#	select STT_META_SER, MINUTES_USER_SER, MINUTES_SITE_SER, CREATE_USER, date_format(CREATE_TIME, '%Y%m%d%h%i%s'), 
#	UPDATE_USER, date_format(UPDATE_TIME, '%Y%m%d%h%i%s'), MINUTES_MEETINGROOM, MINUTES_MACHINE, MINUTES_ID, 
#	MINUTES_NAME, date_format(MINUTES_START_DATE, '%Y%m%d%h%i%s'), MINUTES_TOPIC, MINUTES_JOINED_MEM,MINUTES_JOINED_CNT, 
#	MINUTES_STATUS, REC_SRC_CD, SRC_FILE_PATH, DST_FILE_PATH, START_TIME, END_TIME, REC_TIME, MEMO, MINUTES_LANG 
#	from STT_META
#	"""
select_meta_query= "select * from STT_META "
select_result_query = "SELECT * from STT_RESULT "

def select_stt_meta_bymeta_ser(mysql, stt_meta_ser) :
	query=select_meta_query + " where STT_META_SER={};".format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_meta_bystatus(mysql, status) :
	query=select_meta_query + " where MINUTES_STATUS='{}';".format(status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_meta_bystatus_with_textrank(mysql, realtime_status, textrank_status) :
	query = select_meta_query + " where MINUTES_STATUS='{}' and TEXT_RANK_STATUS='{}'".format(realtime_status, textrank_status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_meta_bystatus_with_summarize(mysql, status, summarize_status) :
	query = select_meta_query + " where MINUTES_STATUS='{}' and SUMMARIZE_STATUS='{}'".format(status, summarize_status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_result_by_talker(mysql, stt_meta_ser, attendee) :
	query = select_result_query + " where STT_META_SER='{}' and MINUTES_EMPLOYEE='{}'".format(stt_meta_ser, attendee)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def update_stt_meta_textrank_status(mysql, stt_meta_ser, textrank_status) :
	query = "update STT_META set TEXT_RANK_STATUS='{}' where STT_META_SER='{}';".format(textrank_status, stt_meta_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error
def update_stt_meta_summarize_status(mysql, stt_meta_ser, summarize_status) :
	query = "update STT_META set SUMMARIZE_STATUS='{}' where STT_META_SER='{}';".format(summarize_status, stt_meta_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def insert_textrank_keyword(mysql, stt_meta_ser, attendee, rank) :

	basic = "insert into MINUTES_TEXT_RANK (STT_META_SER, CREATE_USER, CREATE_TIME, ATTENDEE_NAME"
	static = ") values ('{}', 'mtrd', now(), '{}'".format(stt_meta_ser, attendee)

	if (len(rank) > 0) :
		index = 0
		for keyword in rank :
			index = index + 1
			if keyword is not None :
				basic = basic + ', TEXT_RANK{}'.format(index)
				static = static + ", '{}'".format(keyword)
			else :
				pass
		query = basic + static + ');'
	elif (len(rank) == 0):
		query = "insert into MINUTES_TEXT_RANK (STT_META_SER, CREATE_USER, CREATE_TIME, ATTENDEE_NAME, TEXT_RANK1, TEXT_RANK2, TEXT_RANK3, TEXT_RANK4, TEXT_RANK5) values ('{}', 'mtrd', now(), '{}', '', '', '', '', '')".format(stt_meta_ser, attendee)

	result, error = mysql.execute(query, False)
	if result == True :
		return True, None
	else :
		return False, error

def insert_summarize(mysql, stt_meta_ser, summarize, keywords) :
	if len(keywords) > 1000 :
		keywords=keywords[:1000]

	query= """
	insert into MINUTES_SUMMARIZE values ({}, 'MTSd',now(), NULL, NULL,  "{}", "{}" )
	""" .format(stt_meta_ser, keywords, summarize)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error


def update_summarize(mysql, stt_meta_ser, summarize) :

	query= """
	update MINUTES_SUMMARIZE set SUMMARIZE="{}", UPDATE_USER='MTSd', UPDATE_TIME=now() where STT_META_SER={};
	""" .format(summarize, stt_meta_ser)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error



def update_stt_meta_status(mysql, stt_meta_ser, status) :
	query =" update STT_META set MINUTES_STATUS={} where STT_META_SER={};" .format(status, stt_meta_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def update_stt_meta_complete(mysql, stt_meta_ser, rec_time, dst_file_path, status) :

	query= """
	update STT_META set MINUTES_STATUS={}, REC_TIME={}, DST_FILE_PATH="{}" where STT_META_SER={};
	""" .format(status, rec_time, dst_file_path, stt_meta_ser)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def update_stt_meta_complete_realtime(mysql, stt_meta_ser, start, end, rec_time, dst_file_path, status) :

	query= """
	update STT_META set MINUTES_STATUS={}, START_TIME='{}', END_TIME='{}', REC_TIME={}, SRC_FILE_PATH="{}", DST_FILE_PATH="{}" 
	where STT_META_SER={};
	""" .format(status, start, end, rec_time, dst_file_path, dst_file_path, stt_meta_ser)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error


#[MINUTES_COMMON] Table
def select_minutes_common(mysql) :
	query= " select * from MINUTES_COMMON;"
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None


#[MINUTES_SITE] Table
def select_minutes_team(mysql, team_ser) :
	query= " select * from MINUTES_TEAM where MINUTES_TEAM_SER={};".format(team_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

#[STT_MODEL] Table
def select_stt_model(mysql, model_ser) :
	query= " select * from STT_MODEL where STT_MODEL_SER={}".format(model_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_model_all(mysql) :
	query= """ select * from STT_MODEL;"""
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def update_mic_split_status(mysql, mic_ser, status) :
	query= """ update MINUTES_MIC set VIRTUAL_SPLIT_CONNECTION_STATUS='{}' where MINUTES_MIC_SER='{}'
	""".format(status, mic_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def select_minutes_meetingroom(mysql, meetingroom_ser) :
	query= """ select * from MINUTES_MEETINGROOM where MINUTES_MEETINGROOM_SER='{}';
	""".format(status, meetingroom_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

	
def update_minutes_meetingroom_status(mysql, meetingroom_ser, status) :
	query= """ update MINUTES_MEETINGROOM set MEETINGROOM_STATUS='{}' where MINUTES_MEETINGROOM_SER='{}'
	""".format(status, meetingroom_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def update_minutes_mic_status(mysql, mic_ser, status) :
	query= """ update MINUTES_MIC set MIC_STATUS='{}' where MINUTES_MIC_SER ='{}'
	""".format(status, mic_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error



def select_replace(mysql) :
	query= """ select * from MINUTES_ALL_REPLACE"""
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None


#[STT_RESULT] Table
def insert_stt_result(mysql, stt_meta_ser, vr_mic_flag, mic_ser, vr_mic_ser, mic_id, stt_org_result, str_stt_start, str_stt_end) :
    
    stt_org_result=stt_org_result.replace("'","''")
    if vr_mic_flag == None or vr_mic_flag == 'None':
        vr_mic_flag = 'NULL'
    if mic_ser == None or mic_ser == 'None' :
        mic_ser = 'NULL'
    if vr_mic_ser == None or vr_mic_ser == 'None' :
        vr_mic_ser = 'NULL'

    query= """insert into STT_RESULT (STT_META_SER, CREATE_USER, CREATE_TIME, VIRTUAL_MIC_FLAG, MINUTES_MIC_SER, 
    MINUTES_VIRTUAL_MIC_SER, MINUTES_EMPLOYEE, STT_ORG_RESULT, STT_RESULT_START, STT_RESULT_END) 
    values ({}, '{}', now(), {}, {}, {}, '{}', '{}', '{}','{}')
    """ .format(stt_meta_ser, 'MIPd', vr_mic_flag, mic_ser, vr_mic_ser, mic_id, str(stt_org_result), str_stt_start, str_stt_end)
    #print(query)

    result, error = mysql.execute(query, False)
    if result == True :
        return True, None
    else :
        return False, error


def select_stt_result(mysql, stt_meta_ser) :
	query= " select STT_ORG_RESULT from STT_RESULT where STT_META_SER={} order by STT_RESULT_START; " .format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_result_bymeta(mysql, stt_meta_ser) :
	query= " select * from STT_RESULT where STT_META_SER={} order by STT_RESULT_START; " .format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

# [MINUTES_MIC]
def select_minutes_mic_bysplit_status(mysql, split_status) :
	query= """ select * from MINUTES_MIC where VIRTUAL_MIC_FLAG='1' and VIRTUAL_SPLIT_CONNECTION_STATUS = '{}'
	""".format(split_status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_mic(mysql, mic_ser) :
	query= """ select * from MINUTES_MIC where MINUTES_MIC_SER='{}'
	""".format(mic_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_virtual_mic(mysql, mic_ser) :
	query= """ select * from MINUTES_VIRTUAL_MIC where MINUTES_MIC_SER='{}'
	""".format(mic_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_mic_bygroup(mysql, mic_group) :
	query= """ select * from MINUTES_MIC where MIC_GROUP='{}'
	""".format(mic_group)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_tmp_mic_byid(mysql, minutes_id) :
	query= """ select * from MINUTES_TEMP_MIC where MINUTES_ID ='{}'
	""".format(minutes_id)
	rowcnt, rows, error = mysql.execute_query(query)

	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None


def delete_tmp_mic_byid(mysql, minutes_id) :
	query= """ delete from MINUTES_TEMP_MIC where MINUTES_ID ='{}'
	""".format(minutes_id)
	result, error = mysql.execute(query, True)
	#rowcnt, rows, error = mysql.execute_query(query)

	return result, error

def delete_stt_result_bymeta(mysql, stt_meta_ser) :
	query= """ delete from STT_RESULT where STT_META_SER='{}'
	""".format(stt_meta_ser)
	result, error = mysql.execute(query, True)
	#rowcnt, rows, error = mysql.execute_query(query)

	return result, error

def delete_stt_meta_bymeta(mysql, stt_meta_ser) :
	query= """ delete from STT_META where STT_META_SER='{}'
	""".format(stt_meta_ser)
	result, error = mysql.execute(query, True)
	#rowcnt, rows, error = mysql.execute_query(query)

	return result, error

def delete_stt_meta_bk_bymeta(mysql, stt_meta_ser) :
	query= """ delete from STT_META_BK where STT_META_SER='{}'
	""".format(stt_meta_ser)
	result, error = mysql.execute(query, True)
	#rowcnt, rows, error = mysql.execute_query(query)

	return result, error

def delete_summarize(mysql, stt_meta_ser) :
	query= """ delete from MINUTES_SUMMARIZE where STT_META_SER={}
	""".format(stt_meta_ser)
	result, error = mysql.execute(query, True)

	return result, error

def select_summarize(mysql, stt_meta_ser) :
	query= "select * from MINUTES_SUMMARIZE where STT_META_SER={}".format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None




def get_mic_list_from_meta(mysql, stt_meta) :
	mic_info_list=list()

	#mic_list_str=stt_meta['MINUTES_JOINED_MEM']
	mic_list_str=stt_meta['MINUTES_JOINED_MIC_MEM']

	mic_list=mic_list_str.split(",")

	if len(mic_list) == 0 :
		#log.critical("mic_list is None")
		return False, None

	for mic_ser in mic_list :
		result, mic_rowcnt, mic_rows, error = select_minutes_mic(mysql, mic_ser)
		if result == False :
			#log.critical("MIC SELECT FAILURE[{}] : {}".format(mic_ser, error))
			continue
		elif mic_rowcnt == 0 :
			#log.critical("MIC SELECT Not Found[{}]" .format(mic_ser))
			continue

		mic_info_list.append(mic_rows[0])

	return True, mic_info_list

def make_mic_info_str(mysql, stt_meta) :
	mic_info_str=''
	result, mic_info_list = get_mic_list_from_meta(mysql, stt_meta)
	if result == False :
		return mic_info_str

	for mic_info in mic_info_list :
		MIC_PORT=mic_info['STT_DST_PORT']
		MIC_NAME=''

		#VR_MIC
		if mic_info['VIRTUAL_MIC_FLAG'] == '1' :
			mic_ser=mic_info['MINUTES_MIC_SER']
			res, rowcnt, rows, error = select_minutes_virtual_mic(mysql, mic_ser)
			if res == False :
				log.critical("[{}]:Select VIRTUAL_MIC FAILURE [{}/{}] ".format(stt_meta_ser, mic_ser, error))
				if len(mic_info_str) > 0 :
					mic_info_str=mic_info_str +','

				# Select VR MIC 실패시 기존 마이크 이름만 셋팅하고 넘어가도록
				MIC_NAME=mic_info['MIC_NAME']
				mic_info_str= mic_info_str + str(MIC_PORT) + ":" + str(MIC_NAME) 

			else :
				for row in rows :
					if len(mic_info_str) > 0 :
						mic_info_str=mic_info_str +','

					if row['VIRTUAL_TMP_MIC_NAME'] :
						MIC_NAME=row['VIRTUAL_TMP_MIC_NAME']
					else :
						MIC_NAME=row['VIRTUAL_MIC_NAME']

					mic_info_str= mic_info_str + str(MIC_PORT) + ":" + str(MIC_NAME) 
		#REAL_MIC
		else :
			if len(mic_info_str) > 0 :
				mic_info_str=mic_info_str +','

			if mic_info['TMP_MIC_NAME'] :
				MIC_NAME=mic_info['TMP_MIC_NAME']
			else :
				MIC_NAME=mic_info['MIC_NAME']

			mic_info_str= mic_info_str + str(MIC_PORT) + ":" + str(MIC_NAME) 

	return mic_info_str

