#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import re
import common.logger as logger
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

CHG_FILE_SYNC_MIN_PARAMETER = 1
CHG_FILE_SYNC_MAX_PARAMETER = 6

param_list = ['USE_FLAG', 'SRC_IP', 'SRC_PORT', 'SRC_USER', 'SRC_PASSWD', 'SYNC_INTERVAL']
mandatory_list = []

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-FILE-SYNC (USE_FLAG=a), (SRC_IP=b), (SRC_PORT=c), (SRC_USER=d), (SRC_PASSWD=e), (SYNC_INTERVAL=f)

 [Parameter]
  a = ENUM      (ON|OFF)
  b = IPADDR    (1:15)
  c = INTEGER   (1025:65536)
  d = STRING    (1:100)
  e = STRING    (1:100)
  f = INTEGER   (1:86400)

 [Usage]
  CHG-FILE-SYNC [a, b, c, d, e, f]
   ex) CHG-FILE-SYNC ON, 127.0.0.1, 2222, minds, msl1234~, 3600
  CHG-HA-INFO (USE_FLAG=a), (SRC_IP=b), (SRC_PORT=c), (SRC_USER=d), (SRC_PASSWD=e), (SYNC_INTERVAL=f)
   ex) CHG-FILE-SYNC USE_FLAG=ON, SRC_IP=127.0.0.1, SRC_PORT=2222, SRC_USER=minds, SRC_PASSWD=msl1234~, SYNC_INTERVAL=3600

 [Section Options Configuration]
   use_flag             : Whether to use a HA 
   src_ip               : remote server ip (from)
   src_port             : remote server port
   src_user             : remote server username
   src_passwd           : remote server user password
   sync_interval        : file sync interval (sec)


 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-FILE-SYNC
  Result = SUCCESS
  ===================================================
   use_flag          = before -> after
   src_ip            = before -> after
   src_port          = before -> after
   src_user          = before -> after
   src_passwd        = before -> after
   sync_interval     = before -> after 
  ===================================================

  <FAILURE>
  Date time
  MMC = CHG-FILE-SYNC
  Result = FAILURE
  ===================================================
  Reason = Reason for error
  ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	##### Optional Parameter #####
	if arg_data['USE_FLAG'] != None :
		ret, reason = MMCLib.check_Enum(flag_list, 'USE_FLAG', arg_data['USE_FLAG'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['SRC_IP'] != None :
		ret, reason = MMCLib.check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SRC_IP', arg_data['SRC_IP'], G_log)
		if (ret == False) : return False, reason

		ret, reason = MMCLib.check_Ip_Address('SRC_IP', arg_data['SRC_IP'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SRC_PORT'] != None :
		ret, reason = MMCLib.check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'SRC_PORT', arg_data['SRC_PORT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SRC_USER'] != None :
		ret, reason = MMCLib.check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SRC_USER', arg_data['SRC_USER'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SRC_PASSWD'] != None :
		ret, reason = MMCLib.check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SRC_PASSWD', arg_data['SRC_PASSWD'], G_log)
		if (ret == False) : return False, reason

	if arg_data['SYNC_INTERVAL'] != None :
		ret, reason = MMCLib.check_Decimal_And_Range(MIN_INTERVAL_VALUE, MAX_ADAY_INTERVAL_VALUE, 'SYNC_INTERVAL', arg_data['SYNC_INTERVAL'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, CHG_FILE_SYNC_MAX_PARAMETER, CHG_FILE_SYNC_MIN_PARAMETER)
			if (ret == False) :
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')
			
			### get old data from simd config file (/srv/maum/etc/simd.conf) ###
			org_data = OrderedDict()
			org_item_value = simd_config.items('FILE_SYNC')
			for i in range(len(org_item_value)):
				org_data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(org_data))
			
			### change config file data
			first = 0
			for item in Parsing_Dict :
				if Parsing_Dict[item] != None :
					value = simd_config.set('FILE_SYNC', item.lower(), Parsing_Dict[item])
					if value == False :
						reason = 'Change Config File Failure. [{}]'.format(item)
						return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)
						# make response message
						if first is 0:
							total_body = total_body + ' {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
							first +=1
						else:
							total_body = total_body + '\n {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
				else :
					continue

			G_log.info('CHG-FILE-SYNC Complete!!')
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-FILE-SYNC(), NoSectionError : [{}]".format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-FILE-SYNC(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('CHG-FILE-SYNC(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(G_simd_cfg_path)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_FILE-SYNC(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

