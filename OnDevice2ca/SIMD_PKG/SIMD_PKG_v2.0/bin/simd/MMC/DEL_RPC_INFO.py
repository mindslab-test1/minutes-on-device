#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DEL_RPC_INFO_MIN_PARAMETER = 1
DEL_RPC_INFO_MAX_PARAMETER = 1

param_list = ['RPC_NAME']
mandatory_list = ['RPC_NAME']

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  DEL-RPC-INFO [RPC_NAME=a]

 [Parameter]
  a = STRING    (1:100)

 [Usage]
  ADD-RPC-INFO [a]
  ex) ADD-RPC-INFO STT-RESTART
  ADD-RPC-INFO [RPC_NAME=a]
  ex) ADD-RPC-INFO [RPC_NAME=STT-RESTART]
   
 [Section Options Configuration]
  RPC_NAME             : Remote Process Command Name

 [Result]
  <SUCCESS>
  Date time
  MMC = DEL-RPC-INFO
  Result = SUCCESS
  =============================================
   RPC NAME    = COMMAND 
  =============================================

  <FAILURE>
  Date time
  MMC = DEL-RPC-INFO
  Result = FAILURE
  =============================================
  Reason = Reason for error
  =============================================

"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item]) == None:
			return False, "'{}' is Manddatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['RPC_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'RPC_NAME', arg_data['RPC_NAME'], G_log)
		if (ret == False) : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body = ''
	
	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/rpc_info.conf)
			rpc_config = ConfigParser.RawConfigParser()
			rpc_config.read(G_rpc_cfg_path)

			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DEL_RPC_INFO_MAX_PARAMETER, DEL_RPC_INFO_MIN_PARAMETER)
			if ret == False:
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if(ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if(ret == False):
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get data from '/home/minds/MP/etc/rpc_info.conf' ###
			data = OrderedDict()
			org_item_value = rpc_config.items('RPC_COMMAND')
			exist = False
			for i in range(len(org_item_value)) :
				if Parsing_Dict['RPC_NAME'].lower() == org_item_value[i][0]:
					exist = True
				data[org_item_value[i][0]] = org_item_value[i][1]
				if i is len(org_item_value)-1 and exist is False:
					reason = '[{}] is not Exist.'.format(Parsing_Dict['RPC_NAME'].lower())
					return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
			G_log.info('conf_data = {}'.format(data))

			## del new rpc info
			value = rpc_config.remove_option('RPC_COMMAND', Parsing_Dict['RPC_NAME'].lower())
			if value == False :
				reason = 'Delete Config File Failure. [{}]'.format(item)
				return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
			else :
				with open(G_rpc_cfg_path, 'w') as configfile :
					rpc_config.write(configfile)

			row = ''
			row = ' {0:20} = {1}'.format(Parsing_Dict['RPC_NAME'], data[Parsing_Dict['RPC_NAME'].lower()])
			total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('DEL_RPC_INFO() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-RPC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-RPC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error('DEL_RPC_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL_RPC_INFO(), Config_Read error [{}]'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL_RPC_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL_RPC_INFO(), [{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


