echo '############################################################'
echo '                          install SIMd                      '
echo '############################################################'

echo ' python module installing....'
echo 'y' | sudo yum install python-devel
echo 'y' | sudo pip install --upgrade pip "setuptools<45"
echo 'y' | sudo pip install zmq 
sleep 1
echo 'y' | sudo pip install tornado
sleep 1
echo 'y' | sudo pip install multiprocessing_logging
sleep 1
echo 'y' | sudo pip install websocket_client
sleep 1
echo 'y' | sudo pip install pymysql
sleep 1
echo 'y' | sudo pip install cx_Oracle
sleep 1
echo 'y' | sudo pip install psutil
sleep 1
echo 'y' | sudo pip install gpustat
sleep 1
echo 'y' | sudo pip install bcrypt
sleep 1
echo 'y' | sudo pip install Flask
sleep 1
echo 'y' | sudo pip install pysftp
sleep 1
echo 'y' | sudo pip install paramiko
sleep 1
echo 'y' | sudo pip install schedule
sleep 1
echo ' python module installing compelete'
echo ' '
sleep 1

echo ' Config File installing....'
echo 'y' | cp -rp ./etc/dbconf.conf $HOME/MP/etc/
echo 'y' | cp -rp ./etc/process_info.conf $HOME/MP/etc/
echo 'y' | cp -rp ./etc/simc.conf $HOME/MP/etc/
echo 'y' | cp -rp ./etc/simd.conf $HOME/MP/etc/
#echo 'y' | cp -rp ./etc/framework.conf $HOME/MP/etc/supervisor/conf.d/
echo 'y' | cp -rp ./etc/framework.ini $HOME/MP/etc/supervisor/conf/
echo ' '

sleep 1
echo ' lib File installing....'
#echo 'y' | cp -rp ./lib/*.py $HOME/MP/lib/python/common/
echo 'y' | cp -rp ./lib/*.py $HOME/MP/lib/common/
sleep 1

echo ' bin File installing....'
echo 'y' | cp -rp ./bin/simd.py $HOME/MP/bin/simd
echo 'y' | cp -rp ./bin/simc.py $HOME/MP/bin/simc
echo 'y' | cp -rp ./bin/simw $HOME/MP/bin/simw
echo 'y' | cp -rp ./bin/simd/ $HOME/MP/lib/
sleep 1
