#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DIS_WLMIC_INFO_MIN_PARAMETER = 0
DIS_WLMIC_INFO_MAX_PARAMETER = 0

param_list = []
mandatory_list = {}

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DIS-WLMIC-INFO
 
 [Parameter]
  N/A

 [Usage]
  DIS-WLMIC-INFO 
   ex) DIS-WLMIC-INFO
  
 [Column information]
  WL_MIC_ID           : Wireless MIC Identity
  USED_FLAG           : Whether to use a MIC
  WL_MIC_NAME         : Wireless MIC Name
  TMP_MIC_NAME        : Temporary User Name
  WL_DEVICE_SER       : Connect Wireless Device Serial Number & Name 

 [Result]
 <SUCCESS>
 Date time
 MMC    = DIS-WLMIC-INFO 
 Result = SUCCESS
 ================================================================================
   WL_MIC_ID | USED_FLAG |   WL_MIC_NAME   |   TMP_MIC_NAME   | WL_DEVICE_SER   |
 --------------------------------------------------------------------------------
  value      | value     | value           | value            | value           |
 ...
 ================================================================================

 <FAILURE>
 Date time
 MMC    = DIS-WLMIC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			if len(ARG) is not 0:
				ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DIS_WLMIC_INFO_MAX_PARAMETER, DIS_WLMIC_INFO_MIN_PARAMETER)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

				if (ARG_CNT > 0) : 
					ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
					if (ret == False) :
						result = 'FAILURE'
						return MMCLib.make_result(MMC, ARG, result, reason, '')

			try :
				total_body = ''

				if len(ARG) == 0: # select all
					data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_MIC", "*", ';')

				if int(data_cnt) is 0:
					result = 'FAILURE'
					reason = "DATA DOES NOT EXIST"
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
				if err is not None:
					result = 'FAILURE'
					reason = "Select Failure. [{}]".format(err)
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				total_body=""" {:^9} | {:^9} | {:^13} | {:^14} | {:^19} |\n""" .format('WL_MIC_ID', 'USED_FLAG', 'WL_MIC_NAME', 'TMP_MIC_NAME', 'WL_DEVICE_SER')

				total_body= total_body + ('-' * MMC_SEND_LINE_WLMIC_CNT)
				for thr_num in range(len(db_data)) :
					wl_mic_id = db_data[thr_num]["WL_MIC_ID"]
					used_flag = db_data[thr_num]["USED_FLAG"]
					if used_flag is '0':
						used_flag = "OFF"
					else:
						used_flag = "ON"
					wl_mic_name = db_data[thr_num]["WL_MIC_NAME"]
					tmp_mic_name = db_data[thr_num]["TMP_MIC_NAME"]
					tmp_wl_device_id = db_data[thr_num]["MINUTES_WL_DEVICE_SER"]
					data_cnt2, db_data2, err2 = MMCLib.Select_query(mysql, "MINUTES_WL_DEVICE", "DEVICE_NAME", "WHERE MINUTES_WL_DEVICE_SER={}".format(tmp_wl_device_id))
					if int(data_cnt2) is 0:
						wl_device_name = "UNKNOWN"
					else:
						wl_device_name = str(tmp_wl_device_id) + ' (' + db_data2[0]["DEVICE_NAME"] + ')'
					row_buf="\n {:9} | {:9} | {:13} | {:11} | {:19} |" .format(wl_mic_id, used_flag, wl_mic_name, tmp_mic_name, wl_device_name)
					total_body = total_body + row_buf 

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DIS-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB Select Failure'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-WLMIC-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-WLMIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-WLMIC-INFO(), Config read error: [{}]".format(e))
		reason='DIS-WLMIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DIS-WLMIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DIS-WLMIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

