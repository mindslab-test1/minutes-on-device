#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
# PYTHON COMMON MODULE
#######################################################################
import os, sys
import json
import traceback
import time
import re
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else:
	import configparser as ConfigParser
import ipaddress
from datetime import datetime
from simd_main import *

#######################################################################
# INSTALL MODULE
#######################################################################
import pymysql

#######################################################################
# COMMON LIBRRARY
#######################################################################
import common.dblib as DBLib


#######################################################################
# Argument Handler
#######################################################################
flag_list = ['ON', 'OFF']
ha_list = ['ACTIVE', 'STANDBY']

MMC_SEND_LINE_FAILURE_CNT = 70
MMC_SEND_LINE_PRC_SUCCESS_CNT = 120
MMC_SEND_LINE_RM_THR_SUCCESS_CNT = 74
MMC_SEND_LINE_SYS_SUCCESS_CNT = 58
MMC_SEND_LINE_DBCONF_CNT = 50
MMC_SEND_LINE_LOG_LEVEL_CNT = 44
MMC_SEND_LINE_HA_CNT = 55
MMC_SEND_LINE_RM_CNT = 45
MMC_SEND_LINE_DB_PWD_CNT = 100 
MMC_SEND_LINE_RPC_CNT = 80 
MMC_SEND_LINE_FILE_SYNC_CNT = 55
MMC_SEND_LINE_WLMIC_CNT = 80
MIN_STRING_LENGTH = 1
MAX_STRING20_LENGTH = 20 - 1
MAX_STRING50_LENGTH = 50 - 1
MAX_STRING100_LENGTH = 100 - 1
MAX_STRING200_LENGTH = 200 - 1
MAX_STRING255_LENGTH = 255 - 1
MIN_PORT_NUMBER = 1025
MAX_PORT_NUMBER = 65536
MIN_INT10_VALUE = 1
MAX_INT10_VALUE = 9999999999
MIN_THRESHOLD_VALUE = 1
MAX_THRESHOLD_VALUE = 100
MIN_COUNT_VALUE = 1
MAX_COUNT_VALUE = 100
MIN_INTERVAL_VALUE = 1
MAX_AHOUR_INTERVAL_VALUE = 3600
MAX_ADAY_INTERVAL_VALUE = 86400



#######################################################################
# Function
#######################################################################
def Argument_Parsing(ARG, param_list, max_cnt, min_cnt):

	ARG_CNT = len(ARG)
	print (ARG, ARG_CNT)
	# Just display all of them, MMC has not parameters.
	if (max_cnt is 0 and min_cnt is 0) and (min_cnt is 0 and ARG_CNT is 0) :
		return True, ARG_CNT, None, ''

	# Check Arguments count
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt:
		print (max_cnt, min_cnt)
		return False, ARG_CNT, None, "PARAMETER Count is Invalid."
	
	arg_data = {}
	for item in param_list :
		arg_data[item] = None

	# if client input several parameters with attribute name
	if ('=' in ARG[0]) :
		for item in ARG :
			if '=' not in item:
				return False, ARG_CNT, None, "PARAMETER Type is Invalid. Using '=' is all or not"
			else:
				name, value = item.split('=', 1)
				print ("name [{}], value [{}]".format(name, value))
				# check 'parameter name'
				name = name.upper()
				if name not in param_list:
					return False, ARG_CNT, None, "PARAMETER Name is Invalid. [{}]".format(name)
				# check 'duplicated parameters'
				elif arg_data[name] != None:
					return False, ARG_CNT, None, "PARAMETER is Duplicated. [{}]".format(name)
				# check if parameter exists or not
				elif len(value) is 0:
					return False, ARG_CNT, None, "PARAMETER is empty. [{}]".format(name)
				else:
					arg_data[name] = value
	# if client input all parameters without attribute name
	else:
		print (ARG_CNT, len(param_list), param_list)
		if ARG_CNT < len(param_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid. Must input all parameters."
		if ARG_CNT != len(param_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid."

		# client have to input ordered parameters
		for idx in range(len(ARG)) :
			arg_data[param_list[idx]] = ARG[idx]

	print (arg_data)
	return True, ARG_CNT, arg_data, ''


#######################################################################
# CHECK VALIDATION
#######################################################################
def check_Enum(enum_list, name, value, log):
	log.debug('{} : value [{}] -- enum_list : {}'.format(name, value, enum_list))
	if value not in enum_list:
		return False, "{} : [{}] is INVALID".format(name, value)
	return True, ''

def check_Decimal_And_Range(min, max, name, value, log):
	if value.isdecimal() == False :
		return False, " {} is Only Use Unsigned Decimal".format(name)

	log.debug('{} : max [{}] > value [{}] > min [{}]'.format(name, max, value, min))
	if int(value) > max or int(value) < min :
		return False, "{} : [{}] is INVALID Range.".format(name, value)
	return True, ''

def check_String_Length(min, max, name, value, log):
	log.debug('{} : max [{}] > value [{}] > min [{}]'.format(name, max, len(value), min))
	if len(value) > max or len(value) < min :
		return False, "{} : [{}:{}] is INVALID Length.".format(name, value, len(value))
	return True, ''

def check_Ip_Address(name, value, log):
	log.debug('{} : ip address [{}]'.format(name, value))
	try:
		ip = ipaddress.ip_address(value)
		log.debug("[{}] is IP{} Address.".format(ip, ip.version))
		return True, ''
	except ValueError:
		return False, "{} : [{}] is not IP Address Format.".format(name, value)

def check_Email_Address(name, value, log):
	log.debug('{} : E-mail address [{}]'.format(name, value))
	email = re.compile(r'''(  
					([a-zA-Z0-9._%+-]+)
					@
					([a-zA-Z0-9.-]+)
					(\.[a-zA-Z]{2,4}))''', re.VERBOSE)  
	result = email.match(value)
	if result is None:
		return False, "{} : [{}] is not E-mail Address Format.".format(name, value)
	return True, ''

def check_Telephone_Number(name, value, log):
	log.debug('{} : Telephone Number [{}]'.format(name, value))
	phone = re.compile(r'''(
					(\d{2}|\(\d{2}\)|\d{3}|\(\d{3}\))?
					(|-|\.)?
					(\d{3}|\d{4})
					(\s|-|\.)
					(\d{4}))''', re.VERBOSE)  
	result = phone.match(value)
	if result is None:
		return False, "{} : [{}] is not Telephone Number Format.".format(name, value)
	return True, ''

def check_LeapYear(name, value, log):
	log.debug('{} : Year [{}]'.format(name, value))
	return value % 4 == 0 and (value % 100 != 0 or value % 400 == 0);


#######################################################################
# MMC SEND
#######################################################################
def choose_mmc_send_line(MMC):
	if 'PRC' in MMC.upper():
		return MMC_SEND_LINE_PRC_SUCCESS_CNT
	elif 'RM-THR' in MMC.upper():
		return MMC_SEND_LINE_RM_THR_SUCCESS_CNT
	elif 'SYS-INFO' in MMC.upper():
		return MMC_SEND_LINE_SYS_SUCCESS_CNT
	elif 'DBCONF' in MMC.upper():
		return MMC_SEND_LINE_DBCONF_CNT
	elif 'LOG-LEVEL' in MMC.upper():
		return MMC_SEND_LINE_LOG_LEVEL_CNT
	elif 'HA-INFO' in MMC.upper():
		return MMC_SEND_LINE_HA_CNT
	elif 'RM-INFO' in MMC.upper():
		return MMC_SEND_LINE_RM_CNT
	elif 'DB-PWD' in MMC.upper():
		return MMC_SEND_LINE_DB_PWD_CNT
	elif '-RPC' in MMC.upper():
		return MMC_SEND_LINE_RPC_CNT
	elif 'FILE_SYNC' in MMC.upper():
		return MMC_SEND_LINE_FILE_SYNC_CNT
	elif 'WLMIC' in MMC.upper():
		return MMC_SEND_LINE_WLMIC_CNT
	else:
		return 50

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body, succ_line_cnt, fail_line_cnt):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else:
		if (result.upper() == 'FAILURE'):
			msg_body = '=' * fail_line_cnt
			msg_body = msg_body + """\n {}\n""".format(reason)
			msg_body = msg_body + '=' * fail_line_cnt
		else:
			msg_body = '=' * succ_line_cnt
			msg_body = msg_body + """\n{}\n""".format(total_body)
			msg_body = msg_body + '=' * succ_line_cnt
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body):
	result_msg = {}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body, choose_mmc_send_line(MMC), MMC_SEND_LINE_FAILURE_CNT)
	data = msg_header + msg_body
	result_msg['msg_body']['data'] = data
	return result_msg

#######################################################################
# MMC DATABASE QUERY
#######################################################################
# Table, RESULT, WHERE
def Select_query(mysql, table, column, where):
	query = "SELECT {} FROM {} {}".format(column, table, where)
	print (query)

	try:
		# check end of query
		if where[-1] != ';':
			query = query + ';'

		rowcnt, rows, error = mysql.execute_query(query)
		print ("row cnt is [{}]".format(rowcnt))
		return rowcnt, rows, None 

	except Exception as e :
		print('Select_query() error : {}'.format(e))
		print(traceback.format_exc())
		return '', '', e 

def Insert_Query():
	pass

def Update_Query():
	pass

def Delete_query(mysql, table, where):
	query = "DELETE FROM {} WHERE {}".format(table, where)
	print (query)

	# check end of query
	if where[-1] != ';':
		query = query + ';'

	ret, err = mysql.execute(query)
	if ret is False:
		return False, err

	return True, ''




