#! /usr/bin/python
#-*- coding:utf-8 -*-

##############################################################
# PYTHON COMMON MODULE
##############################################################
import sys
import os

##############################################################
# COMMON PATH
##############################################################
root_path = os.getenv("MAIN_PRC")
exe_path = os.getenv("MAIN_PRC") + "/lib/simd"
cfg_path = os.getenv("MAIN_PRC") + "/etc"

##############################################################
# SIMd
##############################################################
sys.path.append(exe_path)
from simd_main import simd_main

if __name__ == "__main__" :
	simd_main(root_path, exe_path, cfg_path)
	print("SIMd EXIT")
