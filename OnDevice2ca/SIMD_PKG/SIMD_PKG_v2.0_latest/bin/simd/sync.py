#! /usr/bin/python
#-*- coding:utf-8 -*-

import os, sys
import shutil
import pysftp
import paramiko
import schedule
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else:
	import configparser as ConfigParser
import time
import traceback
from paramiko.py3compat import decodebytes

def sync_dir(remote_ip, remote_port, remote_user, remote_passwd, remote_dir, local_dir, log, remove_outdated=True):
    """
    Sync to all the contents of remote_dir only.

    :param remote_dir:      Remote directory to sync from.
    :param local_dir:       Local directory to sync to.
    :param remove_outdated: If outdated items should be removed.
    :return                 The number of files synced.
    """

    files_synced = 0
    print('------------------------')
    print("sftp sync of '{}' starting...".format(remote_dir))
    log.info('------------------------')
    log.info("sftp sync of '{}' starting...".format(remote_dir))
    sftp = _get_connection(remote_ip, int(remote_port), remote_user, remote_passwd)
    if sftp is not None:
        try:
            files_synced = _sync_r(sftp, remote_dir, local_dir, log)
            print("synced {} file(s) from '{}'".format(files_synced, remote_dir))
            log.info("synced {} file(s) from '{}'".format(files_synced, remote_dir))
            if remove_outdated:
                print("cleaning up outdated items of '{}' starting...".format(remote_dir))
                log.info("cleaning up outdated items of '{}' starting...".format(remote_dir))
                outdated_removed = _remove_outdated_r(sftp, remote_dir, local_dir, log)
                print("removed {} outdated item(s) of '{}'".format(outdated_removed, remote_dir))
                log.info("removed {} outdated item(s) of '{}'".format(outdated_removed, remote_dir))
            sftp.close()
        except Exception as e:
            print("sync of '{}' could not complete successfully ({})".format(remote_dir), str(e))
            log.info("sync of '{}' could not complete successfully ({})".format(remote_dir), str(e))
            files_synced = 0
    log.info('------------------------')
    return files_synced


def _get_connection(ip, port, user, passwd):
    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None
    return pysftp.Connection(host=ip, port=port, username=user, password=passwd, cnopts=cnopts)


def _sync_r(sftp, remote_dir, local_dir, log):
    """
    Recursively sync the sftp contents starting at remote dir to the local dir and return the number of files synced.
    :param sftp:        Connection to the sftp server.
    :param remote_dir:  Remote dir to start sync from.
    :param local_dir:   To sync to.
    :return             The number of files synced.
    """
    files_synced = 0
    for item in sftp.listdir(remote_dir):
        remote_dir_item = os.path.join(remote_dir, item)
        local_dir_item = os.path.join(local_dir, item)
        if sftp.isfile(remote_dir_item):
            if not os.path.exists(local_dir):
                os.makedirs(local_dir)
            if _should_sync_file(sftp, remote_dir_item, local_dir_item):
                print('sync {} => {}'.format(remote_dir_item, local_dir_item))
                log.info('sync {} => {}'.format(remote_dir_item, local_dir_item))
                sftp.get(remote_dir_item, local_dir_item, preserve_mtime=True)
                files_synced += 1
        else:
            files_synced += _sync_r(sftp, remote_dir_item, local_dir_item, log)
    return files_synced


def _should_sync_file(sftp, remote_file_path, local_file_path):
    """
    If the remote_file should be synced - if it was not downloaded or it is out of sync with the remote version.

    :param sftp:                Connection to the sftp server.
    :param remote_file_path:    Remote file path.
    :param local_file_path:     Local file path.
    """
    if not os.path.exists(local_file_path):
        return True
    else:
        remote_attr = sftp.lstat(remote_file_path)
        local_stat = os.stat(local_file_path)
        return remote_attr.st_size != local_stat.st_size or remote_attr.st_mtime != local_stat.st_mtime


def _remove_outdated_r(sftp, remote_dir, local_dir, log):
    items_removed = 0
    for item in os.listdir(local_dir):
        remote_dir_item = os.path.join(remote_dir, item)
        local_dir_item = os.path.join(local_dir, item)
        if not sftp.exists(remote_dir_item):
            print('removing {}'.format(local_dir_item))
            log.info('removing {}'.format(local_dir_item))
            _remove(local_dir_item, log)
            items_removed += 1
        else:
            if os.path.isdir(local_dir_item):
                items_removed += _remove_outdated_r(sftp, remote_dir_item, local_dir_item, log)
    return items_removed


def _remove(path, log):
    """ param <path> could either be relative or absolute. """
    try:
        if os.path.isfile(path):
            os.remove(path)  # remove file
        else:
            shutil.rmtree(path)  # remove directory
    except Exception as e:
        print("could not remove {}, error {0}".format(path, str(e)))
        log.info("could not remove {}, error {0}".format(path, str(e)))

def sync_proc(log, simd_cfg_path):

    ### ConfigParser 객체 생성
	simd_config = ConfigParser.RawConfigParser()

	while True :

		try :
			## Config Value setting
			simd_config.read(simd_cfg_path)
			use_flag = simd_config.get("FILE_SYNC", "use_flag").lower()
			if ((use_flag == "on") or (use_flag == "off")):
				pass
			else :
				log.error("'use_flag' is only 'ON' and 'OFF'")
				time.sleep(1)
				continue

			if (use_flag != "on") :
				log.info("[SYNC] 'use_flag' is 'OFF'")
				log.info("[SYNC] Change the Config 'ON' if you want to use")
				time.sleep(0.05)
				continue
			else:
				log.info("'use_flag' is 'ON'")
				break
				

		except ConfigParser.MissingSectionHeaderError as e :
			log.error("simd_conf read Error")

		except Exception as e :
			log.error("SYNC_PROCESS ERROR : [{}]".format(e))
			log.error(traceback.format_exc())

		time.sleep(0.05)

	try : 
		remote_ip = simd_config.get("FILE_SYNC", "src_ip")
		remote_port = simd_config.get("FILE_SYNC", "src_port")
		remote_user = simd_config.get("FILE_SYNC", "src_user")
		remote_passwd = simd_config.get("FILE_SYNC", "src_passwd")
		remote_path = simd_config.get("FILE_SYNC", "src_path")
		local_path = simd_config.get("FILE_SYNC", "my_path")
		sync_interval = simd_config.get("FILE_SYNC", "sync_interval")

		schedule.every(int(sync_interval)).seconds.do(sync_dir, remote_ip, remote_port, remote_user, remote_passwd, remote_path, local_path, log)

		while True:
			schedule.run_pending()
			time.sleep(1)

	except ConfigParser.MissingSectionHeaderError as e :
		log.error("simd_conf read Error")

	except Exception as e :
		log.error("SYNC_PROCESS ERROR : [{}]".format(e))
		log.error(traceback.format_exc())


#if __name__ == "__main__":
#	sync_dir('/home/minds/git/mss/test_data_file', '/home/minds/git/mss/sync_test', False)	
