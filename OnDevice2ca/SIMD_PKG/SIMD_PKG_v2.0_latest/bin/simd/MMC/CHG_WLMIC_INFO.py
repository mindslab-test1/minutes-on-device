#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

CHG_WLMIC_INFO_MIN_PARAMETER = 3
CHG_WLMIC_INFO_MAX_PARAMETER = 5

param_list = ["WL_MIC_ID", "USED_FLAG", "WL_MIC_NAME", "TMP_MIC_NAME", "WL_DEVICE_SER"]
mandatory_list = ["WL_MIC_ID", "WL_DEVICE_SER"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  CHG-WLMIC-INFO [WL_MIC_ID=a], [WL_DEVICE_SER=b] (USED_FLAG=c), (WL_MIC_NAME=d), (TMP_MIC_NAME=e)

 [Parameter]
  a = DECIMAL     (1:100)
  b = DECIMAL     (1:9999999999)
  c = ENUM        (ON|OFF)
  d = STRING      (1:100)
  e = STRING      (1:100)

 [Usage]
  CHG-WLMIC-INFO [a, b, c, d, e]
   ex) CHG-WLMIC-INFO 3, 1, ON, TEST_MIC3, 홍길동
  CHG-WLMIC-INFO [WL_MIC_ID=a], [WL_DEVICE_SER=b] (USED_FLAG=c), (WL_MIC_NAME=d), (TMP_MIC_NAME=e)
   ex) CHG-WLMIC-INFO WL_MIC_ID=3, WL_DEVICE_SER=1, USED_FLAG=ON, WL_MIC_NAME=TEST_MIC3, TMP_MIC_NAME=홍길동
  
 [Column information]
  WL_MIC_ID           : Wireless MIC Identity
  WL_DEVICE_SER       : Connect Wireless Device Serial Number & Name
  USED_FLAG           : Whether to use a MIC
  WL_MIC_NAME         : Wireless MIC Name
  TMP_MIC_NAME        : Temporary User Name

 [Result]
  <SUCCESS>
  Date time
  MMC    = CHG-WLMIC-INFO 
  Result = SUCCESS
  ====================================================
  WL_MIC_ID           : MIC Identity
  WL_DEVICE_SER       : Device Identity
  ----------------------------------------------------
  USED_FLAG           : before -> after
  WL_MIC_NAME         : before -> after
  TMP_MIC_NAME        : before -> after 
  ====================================================

  <FAILURE>
  Date time
  MMC    = CHG-WLMIC-INFO
  Result = FAILURE
  ===========================================================
  Reason = Reason for error
  ===========================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list) :
	
	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['WL_MIC_ID'] != None:
		ret, reason = check_Decimal_And_Range(MIN_COUNT_VALUE, MAX_COUNT_VALUE, 'WL_MIC_ID', arg_data['WL_MIC_ID'], G_log)
		if (ret == False) : return False, reason

	if arg_data['WL_DEVICE_SER'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'WL_DEVICE_SER', arg_data['WL_DEVICE_SER'], G_log)
		if (ret == False) : return False, reason

	##### Optional Parameter #####
	if arg_data['USED_FLAG'] != None :
		ret, reason = check_Enum(flag_list, 'USED_FLAG', arg_data['USED_FLAG'], G_log)
		if (ret == False) : return False, reason

	if arg_data['WL_MIC_NAME'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'WL_MIC_NAME', arg_data['WL_MIC_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['TMP_MIC_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'TMP_MIC_NAME', arg_data['TMP_MIC_NAME'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, CHG_WLMIC_INFO_MAX_PARAMETER, CHG_WLMIC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			try :
				data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_MIC", "*" , "where WL_MIC_ID= {} and MINUTES_WL_DEVICE_SER={};".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER']))
				if int(data_cnt) is 0:
					result = 'FAILURE'
					reason = "WL_MIC_ID [{}] and WL_DEVICE_SER [{}] does not exist".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
				elif err is not None:
					result = 'FAILURE'
					reason = "Select Failure. [{}]".format(err)
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
				else :
					# make query
					update_query = """ UPDATE MINUTES_WL_MIC SET UPDATE_USER='simd', UPDATE_TIME=now()"""
					if Parsing_Dict['USED_FLAG'] is not None:
						if (Parsing_Dict['USED_FLAG'] == 'OFF'):
							update_query = update_query + """, USED_FLAG='0'"""
						else:
							update_query = update_query + """, USED_FLAG='1'"""
					if Parsing_Dict['WL_MIC_NAME'] is not None:
						update_query = update_query + """, WL_MIC_NAME='{}'""".format(Parsing_Dict['WL_MIC_NAME'])
					if Parsing_Dict['WL_DEVICE_SER'] is not None:
						update_query = update_query + """, MINUTES_WL_DEVICE_SER={}""".format(Parsing_Dict['WL_DEVICE_SER'])
					if Parsing_Dict['TMP_MIC_NAME'] is not None:
						update_query = update_query + """, TMP_MIC_NAME='{}'""".format(Parsing_Dict['TMP_MIC_NAME'])
					# WEHRE
					if Parsing_Dict['WL_MIC_ID'] is not None:
						update_query = update_query + """ WHERE WL_MIC_ID={} AND MINUTES_WL_DEVICE_SER={}""".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER'])
#
					G_log.debug("update query = {}".format(update_query))
					ret, err = mysql.execute(update_query)
					if ret is False:
						result = 'FAILURE'
						if err is not None:
							reason='DB UPDATE FAILURE. [{}]'.format(err)
						else:
							reason='Update Failure.'
						return MMCLib.make_result(MMC, ARG, result, reason, total_body)

					# make response message
					total_body = total_body +'WL_MIC_ID = {}, WL_DEVICE_SER = {}'.format(db_data[0]['WL_MIC_ID'], db_data[0]['MINUTES_WL_DEVICE_SER'])
					total_body = total_body + '\n--------------------------------------------------------------------------------'
					if Parsing_Dict['USED_FLAG'] is not None:
						if (db_data[0]['USED_FLAG']) is '1':
							org_used_flag = "ON"
						else:
							org_used_flag = "OFF"
						total_body = total_body +'\nUSED_FLAG           = {} -> {}'.format(org_used_flag, Parsing_Dict['USED_FLAG'])
					if Parsing_Dict['WL_MIC_NAME'] is not None:
						total_body = total_body +'\nWL_MIC_NAME         = {} -> {}'.format(db_data[0]['WL_MIC_NAME'], Parsing_Dict['WL_MIC_NAME'])
					if Parsing_Dict['TMP_MIC_NAME'] is not None:
						total_body = total_body +'\nTMP_MIC_NAME        = {} -> {}'.format(db_data[0]['TMP_MIC_NAME'], Parsing_Dict['TMP_MIC_NAME'])
#
					return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('CHG-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB UPDATE FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-WLMIC-INFO(), NoSectionError : [{}]".format(e))
		reason='CHG-WLMIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("CHG-WLMIC-INFO(), Config read error: [{}]".format(e))
		reason='CHG-WLMIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-WLMIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-WLMIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

