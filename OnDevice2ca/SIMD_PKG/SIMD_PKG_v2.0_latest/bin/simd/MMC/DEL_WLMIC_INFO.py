#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DEL_WLMIC_INFO_MIN_PARAMETER = 2
DEL_WLMIC_INFO_MAX_PARAMETER = 2

param_list = ["WL_MIC_ID", "WL_DEVICE_SER"]
mandatory_list = ["WL_MIC_ID", "WL_DEVICE_SER"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DEL-WLMIC-INFO [WL_MIC_ID=a], [WL_DEVICE_SER=b]

 [Parameter]
  a = DECIMAL    (1:100)
  b = DECIMAL    (1:9999999999)

 [Usage]
  DEL-WLMIC-INFO [a, b]
   ex) DEL-WLMIC-INFO 3, 2
  DEL-WLMIC-INFO [WL_MIC_ID=a], [WL_DEVICE_SER=b]
   ex) DEL-WLMIC-INFO WL_MIC_ID=1, WL_DEVICE_SER=2
  
 [Column information]
  WL_MIC_ID           : Wireless MIC Identity
  USED_FLAG           : Whether to use a MIC
  WL_MIC_NAME         : Wireless MIC Name
  TMP_MIC_NAME        : Temporary User Name
  WL_DEVICE_SER       : Connect Wireless Device Serial Number & Name


 [Result]
 <SUCCESS>
 Date time
 MMC    = DEL-WLMIC-INFO 
 Result = SUCCESS
 ================================================================================
   WL_MIC_ID | USED_FLAG |   WL_MIC_NAME   |   TMP_MIC_NAME   | WL_DEVICE_SER   |
 --------------------------------------------------------------------------------
  value      | value     | value           | value            | value           |
 ================================================================================

 <FAILURE>
 Date time
 MMC    = DEL-WLMIC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

	
def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['WL_MIC_ID'] != None:
		ret, reason = check_Decimal_And_Range(MIN_COUNT_VALUE, MAX_COUNT_VALUE, 'WL_MIC_ID', arg_data['WL_MIC_ID'], G_log)
		if (ret == False) : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DEL_WLMIC_INFO_MAX_PARAMETER, DEL_WLMIC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# delete pricing information
			try :
				data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_MIC", "*" , "where WL_MIC_ID= {} and MINUTES_WL_DEVICE_SER={};".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER']))
				print (data_cnt)
				if int(data_cnt) is 0 :
					result = 'FAILURE'
					reason = "WL_MIC_ID [{}] and WL_DEVICE_SER [{}] does not exist".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				if err is not None:
					result = 'FAILURE'
					reason = "Select Failure."
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				ret, err = MMCLib.Delete_query(mysql, "MINUTES_WL_MIC", "WL_MIC_ID={} and MINUTES_WL_DEVICE_SER={}".format(Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_DEVICE_SER']))
				if ret is False:
					result='FAILURE'
					if err is not None:
						reason='Delete Failure. [{}]'.format(err)
					else:
						reason='Delete Failure.'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DEL-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB Delete Failure'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_MIC", "*" , ';')
			if int(data_cnt) is 0 :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			elif err is not None:
				result = 'FAILURE'
				reason = "Select Failure."
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				G_log.debug(db_data)
				total_body=""" {:^9} | {:^9} | {:^13} | {:^14} | {:^19} |\n""" .format('WL_MIC_ID', 'USED_FLAG', 'WL_MIC_NAME', 'TMP_MIC_NAME', 'WL_DEVICE_SER')
				total_body= total_body + ('-' * MMC_SEND_LINE_WLMIC_CNT)
				for thr_num in range(len(db_data)) :
					wl_mic_id = db_data[thr_num]["WL_MIC_ID"]
					used_flag = db_data[thr_num]["USED_FLAG"]
					if used_flag is '0':
					    used_flag = "OFF"
					else:
						used_flag = "ON"
					wl_mic_name = db_data[thr_num]["WL_MIC_NAME"]
					tmp_mic_name = db_data[thr_num]["TMP_MIC_NAME"]
					if(tmp_mic_name is None):
						tmp_mic_name = "None"
					tmp_wl_device_id = db_data[thr_num]["MINUTES_WL_DEVICE_SER"]
					data_cnt2, db_data2, err2= MMCLib.Select_query(mysql, "MINUTES_WL_DEVICE", "DEVICE_NAME", "WHERE MINUTES_WL_DEVICE_SER={}".format(tmp_wl_device_id))
					if int(data_cnt2) is 0:
						wl_device_name = "UNKNOWN"
					else:
						wl_device_name = db_data2[0]["DEVICE_NAME"]
					row_buf="\n {:9} | {:9} | {:13} | {:11} | {:19} |" .format(wl_mic_id, used_flag, wl_mic_name, tmp_mic_name, wl_device_name)
					total_body = total_body + row_buf

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-WLMIC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-WLMIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DEL-WLMIC-INFO(), Config read error: [{}]".format(e))
		reason='DEL-WLMIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DEL-WLMIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL-WLMIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

