#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import common.dblib as DBLib
import traceback
import ConfigParser
import pymysql
import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DIS_DBCONF_INFO_MIN_PARAMETER = 0
DIS_DBCONF_INFO_MAX_PARAMETER = 0

param_list = {}
mandatory_list = {}

def mmc_help():
	total_body = """
============================================================
 {} = mandatory, () = optional
============================================================
 [Command]
  DIS-DBCONF-INFO

 [Parameter]
  N/A

 [Usage]
  DIS-DBCONF-INFO

 [Section Options Configuration]
  type(db.type)                 : Database type
  primary_host(db.pri_host)     : Database primary_host
  primary_port(db.pri_port)     : Database primary_port
  secondary_host(db.sec_host)   : Database second_host
  secondary_port(db.sec_port)   : Database second_port
  user(db.user)                 : Database user
  password(db.psswd)            : Database password
  sid(db.sid)                   : Database sid
  encode(db.encode)             : Database Encoding

 
 [Result]
  <SUCCESS>
  Date time
  MMC = DIS-DBCONF-INFO
  Result = SUCCESS
  ====================================================
   TYPE             : value
   PRIMARY_HOST     : value
   PRIMARY_PORT     : value
   SECONDARY_HOST   : value
   SECONDARY_PORT   : value
   USER             : value
   PASSWORD         : value
   SID              : value
   ENCODE           : value
  ====================================================
  
  <FAILURE>
  Date time
  MMC = DIS-DBCONF-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

    return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/dbconf.conf)
			db_config = ConfigParser.RawConfigParser()
			db_config.read(G_db_cfg_path)
			
			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, DIS_DBCONF_INFO_MAX_PARAMETER, DIS_DBCONF_INFO_MIN_PARAMETER)
			if ret == False :
				result = 'FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARV, result, reason, '')

			db_convert = {}
			db_convert["db.type"] = "TYPE"
			db_convert["db.pri_host"] = "PRIMARY_HOST"
			db_convert["db.pri_port"] = "PRIMARY_PORT"
			db_convert["db.sec_host"] = "SECONDARY_HOST"
			db_convert["db.sec_port"] = "SECONDARY_PORT"
			db_convert["db.user"] = "USER"
			db_convert["db.passwd"] = "PASSWORD"
			db_convert["db.sid"] = "SID"
			db_convert["db.encode"] = "ENCODE"

			data = OrderedDict()	
			org_item_value = db_config.items('DBCONF')
			for i in range(len(org_item_value)) :
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))
			
			row = '' 
			for item in data :
				if (row == ''):
					row = ' {0:15} = {1}'.format(db_convert[item], data[item])
				else : 
					row = '\n {0:15} = {1}'.format(db_convert[item], data[item])
				total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('DIS_DBCONF_CONF() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-DBCONF-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-DBCONF-INFO(), NoSectionError'
		G_log.error(traceback.format_exc())
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error("DIS-DBCONF-INFO(), Config read error: [{}]".format(e))
		reason='DIS-DBCONF-INFO(), Config read error'
		G_log.error(traceback.format_exc())
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS_DBCONF_CONF(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

