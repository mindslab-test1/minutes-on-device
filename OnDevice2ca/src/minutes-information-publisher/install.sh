DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop minutes-information-publisher
sleep 1
cp ${DIR}/mipd.py $HOME/MP/bin/mipd
svctl start minutes-information-publisher
