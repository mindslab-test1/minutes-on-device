#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import libmmc as MMCLib
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_HA_INFO_MIN_PARAMETER = 1
CHG_HA_INFO_MAX_PARAMETER = 6

param_list = ['USE_FLAG', 'OPPOSITE_IP', 'HA_PORT', 'HB_INTERVAL', 'HB_COUNT', 'HA_MODE']
mandatory_list = []

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-HA-INFO (USE_FLAG=a), (OPPOSITE_IP=b), (HA_PORT=c), (HB_INTERVAL=d), (HB_COUNT=e), (HA_MODE=f)

 [Parameter]
  a = ENUM      (ON|OFF)
  b = IPADDR    (1:15)
  c = INTEGER   1025:65536)
  d = INTEGER   (1:3600)
  e = INTEGER   (1:100)
  f = STRING    (1:100)

 [Usage]
  CHG-HA-INFO [a, b, c, d, e, f]
   ex) CHG-HA-INFO ON, 127.0.0.1, 8888, 5, 3, ACTIVE
  CHG-HA-INFO (USE_FLAG=a), (OPPOSITE_IP=b), (HA_PORT=c), (HB_INTERVAL=d), (HB_COUNT=e), (HA_MODE=f)
   ex) CHG-HA-INFO USE_FLAG=ON, OPPOSITE_IP=127.0.0.1, HA_PORT=8888, HB_INTERVAL=5, HB_COUNT=3, HA_MODE=ACTIVE

 [Section Options Configuration]
  use_flag             : Whether to use a ha
  opposite_ip          : target_ip information
  ha_port              : ha_port information
  hb_interval          : HA_CHECK_MSG interval
  hb_count             : HA_CHECK_MSG count
  ha_mode              : ha_mode (ACTIVE or STANDBY)

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-HA-INFO
  Result = SUCCESS
  ===================================================
   use_flag             = before -> after
   opposite_ip          = before -> after
   ha_port              = before -> after
   hb_interval          = before -> after
   hb_count             = before -> after 
   ha_mode              = before -> after 
  ===================================================

  <FAILURE>
  Date time
  MMC = CHG-HA-INFO
  Result = FAILURE
  ===================================================
  Reason = Reason for error
  ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	##### Optional Parameter #####
	if arg_data['USE_FLAG'] != None :
		ret, reason = check_Enum(flag_list, 'USE_FLAG', arg_data['USE_FLAG'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['OPPOSITE_IP'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'OPPOSITE_IP', arg_data['OPPOSITE_IP'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Ip_Address('OPPOSITE_IP', arg_data['OPPOSITE_IP'], G_log)
		if (ret == False) : return False, reason

	if arg_data['HA_PORT'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'HA_PORT', arg_data['HA_PORT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['HB_INTERVAL'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INTERVAL_VALUE, MAX_INTERVAL_VALUE, 'HB_INTERVAL', arg_data['HB_INTERVAL'], G_log)
		if (ret == False) : return False, reason

	if arg_data['HB_COUNT'] != None :
		ret, reason = check_Decimal_And_Range(MIN_COUNT_VALUE, MAX_COUNT_VALUE, 'HB_COUNT', arg_data['HB_COUNT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['HA_MODE'] != None :
		ret, reason = check_Enum(ha_list, 'HA_MODE', arg_data['HA_MODE'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_HA_INFO_MAX_PARAMETER, CHG_HA_INFO_MIN_PARAMETER)
			if (ret == False) :
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')
			
			### get old data from simd config file (/srv/maum/etc/simd.conf) ###
			org_data = OrderedDict()
			org_item_value = simd_config.items('HA')
			for i in range(len(org_item_value)):
				org_data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(org_data))
			
			### change config file data
			first = 0
			for item in Parsing_Dict :
				if Parsing_Dict[item] != None :
					value = simd_config.set('HA', item.lower(), Parsing_Dict[item].upper())
					if value == False :
						reason = 'Change Config File Failure. [{}]'.format(item)
						return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)
						# make response message
						if first is 0:
							total_body = total_body + ' {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
							first +=1
						else:
							total_body = total_body + '\n {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
				else :
					continue

			G_log.info('CHG-HA-INFO Complete!!')
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-HA-INFO(), NoSectionError : [{}]".format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-HA-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('CHG-HA-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(G_simd_cfg_path)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_RM_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

