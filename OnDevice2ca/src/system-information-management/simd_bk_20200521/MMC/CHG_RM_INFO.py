#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import libmmc as MMCLib
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_RM_INFO_MIN_PARAMETER = 1
CHG_RM_INFO_MAX_PARAMETER = 5

param_list = ['USE_FLAG', 'WS_PORT', 'MONITORING_INTERVAL', 'DB_INSERT_FLAG', 'DB_INSERT_INTERVAL']
mandatory_list = {}

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-RM-INFO (USE_FLAG=a), (WS_PORT=b), (MONITORING_INTERVAL=c), (DB_INSERT_FLAG=d), (DB_INSERT_INTERVAL=e)

 [Parameter]
  a = ENUM      (ON|OFF)
  b = INTEGER   (1025:65536)
  c = INTEGER   (1:3600)
  d = ENUM      (ON|OFF)
  e = INTEGER   (1:3600)

 [Usage]
  CHG-RM-INFO [a, b, c, d, e]
   ex) CHG-RM-INFO ON, 28081, 1, ON, 5
  CHG-RM-INFO (USE_FLAG=a), (WS_PORT=b), (MONITORING_INTERVAL=c), (DB_INSERT_FLAG=d), (DB_INSERT_INTERVAL=e)
   ex) CHG-RM-INFO USE_FLAG=ON, WS_PORT=28081, MONITORING_INTERVAL=1, DB_INSERT_FLAG=ON, DB_INSERT_INTERVAL=5

 [Section Options Configuration]
  use_flag             : Whether to use a resource_monitor
  ws_port              : Web_socket port
  monitoring_interval  : Time interval for resource_monitor
  db_insert_flag       : Whether to use Database insert
  db_insert_interval   : Time interval for Database insert

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-RM-INFO
  Result = SUCCESS
  ===================================================
  use_flag            = before -> after
  ws_port             = before -> after
  monitoring_interval = before -> after
  db_insert_flag      = before -> after
  db_insert_interval  = before -> after
  ===================================================

  <FAILURE>
  Date time
  MMC = CHG-RM-INFO
  Result = FAILURE
  ===================================================
  Reason = Reason for error
  ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	##### Optional Parameter #####
	if arg_data['USE_FLAG'] != None:
		ret, reason = check_Enum(flag_list, 'USE_FLAG', arg_data['USE_FLAG'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['WS_PORT'] != None:
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'WS_PORT', arg_data['WS_PORT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['MONITORING_INTERVAL'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INTERVAL_VALUE, MAX_INTERVAL_VALUE, 'MONITORING_INTERVAL', arg_data['MONITORING_INTERVAL'], G_log)
		if (ret == False) : return False, reason

	if arg_data['DB_INSERT_FLAG'] != None:
		ret, reason = check_Enum(flag_list, 'DB_INSERT_FLAG', arg_data['DB_INSERT_FLAG'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['DB_INSERT_INTERVAL'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INTERVAL_VALUE, MAX_INTERVAL_VALUE, 'DB_INSERT_INTERVAL', arg_data['DB_INSERT_INTERVAL'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_RM_INFO_MAX_PARAMETER, CHG_RM_INFO_MIN_PARAMETER)
			if (ret == False) :
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			
			### get old data from simd config file (/srv/maum/etc/simd.conf) ###
			org_data = OrderedDict()
			org_item_value = simd_config.items('HARDWARE_MONITORING')
			for i in range(len(org_item_value)):
				org_data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(org_data))
			
			### change config file data
			first = 0
			for item in Parsing_Dict :
				if Parsing_Dict[item] != None :
					value = simd_config.set('HARDWARE_MONITORING', item.lower(), Parsing_Dict[item].upper())
					if value == False :
						reason = 'Change Config File Failure. [{}]'.format(item)
						return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)
						# make response message
						if first is 0:
							total_body = total_body + ' {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
							first += 1
						else:
							total_body = total_body + '\n {:20} = {} -> {}'.format(item, org_data[item.lower()], Parsing_Dict[item])
				else :
					continue
			
			G_log.info('CHG-RM-INFO Complete!!') 
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-SYS-INFO(), NoSectionError : [{}]".format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-SYS-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('CHG_RM_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(G_simd_cfg_path)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_RM_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

