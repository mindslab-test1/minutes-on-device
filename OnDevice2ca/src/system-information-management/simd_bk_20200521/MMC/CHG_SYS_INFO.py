#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import traceback
import ConfigParser
import pymysql
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_SYS_INFO_MIN_PARAMETER = 1
CHG_SYS_INFO_MAX_PARAMETER = 6

param_list =['SYS_NAME', 'MMC_PORT', 'DB_USE_FLAG', 'UPLOAD_DIR', 'RESULT_DIR', 'ADMIN_EMAIL']
mandatory_list = []

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   CHG-SYS-INFO (SYS_NAME=a), (MMC_PORT=b), (DB_USE_FLAG=c), (UPLOAD_DIR=d), (RESULT_DIR=e), (ADMIN_EMAIL=f)

 [Parameter]
   a = STRING 	(1:200)
   b = INTEGER  (1025:65536)
   c = ENUM		(ON|OFF)
   d = STRING	(1:200)
   e = STRING	(1:200)
   f = STRING	(1:200)

 [Usage]
   CHG-SYS-INFO [a, b, c, d, e, f]
    ex) CHG-SYS-INFO CLOUD_DEV, 8889, ON, /DATA/record/upload, /DATA/record/result, greshaper@mindslab.ai
   CHG-SYS-INFO (sys_name=a), (mmc_port=b), (db_use_flag=c), (upload_dir=d), result_dir=e, admin_emailf=f
    ex) CHG-SYS-INFO SYS_NAME=CLOUD_DEV, MMC_PORT=8889, DB_USE_FLAG=ON, UPLOAD_DIR=/DATA/record/upload, RESULT_DIR=/DATA/record/result, ADMIN_EMAIL=greshaper@mindslab.ai

 [Column information]
   SYS_NAME            : System name
   MMC_PORT            : SIMc bind port
   DB_USE_FLAG         : Whether to use database
   UPLOAD_DIR          : Where to upload voice files from server
   RESULT_DIR          : Where the result informations are stored
   ADMIN_EMAIL         : Administrator email address

 [Result]
 <SUCCESS>
 Date time
 MMC = CHG-SYS-INFO
 Result = SUCCESS
 ===================================================
 [SYS_CONF]
  SYS_NAME     = before -> after
  MMC_PORT     = before -> after
  DB_USE_FLAG  = before -> after
 [DB_INFO]
  UPLOAD_DIR   = before -> after
  RESULT_DIR   = before -> after
  ADMIN_EMAIL  = before -> after
  ===================================================

 <FAILURE>
 Date time
 MMC = CHG-SYS-INFO
 Result = FAILURE
 ===================================================
 Reason = Reason for error
 ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	###### Optional Parameter ######
	if arg_data['SYS_NAME'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'SYS_NAME', arg_data['SYS_NAME'], G_log)
		if (ret == False) : return False, reason
		
	if arg_data['MMC_PORT'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'MMC_PORT', arg_data['MMC_PORT'], G_log)
		if (ret == False) : return False, reason

	if arg_data['DB_USE_FLAG'] != None :
		ret, reason = check_Enum(flag_list, 'DB_USE_FLAG', arg_data['DB_USE_FLAG'].upper(), G_log)
		if (ret == False) : return False, reason

	if arg_data['UPLOAD_DIR'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING200_LENGTH, 'UPLOAD_DIR', arg_data['UPLOAD_DIR'], G_log)
		if (ret == False) : return False, reason

	if arg_data['RESULT_DIR'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING200_LENGTH, 'RESULT_DIR', arg_data['RESULT_DIR'], G_log)
		if (ret == False) : return False, reason

	if arg_data['ADMIN_EMAIL'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING200_LENGTH, 'ADMIN_EMAIL', arg_data['ADMIN_EMAIL'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Email_Address('ADMIN_EMAIL', arg_data['ADMIN_EMAIL'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if(db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return MMCLib.make_result(MMC, ARG, result, reason, '')
				
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, CHG_SYS_INFO_MAX_PARAMETER, CHG_SYS_INFO_MIN_PARAMETER)
			if (ret == False):
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get old data from simd config file (/srv/maum/etc/simd.conf) ###
			org_data = OrderedDict()
			org_item_value = simd_config.items('SYS')
			for i in range(len(org_item_value)):
				org_data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(org_data))

			### get old data from 'MINUTES_COMMON' Table ###
			org_DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
			if not org_DB_data :
				result='FAILURE'
				reason='Select Database Failure.'
				return MMCLib.make_result(MMC, ARG, result, reason, '')
			G_log.info('db_data = {}'.format(org_DB_data))

			## change config file data
			if Parsing_Dict['SYS_NAME'] is not None :
				value = simd_config.set('SYS', 'sys_name', Parsing_Dict['SYS_NAME'])
				if value == False :
					return MMCLib.make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [sys_name]', "")
				else :
					with open(G_simd_cfg_path, 'w') as configfile :
						simd_config.write(configfile)	

			if Parsing_Dict['MMC_PORT'] is not None :
				value = simd_config.set('SYS', 'mmc_port', Parsing_Dict['MMC_PORT'])
				if value == False :
					return MMCLib.make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [mmc_port]', "")
				else :
					with open(G_simd_cfg_path, 'w') as configfile :
						simd_config.write(configfile)	

			if Parsing_Dict['DB_USE_FLAG'] is not None :
				value = simd_config.set('SYS', 'db_use_flag', Parsing_Dict['DB_USE_FLAG'])
				if value == False :
					return MMCLib.make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [db_use_flag]', "")
				else :
					with open(G_simd_cfg_path, 'w') as configfile :
						simd_config.write(configfile)	

			## update DB data
			# make query
			G_log.debug('arg count ={}'.format(ARG_CNT))
			update_query_base = """ update MINUTES_COMMON set """
			if Parsing_Dict['UPLOAD_DIR'] is not None :
				update_query = update_query_base + """ UPLOAD_DIR='{}'""".format(Parsing_Dict['UPLOAD_DIR'])
				G_log.debug(update_query)
				ret = mysql.execute(update_query, True)
				if ret is False:
					result = 'FAILURE'
					reason = 'DB Update Failure'
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if Parsing_Dict['RESULT_DIR'] is not None :
				update_query = update_query_base + """ RESULT_DIR='{}'""".format(Parsing_Dict['RESULT_DIR'])
				G_log.debug(update_query)
				ret = mysql.execute(update_query, True)
				if ret is False:
					result = 'FAILURE'
					reason = 'DB Update Failure'
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if Parsing_Dict['ADMIN_EMAIL'] is not None :
				update_query = update_query_base + """ ADMIN_EMAIL='{}'""".format(Parsing_Dict['ADMIN_EMAIL'])
				G_log.debug(update_query)
				ret = mysql.execute(update_query, True)
				if ret is False:
					result = 'FAILURE'
					reason = 'DB Update Failure'
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			# make response message
			total_body = total_body + ' [SYS_CONF]'
			if Parsing_Dict['SYS_NAME'] is not None :
				total_body = total_body + '\n   SYS_NAME       = {} -> {}'.format(org_data['sys_name'], Parsing_Dict['SYS_NAME'])
			if Parsing_Dict['MMC_PORT'] is not None :
				total_body = total_body + '\n   MMC_PORT       = {} -> {}'.format(org_data['mmc_port'], Parsing_Dict['MMC_PORT'])
			if Parsing_Dict['DB_USE_FLAG'] is not None :
				total_body = total_body + '\n   DB_USE_FLAG    = {} -> {}'.format(org_data['db_use_flag'], Parsing_Dict['DB_USE_FLAG'])
			total_body = total_body + """\n----------------------------------------------------------"""
			total_body = total_body + '\n [DB_INFO]'
			if Parsing_Dict['UPLOAD_DIR'] is not None :
				total_body = total_body + '\n   UPLOAD_DIR     = {} -> {}'.format(org_DB_data[0]['UPLOAD_DIR'], Parsing_Dict['UPLOAD_DIR'])
			if Parsing_Dict['RESULT_DIR'] is not None :
				total_body = total_body + '\n   RESULT_DIR     = {} -> {}'.format(org_DB_data[0]['RESULT_DIR'], Parsing_Dict['RESULT_DIR'])
			if Parsing_Dict['ADMIN_EMAIL'] is not None :
				total_body = total_body + '\n   ADMIN_EMAIL    = {} -> {}'.format(org_DB_data[0]['ADMIN_EMAIL'], Parsing_Dict['ADMIN_EMAIL'])
			
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-SYS-INFO(), NoSectionError : [{}]".format(e))
		reason='CHG-SYS-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error("CHG-SYS-INFO(), Config read error: [{}]".format(e))
		reason='CHG-SYS-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-SYS-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_SYS_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason = 'CHG-SYS-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

def Update_Query(table, DB_data, ID, mysql):

	try :
		if (table == 'MINUTES_COMMON'):
			sql = """
				update MINUTES_COMMON set UPLOAD_DIR='{}', ADMIN_EMAIL='{}', RESULT_DIR='{}';
				""".format(DB_data['UPLOAD_DIR'], DB_data['ADMIN_EMAIL'], DB_data['RESULT_DIR'])

			mysql.execute(sql, True)

		return True

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return False

