#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DIS_FILE_SYNC_MIN_PARAMETER = 0
DIS_FILE_SYNC_MAX_PARAMETER = 0

param_list = {}
mandatory_list = {}

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   DIS-FILE-SYNC

 [Parameter]
   N/A

 [Usage]
   DIS-FILE-SYNC

 [Section Options Configuration]
   use_flag             : Whether to use a HA 
   src_ip               : remote server ip (from)
   src_user             : remote server username
   src_passwd           : remote server user password
   src_path             : remote server sync path (absolute path)
   my_path              : local server sync path (to) (absolute path)
   sync_interval        : file sync interval (sec)

 [Result]
  <SUCCESS>
  Date time
  MMC = DIS-FILE-SYNC
  Result = SUCCESS
  =============================================
   use_flag             : value
   src_ip               : value
   src_user             : value
   src_passwd           : value
   src_path             : value
   my_path              : value
   sync_interval        : value
  =============================================

  <FAILURE>
  Date time
  MMC = DIS-FILE-SYNC
  Result = FAILURE
  =============================================
  Reason = Reason for error
  =============================================

"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body = ''
	
	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)

			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DIS_FILE_SYNC_MAX_PARAMETER, DIS_FILE_SYNC_MIN_PARAMETER)
			if ret == False:
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if(ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if(ret == False):
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get data from '/home/minds/MP/etc/simd.conf' ###
			data = OrderedDict()
			org_item_value = simd_config.items('FILE_SYNC')
			for i in range(len(org_item_value)) :
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))

			row = ''
			for item in data :
				if (row == ''):
					row = ' {0:20} = {1}'.format(item, data[item])
				else :
					row = '\n {0:20} = {1}'.format(item, data[item])
				total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('DIS_FILE_SYNC() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-FILE_SYNC(), NoSectionError : [{}]".format(e))
		reason='DIS-HA-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error('DIS_FILE_SYNC(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='Config_Read error [{}]'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS_FILE_SYNC(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


