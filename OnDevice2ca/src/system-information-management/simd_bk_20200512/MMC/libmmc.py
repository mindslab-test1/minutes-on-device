#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
# PYTHON COMMON MODULE
#######################################################################
import os, sys
import json
import traceback
import time
import re
import ConfigParser
from datetime import datetime
from simd_main import *

#######################################################################
# INSTALL MODULE
#######################################################################
import pymysql

#######################################################################
# COMMON LIBRRARY
#######################################################################
import common.dblib as DBLib


#######################################################################
# Argument Handler
#######################################################################
MMC_SEND_LINE_FAILURE_CNT = 70
MMC_SEND_LINE_PRC_SUCCESS_CNT = 120
MMC_SEND_LINE_RM_THR_SUCCESS_CNT = 74
MIN_STRING_LENGTH = 1
MAX_STRING100_LENGTH = 100 - 1
MAX_STRING200_LENGTH = 200 - 1
MAX_STRING255_LENGTH = 255 - 1
MIN_PORT_NUMBER = 1025
MAX_PORT_NUMBER = 65536
MIN_INT10_VALUE = 1
MAX_INT10_VALUE = 9999999999
MIN_THRESHOLD_VALUE = 1
MAX_THRESHOLD_VALUE = 100


#######################################################################
# Function
#######################################################################
def Argument_Parsing(ARG, param_list, max_cnt, min_cnt):

	ARG_CNT = len(ARG)
	print ARG, ARG_CNT
	# Just display all of them, MMC has not parameters.
	if (max_cnt is 0 and min_cnt is 0) and (min_cnt is 0 and ARG_CNT is 0) :
		return True, ARG_CNT, None, ''

	# Check Arguments count
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt:
		print max_cnt, min_cnt
		return False, ARG_CNT, None, "PARAMETER Count is Invalid."
	
	arg_data = {}
	for item in param_list :
		arg_data[item] = None

	# if client input several parameters with attribute name
	if ('=' in ARG[0]) :
		for item in ARG :
			if '=' not in item:
				return False, ARG_CNT, None, "PARAMETER Type is Invalid. Using '=' is all or not"
			else:
				name, value = item.split('=', 1)
				print "name [{}], value [{}]".format(name, value)
				# check 'parameter name'
				if name not in param_list:
					return False, ARG_CNT, None, "PARAMETER Name is Invalid. [{}]".format(name)
				# check 'duplicated parameters'
				elif arg_data[name] != None:
					return False, ARG_CNT, None, "PARAMETER is Duplicated. [{}]".format(name)
				# check if parameter exists or not
				elif len(value) is 0:
					return False, ARG_CNT, None, "PARAMETER is empty. [{}]".format(name)
				else:
					arg_data[name] = value
	# if client input all parameters without attribute name
	else:
		print ARG_CNT, len(param_list), param_list
		if ARG_CNT != len(param_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid. Must input all parameters."

		# client have to input ordered parameters
		for idx in range(len(ARG)) :
			arg_data[param_list[idx]] = ARG[idx]

	print arg_data
	return True, ARG_CNT, arg_data, ''


#######################################################################
# CHECK VALIDATION
#######################################################################
def check_Enum(enum_list, name, value, log):
	log.debug('{} : value [{}] -- enum_list : {}'.format(name, value, enum_list))
	if value not in enum_list:
		return False, "{} : [{}] is INVALID".format(name, value)
	return True, ''

def check_Decimal_And_Range(min, max, name, value, log):
	if value.isdecimal() == False :
		return False, " {} is Only Use Unsigned Decimal".format(name)

	log.debug('{} : max [{}] > value [{}] > min [{}]'.format(name, max, value, min))
	if int(value) > max or int(value) < min :
		return False, "{} : [{}] is INVALID Range.".format(name, value)
	return True, ''

def check_String_Length(min, max, name, value, log):
	log.debug('{} : max [{}] > value [{}] > min [{}]'.format(name, max, len(value), min))
	if len(value) > max or len(value) < min :
		return False, "{} : [{}:{}] is INVALID Length.".format(name, value, len(value))
	return True, ''

def check_Ip_Address(name, value):
	log.debug('{} : ip address [{}]'.format(name, value))
	try:
		ip = ipaddress.ip_address(value)
		log.debug("[{}] is IP{} Address.".format(ip, ip.version))
		return True, ''
	except ValueError:
		return False, "{} : [{}] is not IP Address Format.".format(name, value)

def check_Email_Address(name, value):
	log.debug('{} : E-mail address [{}]'.format(name, value))
	email = re.compile(r'''(  
					([a-zA-Z0-9._%+-]+)
					@
					([a-zA-Z0-9.-]+)
					(\.[a-zA-Z]{2,4}))''', re.VERBOSE)  
	result = email.match(value)
	if result is None:
		return False, "{} : [{}] is not E-mail Address Format.".foramt(name, value)
	return True, ''

def check_Telephone_Number(name, value):
	log.debug('{} : Telephone Number [{}]'.format(name, value))
	phone = re.compile(r'''(
					(\d{2}|\(\d{2}\)|\d{3}|\(\d{3}\))?
					(|-|\.)?
					(\d{3}|\d{4})
					(\s|-|\.)
					(\d{4}))''', re.VERBOSE)  
	result = phone.match(value)
	if result is None:
		return False, "{} : [{}] is not Telephone Number Format.".foramt(name, value)
	return True, ''


#######################################################################
# MMC SEND
#######################################################################
def choose_mmc_send_line(MMC):
	if 'PRC' in MMC.upper():
		return MMC_SEND_LINE_PRC_SUCCESS_CNT
	if 'RM-THR' in MMC.upper():
		return MMC_SEND_LINE_RM_THR_SUCCESS_CNT

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body, succ_line_cnt, fail_line_cnt):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else:
		if (result.upper() == 'FAILURE'):
			msg_body = '=' * fail_line_cnt
			msg_body = msg_body + """\n {}\n""".format(reason)
			msg_body = msg_body + '=' * fail_line_cnt
		else:
			msg_body = '=' * succ_line_cnt
			msg_body = msg_body + """\n{}\n""".format(total_body)
			msg_body = msg_body + '=' * succ_line_cnt
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body):
	result_msg = {}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body, choose_mmc_send_line(MMC), MMC_SEND_LINE_FAILURE_CNT)
	data = msg_header + msg_body
	result_msg['msg_body']['data'] = data
	return result_msg

#######################################################################
# MMC DATABASE QUERY
#######################################################################



