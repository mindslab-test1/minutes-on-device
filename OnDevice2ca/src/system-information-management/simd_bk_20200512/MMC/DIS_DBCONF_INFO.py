#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

DIS_DBCONF_INFO_MIN_PARAMETER = 0
DIS_DBCONF_INFO_MAX_PARAMETER = 0

def mmc_help():
	total_body = """
============================================================
 {} = mandatory, () = optional
============================================================
 [Command]
  DIS-DBCONF-INFO

 [Parameter]
  N/A

 [Usage]
  DIS-DBCONF-INFO

 [Section Options Configuration]
  database.type(db.type)              : Database type
  database.primary_host(db.pri_host)  : Database primary_host
  database.primary_port(db.pri_port)  : Database primary_port
  database.second_host(db.sec_host)   : Database second_host
  database.second_port(db.sec_port)   : Database second_port
  database.user(db.user)              : Database user
  database.password(db.psswd)         : Database password
  database.sid(db.sid)                : Database sid
  database.encode(db.encode)          : Database Encoding

  ** Can be changed to option name in () when modifiying
  ** database.encode cannot be modified
 
 [Result]
  <SUCCESS>
  Date time
  MMC = DIS-DBCONF-INFO
  Result = SUCCESS
  ====================================================
  database.type          : value
  database.primary_host  : value
  database.primary_port  : value
  database.second_host   : value
  database.second_port   : value
  database.user          : value
  database.password      : value
  database.sid           : value
  database.encode        : value
  ====================================================
  
  <FAILURE>
  Date time
  MMC = DIS-DBCONF-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body

def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	#ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"
	else :
		return True, ARG_CNT, None, ""

def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/dbconf.conf)
			db_config = ConfigParser.RawConfigParser()
			db_config.read(G_db_cfg_path)
			
			# make argument list (parsing and check validation)
			if len(ARG) is not 0:
				org_list = ['']
				ret, ARG_CNT, Parsing_Dic, reason = Arg_Parsing(ARG, org_list, DIS_DBCONF_INFO_MAX_PARAMETER, DIS_DB_CONF_INFO_MIN_PARAMETER)
				if ret == False :
					result = 'FAILURE'
					return make_result(MMC, ARG, result, reason, total_body)
			else :
				db_convert = {}
				db_convert["db.type"] = "type"
				db_convert["db.pri_host"] = "primary_host"
				db_convert["db.pri_port"] = "primary_port"
				db_convert["db.sec_host"] = "second_host"
				db_convert["db.sec_port"] = "second_port"
				db_convert["db.user"] = "user"
				db_convert["db.passwd"] = "password"
				db_convert["db.sid"] = "sid"
				db_convert["db.encode"] = "encode"

				data = OrderedDict()	
				org_item_value = db_config.items('DBCONF')
				for i in range(len(org_item_value)) :
					data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(data))
				
				row = '' 
				for item in data :
					if (row == ''):
						row = '{0:15} = {1}'.format(db_convert[item], data[item])
					else : 
						row = '\n{0:15} = {1}'.format(db_convert[item], data[item])
					total_body = total_body + row

			result='SUCCESS'
			reason=''
			G_log.info('DIS_DBCONF_CONF() Complete!!')
			return make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-DBCONF-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error("DIS-DBCONF-INFO(), Config read error: [{}]".format(e))
		reason='DIS-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS_DBCONF_CONF(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC,result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help') :
		msg_body = "{}".format(total_body)
	else :
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)

		else :
			msg_body = """
======================================
{}
======================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg
