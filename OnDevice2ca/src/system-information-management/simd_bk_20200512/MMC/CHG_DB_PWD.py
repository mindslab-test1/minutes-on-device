#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import traceback
import bcrypt
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
===========================================================
 {} = mandatory, () = optional
===========================================================
 [Usage]
 	1. CHG-DB-PWD {ID = PW}
	  - ID to change and password to change
	2. CHG-DB-PWD {ID}, {PW}
	  - ID to change and password to change

	** ID : ID to change
	** PW : Password to change
 
 [Result]
 	<SUCCESS>
	Date time
	MMC    = CHG-DB-PWD
	Result = SUCCESS
	====================================================
	[Before]
		id = password before change
	[After]
		id = password after change
	====================================================

 	<FAILURE>
	Date time
	MMC = CHG-DB-PWD
	Result = FAILURE
	====================================================
	Reason = Reason for error
	====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG, log) :
	ORG_LIST = ["ID", "PW"]
	ARG_CNT = len(ARG)

	#### ID, PASSWORD 는 한번에 하나씩만 변경 가능하다.
	if ((ARG_CNT > len(ORG_LIST)) or (ARG_CNT == 0)):
		return validation_check_result(False, 0, {})
	
	Parsing_Dict = OrderedDict()

	###### ID = PW 들어온 경우
	if ('=' in ARG[0]) :
		if (ARG_CNT != 1) :
			return validation_check_result(False, 0, {})
		else :
			ID, PWD = ARG[0].split('=')
			Parsing_Dict[ID] = PWD
	
	###### ID, PW 들어온 경우
	else :
		if (ARG_CNT != 2) :
			return validation_check_result(False, 0, {})
		else :
			Parsing_Dict[ARG[0]] = ARG[1]

	for ID in Parsing_Dict :
		print("Parsing :: {} = {}" .format(ID, Parsing_Dict[ID]))
		if (Parsing_Dict[ID] == ''):
			return validation_check_result(False, 1, ID)

	return validation_check_result(True, ARG_CNT, Parsing_Dict)

def proc_exec(log, mysql, MMC, ARG):
	total_body=''
	try :
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, log)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				for ID in Parsing_Dict :
					DB_data = DIS_Query(mysql, "MINUTES_USER", "USER_PW", "where USER_ID = '{}'".format(ID))
					log.info('db_data = {}'.format(DB_data))
					if not DB_data :
						result="FAILURE"
						reason="ID doesn't exist in Database"
						return make_result(MMC, ARG, result, reason, total_body)
					elif (DB_data == ''): 
						result="FAILURE"
						reason="DIS_Query Error"
						return make_result(MMC, ARG, result, reason, total_body)

					else: 
						row = '[Before]'
						total_body = total_body + row
						row = '\t{} = {}' .format(ID, DB_data[0]['USER_PW'])
						total_body = total_body + '\n' + row
		
						##### PWD 암호화
						PWD=b"{}".format(Parsing_Dict[ID])
						DB_data[0]['USER_PW']=bcrypt.hashpw(PWD, bcrypt.gensalt(rounds=10, prefix=b"2a"))

						########### DB 정보 update
						update_query = Update_Query(DB_data[0], ID, mysql)
						if not update_query :
							result="FAILURE"
							reason="Update_Query Error"
							return make_result(MMC, ARG, result, reason, total_body)
						else :
							pass

						row = '[After]'
						total_body = total_body + '\n' + row
						DB_data = DIS_Query(mysql, "MINUTES_USER", "USER_PW", "where USER_ID = '{}'".format(ID))
						row = '\t{} = {}' .format(ID, DB_data[0]['USER_PW'])
						total_body = total_body + '\n' + row
	
						return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		log.error('CHG_DB_PWD(), ERROR Occured [{}]' .format(e))
		log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
======================================
 {}
======================================
""".format(reason)
		else:	
			msg_body = """
======================================
{}
======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
		return ''

def Update_Query(DB_data, ID, mysql):

	try :
		sql = "update MINUTES_USER set USER_PW='{}' where USER_ID = '{}';".format(DB_data['USER_PW'], ID)
		mysql.execute(sql, True)
		return True

	except Exception as e :
		log.error('DB Update Error : {}'.format(e))
		log.error(traceback.format_exc())
		return False
