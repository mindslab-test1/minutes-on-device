#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict

def mmc_help():
	total_body = """
	===========================================================
	 {} = mandatory, () = optional
	===========================================================
	 [Usage]
		DIS-PRC-INFO (MINUTES_PRICING_SER)	
	 	1. DIS-PRC-INFO 
		  - View all
		2. DIS-PRC-INFO (MINUTES_PRICING_SER)	
		  - View specific MINUTES_PRICING_SER information
		  
	 [Column information]
	 	MINUTES_PRICING_SER : Serial number
		CREATE_USER         : Create user
		CREATE_TIME         : Create time
		UPDATE_USER         : Change user
		UPDATE_TIME         : Change time
		PRICING_LEVEL       : Pricing class name
		PRICING_NAME        : Pricing class name
		PERIOD_DAY          : Pricing class period 
		USE_TIME            : Audio file time available for minutes (unit : minute)
		MAU                 : Usage fee
		DESC                : Explain cost

	 [Result]
	 	<SUCCESS>
			Date time
			MMC    = DIS-PRC-INFO 
			Result = SUCCESS
			====================================================
			MINUTES_PRICING_SER : CREATE_USER | CREATE_TIME | UPDATE_USER | UPDATE_TIME | PRICING_LEVEL | PRICING_NAME | PRICING_DAY | USE_TIME | MAU   | DESC
			value               : value       | value       | value       | value       | value         | value        | value       | value    | value | value
				                   ...
			====================================================

	 	<FAILURE>
			Date time
			MMC    = DIS-PC-INFO
			Result = FAILURE
			====================================================
			Reason = Reason for error
			====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Parsing_Dict):
	if ((ret == False) and (ARG_CNT == 0)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 1)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 2)):
		reason = "The input argument structure is mixed"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 3)):
		reason = "Column does not exist [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 4)):
		reason = "Duplicate error [{}]".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (ARG_CNT == 5)):
		if ((Parsing_Dict == 'PRICING_LEVEL') or (Parsing_Dict == 'PERIOD_DAY') or (Parsing_Dict == 'USE_TIME') or (Parsing_Dict == 'MAU'):
			reason = "Value error[{}] (Must be a positive integer)".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		elif (Parsing_Dict == 'DESC'):
			reason = "Value error[{}] (Value must be within 100 characters)".format(Parsing_Dict)
			return ret, ARG_CNT, Parsing_Dict, reason
		else :
			pass
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG) :
	org_list = ["PRICING_LEVEL", "PRICING_NAME", "PRICING_DAY", "USE_TIME", "MAU", "DESC"]
	org_list2 = ["PRICING_LEVEL", "PRICING_NAME", "PRICING_DAY", "USE_TIME", "MAU"]
	prc_class = ["MAUM", "BASIC", "PRO", "PREMIUM"]
	ARG_CNT = len(ARG)
	if ((ARG_CNT == len(org_list)) or (ARG_CNT == (len(org_list) - 1)):	
		Parsing_Dict = OrderedDict()
		for option in org_list :
			Parsing_Dict[option] = None

		if ('=' in ARG[0]):
			for list_num in range(ARG_CNT):
				if ('=' not in ARG[list_num]):
					return validation_check_result(False, 2, {})
				else :
					option, value = ARG[list_num].split('=',1)
					if option not in org_list :
						return validation_check_result(False, 3, option)
					else :
						if (Parsing_Dict[option] != None):
							return validation_check_result(False, 4, option)
						else :
							Parsing_Dict[option] = value
		else :
			for list_num in range(ARG_CNT):
				if ('=' in ARG[list_num]):
					return validation_check_result(False, 2, {})
				else :
					if (ARG_CNT == len(org_list)):
						Parsing_Dict[org_list[list_num]] = ARG[list_num]
					elif (ARG_CNT == (len(org_list)-1)):
						Parsing_Dict[org_list2[list_num]] = ARG[list_num]
					else :
						return validation_check_result(False, 0, {})

		for ItemName in Parsing_Dict :
			print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
			if (Parsing_Dict[ItemName] == None):
				pass
			elif (Parsing_Dict[ItemName] == ''):
				return validation_check_result(False, 1, ItemName)
			else :
				if ((ItemName == 'PRICING_LEVEL') or (ItemName == 'PERIOD_DAY') or (ItemName == 'USE_TIME') or (ItemName == 'MAU')):
					prc_num = re.findall("\d+", Parsing_Dict[ItemName])
					if ((len(prc_num) != 1) or (Parsing_Dict[ItemName] != prc_num) or (int(Parsing_Dict[ItemName]) <= 0)):
						return validation_check_result(False, 5, ItemName)
					else :
						pass
				elif (ItemName == 'DESC')
					if (len(Parsing_Dict[ItemName] > 100):
						return validation_check_result(False, 5, ItemName)
					else :
						pass
				else :
					pass			
		return validation_check_result(True, ARG_CNT, Parsing_Dict)

	else :
		return validation_check_result(False, 0, {})

def proc_exec(MMC, ARG, mysql, root_path, simd_cfg_path, proc_name):
	total_body=''
	try :
		#global G_log
		#G_log = logger.create_logger(root_path + '/logs', 'simd', 'debug', True, False)

		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		
		else :
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return make_result(MMC, ARG, result, reason, '')

			else :
				## DB connection
				#global G_mysql
				#G_mysql = mysql
				#G_mysql = DBLib(G_log)
				#dbconn = G_mysql.connect()

			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG)
			
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			else :
				insert_query = insert_query("MINUTES_PRICING", ARG_CNT, Parsing_Dict, proc_name)

				else :
					db_data = DIS_Query("MINUTES_PRICING", "*", "where MINUTES_PRICING_SER = '{}'".format(Parsing_Dict["MINUTES_PRICING_SER"]))

				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)

				else :
					total_body="""\n\t {:19} : {:11} | {:11}| {:11} | {:11} | {:13} | {:12} | {:11} | {:8} | {:3} | {}\n""" .format('MINUTES_PRICING_SER', 'CREATE_USER', 'CREATE_TIME', 'UPDATE_USER', 'UPDATE_TIME', 'PRICING_LEVEL', 'PRICING_NAME', 'PRICING_DAY', 'USE_TIME', 'MAU', 'DESC')

					total_body= total_body + '\t' + '-'*150
					for thr_num in range(len(db_data)) :
						pri_ser=db_data[thr_num]["MINUTES_PRICING_SER"]
						create_user=db_data[thr_num]["CREATE_USER"]
						create_time=db_data[thr_num]["CREATE_TIME"]
						update_user=db_data[thr_num]["UPDATE_USER"]
						update_time=db_data[thr_num]["UPDATE_TIME"]
						prc_level=db_data[thr_num]["PRICING_LEVEL"]
						prc_name=db_data[thr_num]["PRICING_NAME"]
						prc_day=db_data[thr_num]["PRICING_DAY"]
						use_time=db_data[thr_num]["USE_TIME"]
						mau=db_data[thr_num]["MAU"]
						desc=db_data[thr_num]["DESC"]

						row_buf="\n\t {:^19} : {:^11} | {:^11}| {:^11} | {:^11} | {:^13} | {:^12} | {:^11} | {:^8} | {:^3} | {}\n" .format(pri_ser, create_user, create_time, update_user, update_time, prc_level, prc_name, prc_day, use_time, mau, desc)
						total_body = total_body + row_buf 

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DIS-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DIS-PRC-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DIS-PRC-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
\t{}
\tMMC    = {}
\tRESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="\t{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
\t======================================
\t {}
\t======================================
""".format(reason)
		else:	
			msg_body = """
\t======================================
{}
\t======================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(table, column, where):

	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = G_mysql.execute_query2(sql)

		return rows

	except Exception as e :
		print('DIS_Query error : {}'.format(e))
		print(traceback.format_exc())
		return ''

def insert_query(table, ARG_CNT, Parsing_Dict, proc_name)
	try :
		sql = "insert into {}(
