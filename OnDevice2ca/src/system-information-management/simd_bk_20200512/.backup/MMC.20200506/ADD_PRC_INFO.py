#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

ADD_PRC_INFO_MIN_PARAMETER = 5
ADD_PRC_INFO_MAX_PARAMETER = 6
MIN_PRICING_LEVEL = 1
MAX_PRICING_LEVEL = 5

param_list = ["PRICING_NAME", "PRICING_LEVEL", "PERIOD_DAY", "USE_TIME", "MAU", "DESCRIPT"]
mandatory_list = ["PRICING_NAME", "PRICING_LEVEL", "PERIOD_DAY", "USE_TIME", "MAU"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  ADD-PRC-INFO [PRICING_NAME=a], [PRICING_LEVEL=b], [PERIOD_DAY=c], [USE_TIME=d], [MAU=e], (DESCCRIPT=f)

 [Parameter]
  a = STRING      (1:100)
  b = ENUM        (1|2|3|4)
  c = DECIMAL     (1:9999999999)
  d = DECIMAL     (1:9999999999)
  e = DECIMAL     (1:9999999999)
  f = STRING      (1:100)

 [Usage]
  ADD-PRC-INFO [a, b, c, d, e, f]
   ex) ADD-PRC-INFO MAUM, 2, 7, 3000, 1, APPLE
  ADD-PRC-INFO [PRICING_NAME=a], [PRICING_LEVEL=b], [PERIOD_DAY=c], [USE_TIME=d], [MAU=e], (DESCRIPT=f)
   ex) ADD-PRC-INFO PRICING_NAME=MAUM, PRICING_LEVEL=2, PERIOD_DAY=7, USE_TIME=3000, MAU=1, DESCRIPT=APPLE
  
 [Column information]
  PRICING_NAME        : Pricing class name
  PRICING_LEVEL       : Pricing class level
  PERIOD_DAY          : Pricing class period
  USE_TIME            : Audio file time available for minutes
  MAU                 : Usage fee
  DESCRIPT            : Explain cost

 [Result]
 <SUCCESS>
 Date time
 MMC    = ADD-PRC-INFO 
 Result = SUCCESS
 ==================================================================================================
         PRICING_NAME        | PRICING_LEVEL | PERIOD_DAY | USE_TIME |   MAU   | DESCRIPT
 --------------------------------------------------------------------------------------------------
   value                     | value         |      value |    value |   value | value
   value                     | value         |      value |    value |   value | value
 ...                             
 ==================================================================================================

 <FAILURE>
 Date time
 MMC    = ADD-PRC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['PRICING_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PRICING_NAME', arg_data['PRICING_NAME'], G_log)
		if (ret == False) :
			return False, reason
	
	if arg_data['PRICING_LEVEL'] != None :
		ret, reason = check_Decimal_And_Range(MIN_PRICING_LEVEL, MAX_PRICING_LEVEL, 'PRICING_LEVEL', arg_data['PRICING_LEVEL'], G_log)
		if (ret == False) : 
			return False, reason

	if arg_data['PERIOD_DAY'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'PERIOD_DAY', arg_data['PERIOD_DAY'], G_log)
		if (ret == False) : 
			return False, reason
	
	if arg_data['USE_TIME'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'USE_TIME', arg_data['USE_TIME'], G_log)
		if (ret == False) : 
			return False, reason

	if arg_data['MAU'] != None :
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'MAU', arg_data['MAU'], G_log)
		if (ret == False) : 
			return False, reason

	##### Optional Parameter #####
	if arg_data['DESCRIPT'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'DESCRIPT', arg_data['DESCRIPT'], G_log)
		if (ret == False) :
			return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, ADD_PRC_INFO_MAX_PARAMETER, ADD_PRC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# insert new pricing information
			try :
				total_body = ''

				insert_query = """ insert into MINUTES_PRICING values (NULL, 'simd', now(), NULL, NULL, '{}', '{}', {}, {}, {}, '{}') """.format(Parsing_Dict['PRICING_LEVEL'],Parsing_Dict['PRICING_NAME'],Parsing_Dict['PERIOD_DAY'],Parsing_Dict['USE_TIME'],Parsing_Dict['MAU'],Parsing_Dict['DESCRIPT'])

				ret = mysql.execute(insert_query, False)
				if (ret == False) :
					result='FAILURE'
					reason='DB Insert Falure'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('ADD-PRC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB INSERT FALURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# check inserted information
			db_data = DIS_Query(mysql, "MINUTES_PRICING", "*" , ';')
			if not db_data :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				total_body=""" {:^16} | {:^15} | {:^12} | {:^11} | {:^11} | {}\n""" .format('PRICING_NAME', 'PRICING_LEVEL', 'PERIOD_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')

				total_body = total_body + '-' * 120
				for thr_num in range(len(db_data)) :
					create_user = db_data[thr_num]["CREATE_USER"]
					create_time = db_data[thr_num]["CREATE_TIME"]
					update_user = db_data[thr_num]["UPDATE_USER"]
					update_time = db_data[thr_num]["UPDATE_TIME"]
					prc_level = db_data[thr_num]["PRICING_LEVEL"]
					prc_name = db_data[thr_num]["PRICING_NAME"]
					prc_day = db_data[thr_num]["PERIOD_DAY"]
					use_time = db_data[thr_num]["USE_TIME"]
					mau = db_data[thr_num]["MAU"]
					desc = db_data[thr_num]["DESCRIPT"]
					row_buf="\n {:16} | {:15} | {:12} | {:11} | {:11} | {}" .format(prc_name, prc_level, prc_day, use_time, mau, desc)
					total_body = total_body + row_buf 

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("ADD-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='ADD-PRC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("ADD-PRC-INFO(), Config read error: [{}]".format(e))
		reason='ADD-PRC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='ADD-PRC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('ADD-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='ADD-PRC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt));
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''
