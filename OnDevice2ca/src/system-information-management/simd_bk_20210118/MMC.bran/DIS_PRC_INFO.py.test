#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *

SUCCESS = 0000
ERROR_ARG_INVALID_COUNT = 1000
ERROR_ARG_INVALID_VALUE_FORM = 2001
ERROR_ARG_INVALID_VALUE_RANGE = 2002
ERROR_ARG_INVALID_VALUE_LEN = 2003
ERROR_ARG_INVALID_TYPE = 3000

def mmc_help():
	total_body = """
===========================================================
 {} = mandatory, () = optional
===========================================================
 [Command]
	DIS-PRC-INFO [PRICING_NAME=a]
 
 [Parameter]
 a = STRING (1:100)
	  
 [Column information]
 	MINUTES_PRICING_SER : Serial number
	CREATE_USER         : Create user
	CREATE_TIME         : Create time
	UPDATE_USER         : Change user
	UPDATE_TIME         : Change time
	PRICING_LEVEL       : Pricing class name
	PRICING_NAME        : Pricing class name
	PERIOD_DAY          : Pricing class period 
	USE_TIME            : Audio file time available for minutes (unit : minute)
	MAU                 : Usage fee
	DESCRIPT            : Explain cost

 [Result]
 	<SUCCESS>
	Date time
	MMC    = DIS-PRC-INFO 
	Result = SUCCESS
	========================================================================================================================
	 MINUTES_PRICING_SER | PRICING_LEVEL |       PRICING_NAME        | PERIOD_DAY | USE_TIME |   MAU   | DESCRIPT
	------------------------------------------------------------------------------------------------------------------------
	             value   | value         | value                     |      value |    value |   value | value
	========================================================================================================================

 	<FAILURE>
	Date time
	MMC    = DIS-PC-INFO
	Result = FAILURE
	====================================================
	Reason = Reason for error
	====================================================
"""
	return total_body

def validation_check_result(ret, ARG_CNT, Err_Code, Parsing_Dict):
	if ((ret == False) and (Err_Code == ERROR_ARG_INVALID_COUNT)):
		reason = "The number of input arguments is not correct"
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (Err_Code == ERROR_ARG_INVALID_VALUE_FORM)):
		reason = "Value error[{}] (Don't include spaces in the value)".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	elif ((ret == False) and (Err_Code == ERROR_ARG_INVALID_VALUE_RANGE)):
		reason = "Value error[{}] (Must be an integer greater than or equal to '0')".format(Parsing_Dict)
		return ret, ARG_CNT, Parsing_Dict, reason
	else :
		reason = ''
		return ret, ARG_CNT, Parsing_Dict, reason

def Arg_Parsing(ARG) :

	ARG_CNT = len(ARG)
	if (ARG_CNT > 1): # only PRICING_NAME
		return validation_check_result(False, ARG_CNT, ERROR_ARG_INVALID_COUNT, {})
	else :	
		Parsing_Dict = OrderedDict()
		Parsing_Dict["PRICING_NAME"] = None

		if (ARG_CNT != 0):
			Parsing_Dict["PRICING_NAME"] = ARG[0]
		else :
			pass # print all
		
		for ItemName in Parsing_Dict :
			print("Parsing :: {} = {}" .format(ItemName, Parsing_Dict[ItemName]))
			if (Parsing_Dict[ItemName] == None):
				pass
			elif (Parsing_Dict[ItemName] == ''): # value is 'blank'
				return validation_check_result(False, ARG_CNT, ERROR_ARG_INVALID_VALUE_FORM, ItemName)
			elif (ItemName != 'PRICING_NAME'):
				return validation_check_result(False, ARG_CNT, ERROR_ARG_INVALID_TYPE, ItemName)
			else :
				if (ItemName == 'PRICING_NAME'):
					value = Parsing_Dict[ItemName])
					G_log.debug('PRICING_NAME : {}'.format(value))
					if len(value) > 100:
						return validation_check_result(False, ARG_CNT, ERROR_ARG_INVALID_VALUE_LEN, ItemName)
				else :
					pass			
		return validation_check_result(True, ARG_CNT, SUCCESS, Parsing_Dict)

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG)
			if (ret == False) :
				result='FAILURE'
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
			else :
				if (ARG_CNT == 0): # select all
					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*", ';')
				else :
					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*", "where PRICING_NAME = '{}'".format(Parsing_Dict["PRICING_NAME"]))

				# Not exist information
				if not db_data :
					result = 'FAILURE'
					reason = "DB_data does not exist"
					return make_result(MMC, ARG, result, reason, total_body)
				else :
					total_body="""\n {:^21} | {:^15} | {:^16} | {:^12} | {:^11} | {:^11} | {}\n""" .format('MINUTES_PRICING_SER', 'PRICING_LEVEL', 'PRICING_NAME', 'PERIOD_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')

					total_body= total_body + '' + '-'*120
					for thr_num in range(len(db_data)) :
						pri_ser=db_data[thr_num]["MINUTES_PRICING_SER"]
						create_user=db_data[thr_num]["CREATE_USER"]
						create_time=db_data[thr_num]["CREATE_TIME"]
						update_user=db_data[thr_num]["UPDATE_USER"]
						update_time=db_data[thr_num]["UPDATE_TIME"]
						prc_level=db_data[thr_num]["PRICING_LEVEL"]
						prc_name=db_data[thr_num]["PRICING_NAME"]
						prc_day=db_data[thr_num]["PERIOD_DAY"]
						use_time=db_data[thr_num]["USE_TIME"]
						mau=db_data[thr_num]["MAU"]
						desc=db_data[thr_num]["DESCRIPT"]

						row_buf="\n {:21} | {:15} | {:16} | {:12} | {:11} | {:11} | {}\n" .format(pri_ser, prc_level, prc_name, prc_day, use_time, mau, desc)
						total_body = total_body + row_buf 

					return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DIS-PRC-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DIS-PRC-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DIS-PRC-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body ="{}".format(total_body)
	
	else:
		if (result == 'FAILURE'):
			msg_body = """
========================================================================================================================
 {}
========================================================================================================================
""".format(reason)
		else:	
			msg_body = """
========================================================================================================================
{}
========================================================================================================================
""".format(total_body)
	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result
	
	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(ARG, result, reason, total_body)
	data = msg_header + msg_body 
	result_msg['msg_body']['data'] = data
	return result_msg

def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt));
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

