[ JDK 설치 for Konlpy ]

 $ yum list java*jdk-devel
 $ sudo yum install java최신버전

>> 자바 설치 위치 확인
 $ readlink -f /usr/bin/java

 확인된 자바 경로 환경 설정 추가 (.bashrc)
 export JAVA_HOME=확인된 자바 경로

 $ source .bashrc
 $ echo $JAVA_HOME

[ Konlpy 필수 사전 설치 패키지 ]
 
  $ sudo yum install libxslt-devel
  $ sudo yum install libxml2-devel

[ Konlpy 설치 ]

  $ pip3 install konlpy

