#!/usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, json
import re
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser
from konlpy.tag import Okt
from collections import Counter
import csv

#####################################################################################
# MINDSLAB COMMON
####################################################################################
import common.logger as logger
from common.dblib import DBLib
import common.minds_ipc as IPC
import common.minutes_sql as SQL
import common.simd_config as simd_conf

g_var = {}
log = ''

# get_keyword_rank(string, int, list)
#  - return 'Ordered List'
def get_keyword_rank(phrase, rank_range, except_list):
	
	print ("Rank Range : {}".format(rank_range))

	# create 'Okt' object
	okt = Okt()
	noun_set = okt.nouns(phrase)
	#print(noun_set)

	# except exception keyword
	final_noun_set = []
	for noun in noun_set:
		if noun not in except_list:
			final_noun_set.append(noun)

	# count by nouns
	count = Counter(final_noun_set)

	return count.most_common(int(rank_range)) # most_common() makes ordered 'List'
	

def load_config():

	global g_var

	g_var['proc_name'] = os.path.basename(sys.argv[0])

	proc_name = g_var['proc_name'].upper()

	g_conf_path = os.getenv('HOME') + '/MP/etc/process_info.conf'
	conf = ConfigParser.ConfigParser()
	conf.read(g_conf_path)

	items = conf.items(proc_name)
	for name, value in items :
		g_var[name] = value

	# check config
	for i in g_var :
		print("{} = {}".format(i, g_var[i]))

	return


def init_process():

	global g_var

	try:
		# Init Config
		try:
			load_config()

		except Exception as e :
			print ("init_process() : load_config() failure. <%s>", e)
			print (traceback.format_exc())
			return False, None, None, None, None


		# Init Logger()
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', g_var['proc_name'], g_var['log_level'], True)

		# Init DB
		mysql = DBLib(log)
		db_conn = mysql.connect()

		log.debug("init_process() : DB connect() Success.")

		# Init IPC
		ipc = IPC.MindsIPCs(log, g_var['proc_name'])
		ret = ipc.IPC_Open()
		if ret == False:
			log.critical("init_process() : IPC Open() Failure.")
			return False, None, None, None, None

		ret = ipc.IPC_Regi_Process('simd')
		if ret == False :
			log.critical("init_process() : IPC Regi Process() Failure.")
			return False, None, None, None, None

		log.debug("init_process() : IPC Init() Success.")

		# Loading Exception Keyword
		except_list = []
		with open(os.getenv('HOME') + '/MP/etc/except_keyword.txt', "r") as f :
			for keyword in f:
				except_list.append(keyword.rstrip('\n'))

		# check keyword list
		print (except_list)
	
	except Exception as e:
		log.critical("main(): init_process() exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, None, None, None, None
	else:
		return True, mysql, log, ipc, except_list

def process_ipc_msg(msg):
	
	log.critical("process_ipc_msg() : receive 'CHG-LOG-LEVEL'")

	json_msg = json.loads(msg)

	if 'msg_header' not in json_msg or 'msg_body' not in json_msg :
		log.critical("process_ipc_msg(): received Invalid Json msg! [{}]".format(json_msg))
		return

	elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
		conf = simd_conf.Proc_Conf(g_var['proc_name'].upper())
		log.critical("== changed log level : {} => {}".format(g_var['log_level'], conf.get_loglevel()))
		if g_var['log_level'] != conf.get_loglevel() :
			g_var['log_level'] = conf.get_loglevel()
			logger.change_logger_level(log, g_var['log_level'])
		return
	else:
		log.critical("process_ipc_msg() : received UNKNOWN Message [{}]".format(json_msg))


def main():
	
	global log

	# Init Process
	result, mysql, log, ipc, except_list = init_process()
	if result == False:
		print ("[MRTd] Starting Failure.")
		sys.exit(1)

	log.critical("[MRTd] Starting.....")

	# main loop
	while True:

		index = 0
		try:
			time.sleep(int(g_var['detect_interval']))

			# Process IPC Message => CHG-LOG-LEVEL
			msg = ipc.IPC_Recv()
			if msg :
				process_ipc_msg(msg)

			######################### EVENT COLLECTION #######################
			ret, rowcnt, rows, error = SQL.select_stt_meta_bystatus_with_textrank(mysql, SQL.META_STAT_REALTIME_COMPLETE, SQL.META_STAT_COMPLETE, SQL.TEXT_RANK_STATUS_NONE)
			if (ret == False):
				log.critical("select_stt_meta_bystatus_with_textrank() Failure. error [{}]".format(error))
			else:
				log.info("select_stt_meta_bystatus_with_textrank() => count [{}]".format(rowcnt))
				if (int(rowcnt) <= 0):
					continue

				# get detected info & update status
				for row in rows :
					index = index + 1
					# update 'TEXT_RANK_STATUS' => ING ('1')
					u_ret, u_error = SQL.update_stt_meta_textrank_status(mysql, row['STT_META_SER'], SQL.TEXT_RANK_STATUS_ING)
					if u_ret == False:
						log.critical("update_stt_meta({})_textrank_status(ING) Failure. error [{}]".format(row['STT_META_SER'], u_error))
						continue

					# get Attendees info
					log.info('')
					log.info("No.{} - MINUTES_ID [{}] JOINED COUNT [{}] JOINED MEMBER [{}]".format(index, row['STT_META_SER'], row['MINUTES_JOINED_CNT'], row['MINUTES_JOINED_MEM']))

					attendee_list = (row['MINUTES_JOINED_MEM']).split(',', int(row['MINUTES_JOINED_CNT'])-1)
					# make sentence stuff by talker
					for attendee in attendee_list:
						rank = []
						attendee = attendee.strip()
						log.info("== Text Rank Processing....[{}]".format(attendee))
						t_ret, t_rowcnt, t_rows, t_error = SQL.select_stt_result_by_talker(mysql, row['STT_META_SER'], attendee)
						if (t_ret == False):
							log.critical("select_stt_result_by_talker({}) Failure. stt_meta [{}] error [{}]".format(attendee, row['STT_META_SER'], t_error))
							pass
						elif (int(t_rowcnt) > 0):
							# get talked sentences
							phrase = ''
							for t_row in t_rows:
								if t_row['STT_CHG_RESULT'] != None:
									using_row = t_row['STT_CHG_RESULT']
								else:
									using_row = t_row['STT_ORG_RESULT']
								using_row = using_row.strip()
								#log.info("sentence [{}]".format(using_row))
								# make phrase type
								phrase = phrase + '\n' + using_row

							# get Text Rank ~~ :)
							text_rank = get_keyword_rank(phrase, g_var['rank_range'], except_list)
							for text in text_rank:
								rank.append(str((re.findall('\'([^\']*)\'',str(text)))[0]))

							# insert Text Rank Info
							i_ret, i_error = SQL.insert_textrank_keyword(mysql, row['STT_META_SER'], attendee, rank)
							if (i_ret == False) :
								log.critical("insert_textrank_keyword({}) Failure. stt_meta [{}] error[{}]".format(attendee, row['STT_META_SER'], i_error))
								# restore text rank status
								iu_ret, iu_error = SQL.update_stt_meta_textrank_status(mysql, row['STT_META_SER'], SQL.TEXT_RANK_STATUS_NONE)
								if iu_ret == False:
									log.critical("update_stt_meta_textrank_status(RESTORE) Failure. stt_meta [{}] error [{}]".format(row['STT_META_SER'], iu_error))
									continue
						elif (int(t_rowcnt) == 0):
							# insert empty data
							ei_ret, ei_error = SQL.insert_textrank_keyword(mysql, row['STT_META_SER'], attendee, rank)
							if (ei_ret == False) :
								log.critical("EMPTY :: insert_textrank_keyword({}) Failure. stt_meta [{}] error[{}]".format(attendee, row['STT_META_SER'], ei_error))
								# restore text rank status
								eiu_ret, eiu_error = SQL.update_stt_meta_textrank_status(mysql, row['STT_META_SER'], SQL.TEXT_RANK_STATUS_NONE)
								if eiu_ret == False:
									log.critical("update_stt_meta_textrank_status(RESTORE) Failure. stt_meta [{}] error [{}]".format(row['STT_META_SER'], eiu_error))

					# update 'TEXT_RANK_STATUS' => COMPLETE ('2')
					ret, error = SQL.update_stt_meta_textrank_status(mysql, row['STT_META_SER'], SQL.TEXT_RANK_STATUS_END)
					if ret == False:
						log.critical("update_stt_meta_textrank_status(END) Failure. stt_meta[{}] error [{}]".format(row['STT_META_SER'], error))
						pass

		except Exception as e:
			log.critical("main(): exception raise fail. <%s>", e)
			log.critical(traceback.format_exc())


	log.critical("main() stopped.")

	ipc.IPC_Close()
	mysql.disconnect()
	
	return True

if __name__ == '__main__':
	main()
