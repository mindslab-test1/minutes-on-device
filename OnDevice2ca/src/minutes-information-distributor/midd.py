#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, io, time, json
import logging, logging.handlers
from multiprocessing import Process, Value, Array
import socket
import uuid
import ssl
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser

#####################################################################################
# PYTHON COMMON
####################################################################################
#from common.config import Config
#import common.simd_config as simd_conf
import common.logger as logger
import common.minds_ipc as IPC
from common.dblib import DBLib
import common.minutes_sql as SQL
from common.def_val import *


#####################################################################################
# PYTHON COMMON
####################################################################################
# IPC Module : https://pypi.org/project/pyzmq
#import zmq
# WebSocket Server : https://pypi.org/project/tornado
import tornado.web
import tornado.websocket
from tornado.websocket import websocket_connect
import tornado.ioloop
# WebSocket Client : https://pypi.org/project/websocket-client
import websocket

#환경변수
mysql=None
g_var = {}
g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'

def get_Realtime_Working_META() :
	meta_list=list()

	result, rowcnt, rows, error =SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_WORKING)
	if(result == False) :
		log.critical('Select STT_META Failure [{}]' .format(error))

	else :
		for row in rows :
			stt_meta=row
			if stt_meta['MINUTES_TYPE'] == SQL.META_TYPE_REALTIME :
				#log.critical(stt_meta)
				#if 'CREATE_TIME' in stt_meta:
				#	if stt_meta['CREATE_TIME'] :
				#		stt_meta['CREATE_TIME']=stt_meta['CREATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')
				#if 'UPDATE_TIME' in stt_meta:
				#	if stt_meta['UPDATE_TIME'] :
				#		stt_meta['UPDATE_TIME']=stt_meta['UPDATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')

				meta_list.append(stt_meta)

	return meta_list


class WS_Handler(tornado.websocket.WebSocketHandler):
	clients = set()

	def open(self):
		if self not in self.clients:
			self.id = uuid.uuid4()
			self.clients.add(self)
			log.critical("WS_Handler::open(%s) client connected", self.id)

			#meta_list=get_Realtime_Working_META()
			##log.critical("[CONNECTION NOTI] META LIST : {}".format(meta_list))
			#for meta in meta_list :
			#	container=dict()
			#	container['MINUTES_NAME']=meta['MINUTES_NAME']
			#	container['TOPIC']=meta['MINUTES_TOPIC']
			#	container['MEETINGROOM']=meta['MINUTES_MEETINGROOM']
			#	container['START_TIME']=meta['START_TIME']
			#	container['MIC_INFO']=SQL.make_mic_info_str(mysql, meta)
#
#				send_msg=IPC.MAKE_Event_Collection_Distribute(meta['STT_META_SER'], '#START#', None, None, None, None, None, container)
#
#				json_string = json.dumps(send_msg, ensure_ascii=False)
#				log.critical("[CONNECTION NOTI] {}".format(json_string))
#				json_encode= json_string.encode('utf-8')
#				self.write_message(json_encode)
	
	def on_close(self):
		if self in self.clients:
			self.clients.remove(self)
			log.critical("WS_Handler::on_close(%s) client removed", self.id)
	
	def on_message(self, msg):
		log.info("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		json_msg = json.loads(msg)
		if 'msg_header' in json_msg :
			if 'msg_id' in json_msg['msg_header'] :
				if json_msg['msg_header']['msg_id']=='REQ_MIC_LOGIN' :
					meta_list=get_Realtime_Working_META()
					#log.critical("[CONNECTION NOTI] META LIST : {}".format(meta_list))
					for meta in meta_list :
						container=dict()
						container['MINUTES_NAME']=meta['MINUTES_NAME']
						container['TOPIC']=meta['MINUTES_TOPIC']
						container['MEETINGROOM']=meta['MINUTES_MEETINGROOM']
						container['START_TIME']=meta['START_TIME']
						container['MIC_INFO']=SQL.make_mic_info_str(mysql, meta)
						if meta['MUTE_FLAG'] == '1' :
							container['MUTE_FLAG']='ON'
						else :
							container['MUTE_FLAG']='OFF'

						send_msg=IPC.MAKE_Event_Collection_Distribute(meta['STT_META_SER'], '#START#', None, None, None, None, None, container)

						json_string = json.dumps(send_msg, ensure_ascii=False)
						log.critical("[CONNECTION NOTI] {}".format(json_string))
						json_encode= json_string.encode('utf-8')
						self.write_message(json_encode)

		self.SendAll(self.id, msg)
	
	def check_origin(self, origin):
		return True
	
	def SendAll(self, send_id, msg):
		for client in self.clients:
			if not client.ws_connection.stream.socket:
				self.clients.remove(client)
				log.critical("WS_Handler::SendAll(%s) client removed", self.id)
			else:
				if client.id == send_id:
					continue

				log.info("[TX] {} => {}" .format(msg, client.id))
				client.write_message(msg)

class WS_Server(Process):
	def __init__(self):
		Process.__init__(self)
	
	def run(self):
		log.critical("WS_Server::run() START port[%s]", g_var['websocket_port'])
		try:
			cur_log_level=logger.get_logger_level(log)
			#if g_log_level.value != cur_log_level  :
			#	log.critical("[THREAD:WS_SERVER] LOG LEVEL CHG [{}] -> [{}] " .format(cur_log_level, g_log_level.value))
			#	logger.change_logger_level(log, g_log_level.value)

			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			app = tornado.web.Application([(r"/websocket", WS_Handler)])

			if g_var['websocket_secure_flag'].upper() == 'TRUE' :
				#data_dir=g_var['cert_path']
				#data_dir=g_var['key_path']

				#ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
				#ssl_ctx.load_cert_chain(ssl_root+"/server.crt", ssl_root + "/server.pem")
				ssl_ctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
				#ssl_ctx.load_cert_chain(os.path.join(data_dir, "server.crt"),
				#                        os.path.join(data_dir, "server.key"))
				ssl_ctx.load_cert_chain(g_var['cert_path'], g_var['key_path'])
				server=tornado.httpserver.HTTPServer(app,ssl_options=ssl_ctx)
				server.listen(int(g_var['websocket_port']),"0.0.0.0")
				#server=tornado.httpserver.HTTPServer(app,ssl_options=ssl.CERT_NONE)
				#app.listen(int(g_var['websocket_port']))
			else :
				app.listen(int(g_var['websocket_port']))

			tornado.ioloop.IOLoop.instance().start()


		except Exception as e:
			log.critical("WS_Server() Exception => %s", e)
			pass

		log.critical("WS_Server::run STOP")

class WS_Client():
	def __init__(self, url):
		self.url = url
		self.wsc = None
	
	def connect(self):
		log.critical("WS_Client::connect() => %s", self.url)
		try:
			#self.wsc = websocket.WebSocket()
			#FOR WSS
			if g_var['websocket_secure_flag'].upper() == 'TRUE' :
				self.wsc = websocket.WebSocket(sslopt={"cert_reqs": ssl.CERT_NONE})
			else :
				self.wsc = websocket.WebSocket()
			self.wsc.connect(self.url, setdefaulttimeout=0)
		except socket.error as e:
			if e.errno == 111: # [errno 111] Connection refused
				log.critical("WS_Client::connect refused fail [%s]", e)
				pass
			else:
				log.critical("WS_Client::connect socket fail [%s]", e)
				self.close()
		except Exception as e:
			log.critical("WS_Client::connect fail [%s]", e)
			self.close()

	def send(self, msg):
		if self.wsc is None:
			self.connect()

		if self.wsc:
			try:
				self.wsc.send(msg)
			except Exception as e:
				log.critical("WS_Client::connect fail [%s]", e)
				self.close()

	def close(self):
		log.critical("WS_Client::close()")
		self.wsc.close()
		self.wsc = None

class IPC_Receiver():
	def init(self):
		# zmq consumer Init
		#self.my_pull = ZmqPipline(log)
		#self.my_pull.bind(g_var['my_zmq_port'])
		self.ipc=IPC.MindsIPCs(log, g_var['proc_name'])
		ret=self.ipc.IPC_Open()
		if ret == False :
			print("IPC OPEN FAILURE")

		## websocket client Init
		if g_var['websocket_secure_flag'].upper() == 'TRUE' :
			self.wsc_loc = WS_Client("wss://%s:%s/websocket" %(g_var['websocket_ip'], g_var['websocket_port']))
		else :
			self.wsc_loc = WS_Client("ws://%s:%s/websocket" %(g_var['websocket_ip'], g_var['websocket_port']))
	
	def run(self):
		log.critical("IPC_Receiver::run()")
		while True:
			try:
				recv_msg = self.ipc.IPC_Recv()
				if recv_msg :
					json_msg = json.loads(recv_msg)
				else :
					time.sleep(0.1)
					continue

				if json_msg['msg_header']['msg_id'] == 'EVENT_COLLECTION_DISTRIBUTE' :
					log.info("[RX] {}" .format(json_msg['msg_body']))
					self.wsc_loc.send(recv_msg)
				elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
					#conf=simd_conf.Proc_Conf(g_var['proc_name'])
					#g_log_level.value = conf.get_loglevel()

					conf=ConfigParser.ConfigParser()
					conf.read(g_conf_path)
					proc_name=g_var['proc_name'].upper()
					items=conf.items(proc_name)
					for name, value in items :
						if name == 'log_level' :
							g_var['log_level']=value

					#logger.change_logger_level(log, g_log_level.value)
					log.critical("main: CHG_LOGLEVEL [{}]".format(g_var['log_level']))
				else :
					log.error("main: Unknown JSON MSG recved [{}]".format(json_msg))

			except zmq.ZMQError as e:
				log.critical("ZMQ Error :: %s", e)
				time.sleep(0.1)
			except KeyboardInterrupt:
				log.info("interrupt received, stopping...")
				break
			except Exception as e:
				log.error("main: exception raise fail. <%s>", e)
				log.error(traceback.format_exc())

	def close(self):
		log.critical("IPC_Receiver::close()")
		#self.my_pull.Close()
		self.ipc.IPC_Close()
		self.wsc_loc.Close()
		self.wsc_voice.Close()

#def load_config() :
#	global g_var
#	global g_conf_path

#	g_var['proc_name'] = os.path.basename(sys.argv[0])
#	proc_name=g_var['proc_name'].upper()

#	g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
#	conf=ConfigParser.ConfigParser()
#	conf.read(g_conf_path)
#	items=conf.items(proc_name)
#	for name, value in items :
#		g_var[name]=value

#	# RECORD
#	for i in g_var :
#		print( '{} = {}'.format(i,g_var[i]))
#
#
#	return


def main():
	global log
	global g_var
	global mysql

	#load_config()
	try :
		proc_name=os.path.basename(sys.argv[0])
		g_var=load_config(proc_name, g_conf_path)
	except Exception as e:
		print("load config fail. <%s>",e)
		print(traceback.format_exc())
		return False

	# logger
	log = logger.create_logger(os.getenv('HOME') + '/MP/logs', sys.argv[0], g_var['log_level'], True)

	# MYSQL Class 생성
	mysql = DBLib(log)
	# MySql DB 접속
	db_conn = mysql.connect()


	log.critical('');
	log.critical("*" * 80);
	log.critical('*%30s : %s', "START", g_var['proc_name']);
	log.critical("*" * 80);
	log.critical("* MY ZMQ SERVER INFO")
	log.critical("* WEBSOCKET SERVER INFO")
	log.critical("* WEBSOCKET IP           : %s", g_var['websocket_ip'])
	log.critical("* WEBSOCKET PORT         : %s", g_var['websocket_port'])
	log.critical("* WEBSOCKET SECURE FLAG  : %s", g_var['websocket_secure_flag'].upper())
	if 'cert_path' in g_var :
		log.critical("* CERT_PATH              : %s", g_var['cert_path'])
	if 'key_path' in g_var :
		log.critical("* KEY_PATH               : %s", g_var['key_path'])

	log.critical("*" * 80);

	# WebSocket Server Spawn
	wss = WS_Server()
	wss.start() #Process Spawn: run() method 호출
	#wss.join()  #Process 종료될때까지 block

	ipc_receiver = IPC_Receiver()
	ipc_receiver.init()
	ipc_receiver.run()
	ipc_receiver.close()

	log.critical("Process stopped...")

	return 0

if __name__ == '__main__':
	main()

