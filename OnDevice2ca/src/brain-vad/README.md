# brain-vad
voice activity detection

## Setup
1. `pip install -r requirements.txt`
1. `python -m grpc.tools.protoc --proto_path=. --python_out=. --grpc_python_out=. vad.proto`

## Run Server
1. `python server.py`

## Run Client
1. `python client.py`

## TODO
- [ ] dockerfile