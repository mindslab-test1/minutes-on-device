
#!/usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, json
import traceback
import multiprocessing
from multiprocessing import Value, Array
from gensim.summarization.summarizer import summarize
from gensim.summarization import keywords
from ctypes import c_bool

is_py2 = sys.version[0] == '2'
if is_py2 :
    import Queue
    import ConfigParser
else :
    import queue as Queue
    import configparser as ConfigParser

#####################################################################################
# MINDSLAB COMMON
####################################################################################
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
from common.dblib import DBLib
from common.def_val import *

g_var={}
g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'


#print(keywords(text))
#print(summarize(text, ratio=0.3, split=True))


def run_worker(worker_q, idx) :
	
	# MySql Class 생성
	mysql = DBLib(log)
	# MySql DB 접속
	db_conn = mysql.connect()

    # IPC Connection
	#ipc=IPC.MindsIPCs(log, g_var['proc_name'])
	while work_flag.value == True :
		try :
			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			recv_buf=worker_q.get(True, 1)


			stt_meta=recv_buf
			meta_ser=stt_meta['STT_META_SER']
			log.info("[MTSd_THREAD({})] : META [{}] Summarizing".format(idx, meta_ser))

			user_keyword_list=list()
			result, rowcnt, rows, error = SQL.select_summarize(mysql, meta_ser)
			if result == False :
				log.critical("[MTSd_THREAD({})] : META [{}] Select Summarize Failure [{}]".format(idx, meta_ser, error))
				result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ERROR)
				continue
			elif rowcnt == 0 :
				log.critical("[MTSd_THREAD({})] : META [{}] Summarize Not Found [{}]".format(idx, meta_ser, error))
				result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ERROR)
				continue
			else :
				for row in rows :
					user_keywords=row['SUMMARIZE_KEYWORDS'].split(',')
					for user_keyword in user_keywords :

						if user_keyword == '' :
							pass
						else :
							user_keyword_list.append(user_keyword)

			log.critical("user_keywords : {}".format(user_keyword_list))

			summarize_result=''
			if len(user_keyword_list) > 0 :
				result, rowcnt, rows, error = SQL.select_stt_result_bymeta(mysql, meta_ser)
				if result == False :
					log.critical("[MTSd_THREAD({})] : META [{}] Result Select Failure [{}]".format(idx, meta_ser, error))
					result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ERROR)
					continue

				before=int(g_var['before_target'])
				after=int(g_var['after_target'])
				summarize_row_list=list()
				for i in range(len(rows)) :
					row=rows[i]
					if row['STT_CHG_RESULT'] :
						line_buf=row['STT_CHG_RESULT']
					else :
						line_buf=row['STT_ORG_RESULT']
				
					for user_keyword in user_keyword_list :
						if user_keyword in line_buf :
							start_idx=i - before
							if start_idx < 0 :
								start_idx = 0 

							end_idx=i + after
							if end_idx > len(rows) :
								end_idx=len(rows)
							for j in range(end_idx - start_idx) :
								idx=j + start_idx
								if idx in summarize_row_list :
									continue
								else :
									summarize_row_list.append(idx)
				log.debug("summarize_row_list ::{}".format(summarize_row_list))
							
				minutes_text=''
				for row_idx in summarize_row_list :
					row=rows[row_idx]
					if row['STT_CHG_RESULT'] :
						line_buf=row['STT_CHG_RESULT']
					else :
						line_buf=row['STT_ORG_RESULT']

					line_buf=line_buf.replace('\r\n','')
					line_buf=line_buf.replace('\n','')
					minutes_text = minutes_text + line_buf +'\n'

				summarize_keywords=keywords(minutes_text)
				summarize_keywords=summarize_keywords.replace("\n", ", ")

				summarize_text=summarize(minutes_text, ratio=float(g_var['text_ratio']), split=True)

				for line_idx in range(len(summarize_text)) :
					line=summarize_text[line_idx]
					#동일문장 반복 제거
					if line_idx > 0 :
						before_line=summarize_text[line_idx - 1]
						if line.strip() == before_line.strip() :
							continue

						summarize_result=summarize_result + line + '\n'
					#for row_idx in summarize_row_list :
					#	row=rows[row_idx]
					#	if row['STT_CHG_RESULT'] :
					#		line_buf=row['STT_CHG_RESULT']
					#	else :
					#		line_buf=row['STT_ORG_RESULT']

					#	line_buf=line_buf.replace('\r\n','')
					#	line_buf=line_buf.replace('\n','')
					#	line_buf=line_buf.strip()

					#	if line.strip() == line_buf.strip() :
					#		summarize_result=summarize_result + '[{}] : '.format(row['MINUTES_EMPLOYEE']) + line + '\n'
					#		break

				log.debug("[MSTd_Thread({})] META [{}]".format(idx, meta_ser))
				log.debug(" ##summarized InPut text##\n   {} ".format(minutes_text))
				log.debug(" ##keywords##\n   {} ".format(summarize_keywords))
				log.debug(" ##summarized OutPut text##\n   {} ".format(summarize_result))

			if len(summarize_result) == 0 :
				summarize_result= '회의 요약된 내용이 없습니다.'
			#### [UPDATE] ##########
			result, error = SQL.update_summarize(mysql, meta_ser, summarize_result)
			if result == False :
				log.critical("[MTSd_THREAD({})] : META [{}] summarize Update Failure [{}]".format(idx, meta_ser, error))
				result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ERROR)

			#### [Delete&&Insert] ##########
			#delete summarize
			#result, error = SQL.delete_summarize(mysql, meta_ser)
			#if result == False :
			#	log.critical("[MTSd_THREAD({})] : META [{}] delete summarize Failure [{}]".format(idx, meta_ser, error))

			#result, error = SQL.insert_summarize(mysql, meta_ser, summarize_text, summarize_keywords)
			#if result == False :
			#	log.critical("[MTSd_THREAD({})] : META [{}] summarize Insert Failure [{}]".format(idx, meta_ser, error))
			#	result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ERROR)

			result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_END)
			if result == False :
				log.critical("[MTSd_THREAD({})] : META [{}] End Update Failure [{}]".format(idx, meta_ser, error))

			log.info("[MTSd_THREAD({})] : META [{}] Summarize Complete".format(idx, meta_ser))

		except Queue.Empty:
			continue
		except Exception as e:
			log.critical("load config fail. <%s>", e)
			log.critical(traceback.format_exc())
			continue
	log.critical("[MTSd_THREAD({})] : Process Terminate".format(idx))
				
def init_process() :
	global log
	global g_var
	global work_flag

	work_flag=Value(c_bool,True)


	try:
		# Init Config
		try:
			proc_name=os.path.basename(sys.argv[0])
			g_var=load_config(proc_name, g_conf_path)
			g_var['select_interval'] = 1
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return False, None, None, None, None, None

		# Init Logger
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', g_var['proc_name'].lower(), g_var['log_level'], True)

		# MySql Class 생성
		mysql = DBLib(log)
		# MySql DB 접속
		db_conn = mysql.connect()

		ipc=IPC.MindsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			return False, None, None, None, None, None
		#ret=ipc.IPC_Regi_Process('mccd')
		#if ret == False :
		#	log.critical("IPC_REGI_PROCESS FAIL")
		#	return False, None, None, None
		#ret=ipc.IPC_Regi_Process('rsrd')
		#if ret == False :
		#	log.critical("IPC_REGI_PROCESS FAIL")
		#	return False, None, None, None

		# Init Signal
		#signal.signal(signal.SIGINT, sig_int_handler)

		worker_list=list()
		worker_cnt=int(g_var['thr_cnt'])

		worker_q = multiprocessing.Queue()
		for idx in range(worker_cnt):
			p = multiprocessing.Process(target=run_worker, args=(worker_q, idx))
			p.daemon = True
			p.start()
			worker_list.append(p)


	except Exception as e:
		log.critical("main: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, None, None, None, None, None
	else:
		return True, mysql, log, ipc, worker_list, worker_q

def main():
	global log

	# Init Process
	result, mysql, log, ipc, worker_list, worker_q = init_process()
	if result == False :
		log.critical("[MTSd] STARTING FAILURE ")
		sys.exit(1)

	log.critical("[MTSd] STARTING.... " )

	# Start MainLoop
	while True :
		try:

			time.sleep(g_var['select_interval'])
			msg=ipc.IPC_Recv()
			if msg :
				json_msg = json.loads(msg)
				log.critical("main: JSON MSG recved [{}]".format(json_msg))
				if 'msg_header' not in json_msg or 'msg_body' not in json_msg :
					log.critical("main: Invalid JSON MSG recved [{}]".format(json_msg))
					continue

				elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
					g_var['log_level'] = get_log_level(g_var['proc_name'], g_conf_path)
					logger.change_logger_level(log, g_var['log_level'])
					log.critical("main: CHG_LOG_LEVEL [{}]".format(g_var['log_level']))
					continue

				else :
					log.info("recv :: Unknown MSG : {} " .format(json_msg))
	
			################################################################
			#                 EVENT_COLLECTION                             # 
			################################################################
			result, rowcnt, rows, error =SQL.select_stt_meta_bystatus_with_summarize(mysql, SQL.META_STAT_COMPLETE, SQL.SUMMARIZE_STATUS_START) 
			if(result == False) :
				log.critical('START :: Select STT_META Failure [{}]' .format(error))
				continue
			else :
				if(rowcnt > 0) :
					log.info('START :: find [{}] new data' .format(rowcnt))
				else :
					continue

			for row in rows :
				meta_ser=row['STT_META_SER']
				log.debug("[MTSd] : META [{}] is Starting Summarize".format(meta_ser))
				result, error = SQL.update_stt_meta_summarize_status(mysql, meta_ser, SQL.SUMMARIZE_STATUS_ING)
				if result == False :
					log.critical("[MTSd] : META[{}] is Cant Start Summarize ({})".format(meta_ser, error))
					continue

				worker_q.put(row)
							
		except Exception as e:
			log.critical("main: exception raise fail. <%s>", e)
			log.critical(traceback.format_exc())

	log.critical("main stooped...")
	work_flag.value == False
	for p in worker_list:
		p.join()
	
	ipc.IPC_Close()
	mysql.disconnect()
	return 0

if __name__ == '__main__':
	main()


