#ifndef PDS_EVENT_RECEIVER_H
#define PDS_EVENT_RECEIVER_H

// standard header
#include <thread>

#include "app-variable.h"
#include "stt-client.h"
#include "session-manager.h"
#include "rec-session.h"

//using RecSessionManager = SessionManager<RecSession, std::string, uint32_t>;
using RecSessionManager = SessionManager<RecSession, std::string, std::string>;


class RecEventReceiver : public TaskHandler
{
public:

  RecEventReceiver(RecSessionManager *manager);
  ~RecEventReceiver();

  void Start();
  void StartCall(char* msg, int len);
  void StopCall(char* msg, int len);
  void ProcessCallInfo(char* msg, int len);

  // virtual void handle_task(int index, int event, std::string data);
  virtual void handle_task(int index, Task *task);

private:

  void run();

  RecSessionManager *session_manager_;
  std::thread thrd_;
};

#endif /* PDS_EVENT_RECEIVER_H */
