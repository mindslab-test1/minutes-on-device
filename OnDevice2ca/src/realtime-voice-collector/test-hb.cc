#include <stdio.h>
#include <unistd.h>
#include "app-variable.h"
#include "heart-beat.h"

#ifdef SPDLOG_VER_MAJOR
#include <spdlog/sinks/stdout_color_sinks.h>
#endif

APP_VARIABLE g_var;

int main(int argc, char *argv[])
{
  if (argc < 3) {
    printf("%s local_ip:port remote_ip:port\n", argv[0]);
    return 0;
  }

  SPD_Logger console = spdlog::stdout_color_mt("console");
  console->set_level(spdlog::level::trace);

  bool is_active = false;
  HeartBeat hb(is_active, console);

  char local_ip[128];
  char remote_ip[128];

  sprintf(local_ip, "tcp://%s", argv[1]);
  sprintf(remote_ip, "tcp://%s", argv[2]);

  // Address Sample
  // hb.Start("tcp://127.0.0.1:5570", "tcp://127.0.0.1:5571");
  hb.Start(local_ip, remote_ip);

  while (true) {
    usleep(1 * 1000 * 1000);
    if (is_active) {
      console->debug("---> This is active");
    }
  }

  hb.Stop();

  return 0;
}
