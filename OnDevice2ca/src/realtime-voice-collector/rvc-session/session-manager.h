#ifndef SESSION_MANAGER_H
#define SESSION_MANAGER_H

#include <unordered_map>
#include <thread>
#include <queue>
#include <vector>
#include <mutex>

#include "worker-pool.h"
// #include "channel.h"
// #include "session.h"
#include "voice-channel.h"

using std::vector;
using std::queue;

enum SessionEvent {
  EV_START,
  EV_STOP,
  VOICE_DATA,
  DELETE_SESSION,
  SESSION_EVENT,
  IS_EXPIRED,
  EV_UNKNOWN,
};

template <typename SESS, typename FirstKey, typename SecondKey>
class SessionManager : public TaskHandler {
 public:
  SessionManager(int max_session_num, WorkerPool *pool, int session_timeout);
  virtual ~SessionManager();

  void StartMonitoring(TaskHandler *handler);
  virtual void ProcessData(int index, int data_type, std::string data);

  int WorkerIndex();

  void Dispatch(int data_type, std::string data);

  int  GetSessionIndex(FirstKey key);
  int  FindSessionIndexFromChannel(SecondKey key);

  // find from session list
  SESS *GetSession(int index);
  // find from session map
  SESS *FindSession(FirstKey key, int &index);
  // for create session
  SESS *GetSession(FirstKey key, int &index);

  void ReleaseSession(int index);
  void SetSession(FirstKey session_key, SecondKey channel_session_key, int index);
  void SetSession(SESS *session);

  void Enqueue(int index, Task *task);

 public:
  void ProcessSession(int index, std::string data);
  void ProcessChannel(int index, std::string data);

  WorkerPool *worker_pool_;

  /**
   * @brief session의 key값으로 session number를 찾는다
   *
   * SIP의 CALL-ID나 기타 다른 Signalling Event의 key값을 활용할 수 있다
   * 
   */
  std::unordered_map<FirstKey, int> session_map;

  /**
   * @brief channel의 특정 값으로 session number를 찾는 테이블
   *
   * RTP의 IP값이나, PORT번호 등을 활용할 수 있다
   * session_map의 key값과 동일한 경우도 생길 수 있다
   * 
   */
  std::unordered_map<SecondKey, int> session_map2;

  int max_session_num_;
  queue<int> standby_session_q_;
  vector<SESS *> session_list_;
  std::mutex session_lock_;

  std::thread monitor_thrd_;
  int session_timeout_;
  bool is_done_;
};

#include "session-manager-impl.h"

#endif /* SESSION_MANAGER_H */
