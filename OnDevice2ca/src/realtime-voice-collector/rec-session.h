#ifndef REC_SESSION_H
#define REC_SESSION_H

#include <stdint.h>
#include <memory>
#include "voice-channel.h"
#include "stt-client.h"

typedef struct 
{
	std::string stt_model_ser;
	std::string lean_type;
	std::string server_addr;
	std::string model_name;
	std::string model_lang;
	std::string model_rate;
} stt_info; 

class RecChannel : public VoiceChannel
{
public:

  //RecChannel();
  RecChannel(std::string unique_id, std::string device_ip, std::string tel_no, std::string talker, stt_info stt_info);
  RecChannel(std::string unique_id, std::string device_ip, std::string tel_no, std::string talker);
  virtual ~RecChannel();

  virtual void Stop();

  void DoStart();
  void DoSTT(std::string &buf);
  void DoSTT(short *buf, size_t len);

private:
  std::string unique_id_;
  std::string device_ip_;
  std::string tel_no_;
  std::string talker_;
  stt_info stt_info_;

  bool ConnectSTT();
  std::unique_ptr<SttClient> stt_client_;
};

//class RecSession : public VoiceSession<std::string, uint32_t>
class RecSession : public VoiceSession<std::string, std::string>
{
public:

  RecSession() {}
  virtual ~RecSession() {}

  virtual void Stop();
};

#endif /* REC_SESSION_H */
