#!/usr/bin/python
from konlpy.tag import Okt
from collections import Counter
import csv

filename = "/home/minds/git/gn/tool/konlpy/vad_test10.txt"
f = open(filename, 'r', encoding='utf-8')
news = f.read()
print(news)

# create okt object
okt = Okt()
noun = okt.nouns(news)
print(type(noun))
print(noun)
count = 0

for i, v in enumerate(noun):
	print (i, v)
	if len(v) < 2 :
		print ("len{} {} - pop".format(len(v), v))
		noun.pop(i)
		count += 1
	else:
		print ("len{} {} - not pop".format(len(v), v))
		count += 1

print(count, len(noun))

print(noun)

for i, v in enumerate(noun):
	print (i, v)
	if len(v) < 2 :
		print ("len{} {} - pop".format(len(v), v))
		noun.pop(i)
	else:
		print ("len{} {} - not pop".format(len(v), v))

print(noun)

for i, v in enumerate(noun):
	print (i, v)
	if len(v) < 2 :
		print ("len{} {} - pop".format(len(v), v))
		noun.pop(i)
	else:
		print ("len{} {} - not pop".format(len(v), v))

print(noun)

count = Counter(noun)
print (count)
f.close()

# count frequency of noun
noun_list = count.most_common(100)
print(noun_list)
print(type(noun_list))
for v in noun_list:
	print(v)

# save as Text File
with open("noun_list.txt", 'w', encoding='utf-8') as f :
	for v in noun_list:
		f.write(" ".join(map(str, v))) # convert integer value of tuples to string type and excute 'join'
		f.write("\n")

# save as .csv File
with open("noun_list.csv", "w", newline='', encoding='euc-kr') as f :
	csvw = csv.writer(f)
	for v in noun_list:
		csvw.writerow(v)
