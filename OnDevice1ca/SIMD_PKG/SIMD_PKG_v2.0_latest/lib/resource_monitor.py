# -*- coding: utf-8 -*-
import psutil, os, subprocess, json
import common.simd_config as simd_conf
import gpustat

class ResourceMonitor():
	def __init__(self, proc_list=None):
		self.proc_list = proc_list
		return

	def cpu_percent(self):
		config = simd_conf.Config_Parser(os.getenv('MAUM_ROOT') + '/etc/simd.conf')
		self.recv_interval = float(config.DIS_Item_Value('HARDWARE', 'interval'))
		return psutil.cpu_percent(interval=self.recv_interval, percpu=False)

	def cpu_percent_2(self):
		return psutil.cpu_percent(interval=None, percpu=False)
	
	def mem_total(self):
		return psutil.virtual_memory().total

	def mem_free(self):
		return psutil.virtual_memory().available

	def mem_used(self):
		return self.mem_total() - self.mem_free()

	def disk_total(self, path):
		disk = os.statvfs(path)
		return disk.f_bsize * disk.f_blocks
		## 파일시스템의 블록 크기 곱하기 블록수
	def disk_free(self, path):
		disk = os.statvfs(path)
		return disk.f_bsize * disk.f_bavail
		## 파일 시스템의 블록 크기 곱하기 루트가 아닌 사용자에서 사용할 수 있는 블록 수

	def disk_used(self, path):
		disk = os.statvfs(path)
		return disk.f_bsize * (disk.f_blocks - disk.f_bavail)
		## 파일 시스템의 블록 크기 곱하기 루트로 사용한 블록 수

	def gpu_stat(self):
		command = 'gpustat'
		process = os.popen(command)
		return process.read().split("\n")[1]

	def gpu_stat2(self):
		command = 'gpustat --json'
		json_process = os.popen(command).read()
		process = json.loads(json_process)
	
		if not process['gpus'][0] :
			return None
		else :
			gpu_percent = float(process["gpus"][0]["utilization.gpu"])
			gpu_total_used = float(process["gpus"][0]["memory.used"])
			gpu_total_mem = float(process["gpus"][0]["memory.total"])
			return gpu_percent, gpu_total_used, gpu_total_mem
	
	
	### GPU 가 여러 개 일때. 아직 미 테스트
	def gpu_stat3(self):
		# check gpu driver
		result = subprocess.getoutput('lspci | grep -i VGA')
		if ('NVIDIA' in result.upper()):
			#print ("This System's GPU Driver is NVIDIA!")
			try :
				gpus = {}
				command = 'gpustat --json'
				json_process = os.popen(command).read()
				if ("ERROR" in json_process):
					return None
				else :
					process = json.loads(json_process)
			
					for i in range(len(process["gpus"])) :
						gpus[i] = {} 
						gpus[i]["gpu_percent"] = float(process["gpus"][i]["utilization.gpu"]) 	
						gpus[i]["gpu_total_used"] = float(process["gpus"][i]["memory.used"]) 	
						gpus[i]["gpu_total_mem"] = float(process["gpus"][i]["memory.total"]) 	
					return gpus
		
			except Exception as e:
				return None
		else :
			#print ("This System's GPU Driver is not NVIDIA!")
			return None

	def process_list(self):
		result = []
		for proc_name in self.proc_list:
			try:
				tmp = subprocess.check_output("ps -edf | grep {} | grep -v grep" .format(proc_name), shell=True)
			except subprocess.CalledProcesssError as e:
				tmp_dict = {proc_name:0}
			else:
				tmp_dict = {proc_name:1}
			result.append(tmp_dict)
		return result

