#!/usr/bin/python
# -*- coding: utf-8 -*-
#import sys, os, logging, time
#from common.dblib import DBLib
import traceback

# STT_META 테이블 STATUS 정의
#[FILE UPLOAD]
META_STAT_RESERV=0
META_STAT_WORKING=1
META_STAT_COMPLETE=2
META_STAT_ERROR=3
#[REALTIME]
META_STAT_REALTIME_START=10
META_STAT_REALTIME_WORKING=11
META_STAT_REALTIME_STOP=12
META_STAT_REALTIME_CLOSING=13
META_STAT_REALTIME_COMPLETE=14
META_STAT_REALTIME_EXPIRE=15

META_STAT_GENERAL_FAILURE=90

#[WL_DEVICE]
DEVICE_STATUS_NONE = '0'
DEVICE_STATUS_INIT = '1'
DEVICE_STATUS_CONNECTING = '2'
DEVICE_STATUS_CONNECTED = '3'
DEVICE_STATUS_FAILED = '4'
DEVICE_STATUS_RELEASE = '5'

#[TEXT_RANK_STATUS]
TEXT_RANK_STATUS_NONE = 0
TEXT_RANK_STATUS_ING = 1
TEXT_RANK_STATUS_END = 2

#[STT_META] Table

#select_meta_query= """
#	select STT_META_SER, MINUTES_USER_SER, MINUTES_SITE_SER, CREATE_USER, date_format(CREATE_TIME, '%Y%m%d%h%i%s'), 
#	UPDATE_USER, date_format(UPDATE_TIME, '%Y%m%d%h%i%s'), MINUTES_MEETINGROOM, MINUTES_MACHINE, MINUTES_ID, 
#	MINUTES_NAME, date_format(MINUTES_START_DATE, '%Y%m%d%h%i%s'), MINUTES_TOPIC, MINUTES_JOINED_MEM,MINUTES_JOINED_CNT, 
#	MINUTES_STATUS, REC_SRC_CD, SRC_FILE_PATH, DST_FILE_PATH, START_TIME, END_TIME, REC_TIME, MEMO, MINUTES_LANG 
#	from STT_META
#	"""
select_meta_query= "select * from STT_META "
select_result_query = "select * from STT_RESULT"


def select_stt_meta_bymeta_ser(mysql, stt_meta_ser) :
	query=select_meta_query + " where STT_META_SER={};".format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_meta_bystatus(mysql, status) :
	query=select_meta_query + " where MINUTES_STATUS='{}';".format(status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_meta_bystatus_with_textrank(mysql, status, textrank_status) :
	query=select_meta_query + " where MINUTES_STATUS='{}' and TEXT_RANK_STATUS='{}';".format(status, textrank_status)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_result_by_talker(mysql, stt_meta_ser, talker) :
	query=select_result_query + " where STT_META_SER='{}' and MINUTES_EMPLOYEE='{}';".format(stt_meta_ser, talker)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def update_stt_meta_textrank_status(mysql, stt_meta_ser, textrank_status) :
	query =" update STT_META set TEXT_RANK_STATUS='{}' where STT_META_SER='{}';" .format(textrank_status, stt_meta_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def insert_textrank_keyword(mysql, stt_meta_ser, attendee, rank1, rank2, rank3, rank4, rank5) :
	query ="insert into MINUTES_TEXT_RANK (STT_META_SER, CREATE_USER, CREATE_TIME, ATTENDEE_NAME, TEXT_RANK1, TEXT_RANK2, TEXT_RANK3, TEXT_RANK4, TEXT_RANK5) values ({}, 'mrtd', now(), '{}', '{}', '{}', '{}', '{}', '{}');" .format(stt_meta_ser, attendee, rank1, rank2, rank3, rank4, rank5)
	print(query)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error


def update_stt_meta_status(mysql, stt_meta_ser, status) :
	query =" update STT_META set MINUTES_STATUS={} where STT_META_SER={};" .format(status, stt_meta_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def update_stt_meta_complete(mysql, stt_meta_ser, rec_time, dst_file_path, status) :

	query= """
	update STT_META set MINUTES_STATUS={}, REC_TIME={}, DST_FILE_PATH="{}" where STT_META_SER={};
	""" .format(status, rec_time, dst_file_path, stt_meta_ser)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def update_stt_meta_complete_realtime(mysql, stt_meta_ser, rec_time, dst_file_path, status) :

	query= """
	update STT_META set MINUTES_STATUS={}, REC_TIME={}, SRC_FILE_PATH="{}", DST_FILE_PATH="{}" 
	where STT_META_SER={};
	""" .format(status, rec_time, dst_file_path, dst_file_path, stt_meta_ser)

	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error


#[MINUTES_COMMON] Table
def select_minutes_common(mysql) :
	query= " select * from MINUTES_COMMON;"
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None


#[MINUTES_SITE] Table
def select_minutes_team(mysql, team_ser) :
	query= " select * from MINUTES_TEAM where MINUTES_TEAM_SER={};".format(team_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

#[STT_MODEL] Table
def select_stt_model(mysql, model_ser) :
	query= " select * from STT_MODEL where STT_MODEL_SER={}".format(model_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_stt_model_all(mysql) :
	query= """ select * from STT_MODEL;"""
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_wl_device_all(mysql) :
	query= """ select * from MINUTES_WL_DEVICE """
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_wl_device(mysql, wl_device_ser) :
	query= """ select * from MINUTES_WL_DEVICE where MINUTES_WL_DEVICE_SER='{}'
	""".format(wl_device_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None


def update_minutes_wl_device_status(mysql, wl_device_ser, status) :
	query= """ update MINUTES_WL_DEVICE set WL_DEVICE_STATUS='{}' where MINUTES_WL_DEVICE_SER='{}'
	""".format(status, wl_device_ser)
	result, error = mysql.execute(query, True)
	if result == True :
		return True, None
	else :
		return False, error

def select_minutes_mic(mysql, meetingroom_ser) :
	query= """ select * from MINUTES_MIC where MINUTES_MEETINGROOM_SER='{}'
	""".format(meetingroom_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

def select_minutes_wl_mic(mysql, wl_device_ser) :
	query= """ select * from MINUTES_WL_MIC where MINUTES_WL_DEVICE_SER='{}'
	""".format(wl_device_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None



def select_replace(mysql) :
	query= """ select * from MINUTES_ALL_REPLACE"""
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None





#[STT_RESULT] Table
def insert_stt_result(mysql, stt_meta_ser, mic_id, stt_org_result, str_stt_start, str_stt_end) :

	stt_org_result=stt_org_result.replace("'","''")

#	query= """insert into STT_RESULT (STT_META_SER, CREATE_USER, CREATE_TIME, MINUTES_EMPLOYEE, STT_ORG_RESULT, 
#	STT_RESULT_START, STT_RESULT_END) values ({}, '{}', now(), '{}', '{}', '{}','{}')
#	""" .format(stt_meta_ser, 'MIPd', mic_id, stt_org_result.encode('utf-8'), str_stt_start, str_stt_end)
	query= """insert into STT_RESULT (STT_META_SER, CREATE_USER, CREATE_TIME, MINUTES_EMPLOYEE, STT_ORG_RESULT, 
	STT_RESULT_START, STT_RESULT_END) values ({}, '{}', now(), '{}', '{}', '{}','{}')
	""" .format(stt_meta_ser, 'MIPd', mic_id, str(stt_org_result), str_stt_start, str_stt_end)



	result, error = mysql.execute(query, False)
	if result == True :
		return True, None
	else :
		return False, error


def select_stt_result(mysql, stt_meta_ser) :
	query= " select STT_ORG_RESULT from STT_RESULT where STT_META_SER={} order by STT_RESULT_START; " .format(stt_meta_ser)
	rowcnt, rows, error = mysql.execute_query(query)
	if rowcnt == None or rows == None :
		return False, None, None, error
	else :
		return True, rowcnt, rows, None

