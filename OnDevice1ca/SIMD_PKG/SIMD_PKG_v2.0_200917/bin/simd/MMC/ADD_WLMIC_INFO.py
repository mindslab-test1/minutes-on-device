#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

ADD_WLMIC_INFO_MIN_PARAMETER = 4
ADD_WLMIC_INFO_MAX_PARAMETER = 5

param_list = ["WL_MIC_ID", "USED_FLAG", "WL_MIC_NAME", "TMP_MIC_NAME", "WL_DEVICE_SER"]
mandatory_list = ["WL_MIC_ID", "USED_FLAG", "WL_MIC_NAME", "WL_DEVICE_SER"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  ADD-WLMIC-INFO [WL_MIC_ID=a], [USED_FLAG=b], [WL_MIC_NAME=c], [WL_DEVICE_SER=d], (TMP_MIC_NAME=e)

 [Parameter]
  a = DECIMAL     (1:100)
  b = ENUM        (ON|OFF)
  c = STRING      (1:100)
  d = DECIMAL     (1:9999999999)
  e = STRING      (1:100)

 [Usage]
  ADD-WLMIC-INFO [a, b, c, d, e]
   ex) ADD-WLMIC-INFO 3, ON, TEST_MIC3, 1, 홍길동 
  ADD-WLMIC-INFO [WL_MIC_ID=a], [USED_FLAG=b], [WL_MIC_NAME=c], [WL_DEVICE_SER=d], (TMP_MIC_NAME=e)
   ex) ADD-WLMIC-INFO WL_MIC_ID=3, USED_FLAG=ON, WL_MIC_NAME=TEST_MIC3, WL_DEVICE_SER=1, TMP_MIC_NAME=홍길동
  
 [Column information]
  WL_MIC_ID           : Wireless MIC Identity
  USED_FLAG           : Whether to use a MIC
  WL_MIC_NAME         : Wireless MIC Name
  TMP_MIC_NAME        : Temporary User Name
  WL_DEVICE_SER       : Connect Wireless Device Serial Number & Name
 
 [Result]
 <SUCCESS>
 Date time
 MMC    = ADD-WLMIC-INFO 
 Result = SUCCESS
 ================================================================================
   WL_MIC_ID | USED_FLAG |   WL_MIC_NAME   |   TMP_MIC_NAME   | WL_DEVICE_SER   |
 --------------------------------------------------------------------------------
  value      | value     | value           | value            | value           |
 ================================================================================

 <FAILURE>
 Date time
 MMC    = ADD-WLMIC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['WL_MIC_ID'] != None :
		ret, reason = check_Decimal_And_Range(MIN_COUNT_VALUE, MAX_COUNT_VALUE, 'WL_MIC_ID', arg_data['WL_MIC_ID'], G_log)
		if (ret == False) : return False, reason

	if arg_data['USED_FLAG'] != None :
		ret, reason = check_Enum(flag_list, 'USED_FLAG', arg_data['USED_FLAG'], G_log)
		if (ret == False) : return False, reason

	if arg_data['WL_MIC_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'WL_MIC_NAME', arg_data['WL_MIC_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['WL_DEVICE_SER'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'WL_DEVICE_SER', arg_data['WL_DEVICE_SER'], G_log)
		if (ret == False) : return False, reason

	##### Optional Parameter #####
	if arg_data['TMP_MIC_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'TMP_MIC_NAME', arg_data['TMP_MIC_NAME'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, ADD_WLMIC_INFO_MAX_PARAMETER, ADD_WLMIC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# insert new pricing information
			try :
				total_body = ''

				# check the wl deviced whether to exist
				data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_DEVICE", "*", "WHERE MINUTES_WL_DEVICE_SER={}".format(Parsing_Dict['WL_DEVICE_SER']))
				if int(data_cnt) is 0:
					result='FAILURE'
					reason='NOT EXIST SER={} WL DEVICE. CHECK THE WL DEVICE LIST'.format(Parsing_Dict['WL_DEVICE_SER'])
					return MMCLib.make_result(MMC, ARG, result, reason, '')

				# change used flag value
				if Parsing_Dict['USED_FLAG'] is 'OFF':
					used_flag = '0'
				else:
					used_flag = '1'

				if Parsing_Dict['TMP_MIC_NAME'] is None :
					insert_query = """ INSERT INTO MINUTES_WL_MIC value (NULL, 'simd', now(), NULL, NULL, {}, '{}', {}, '{}', '') """.format(Parsing_Dict['WL_DEVICE_SER'], used_flag, Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_MIC_NAME'])
				else:
					insert_query = """ INSERT INTO MINUTES_WL_MIC value (NULL, 'simd', now(), NULL, NULL, {}, '{}', {}, '{}', '{}') """.format(Parsing_Dict['WL_DEVICE_SER'], used_flag, Parsing_Dict['WL_MIC_ID'], Parsing_Dict['WL_MIC_NAME'], Parsing_Dict['TMP_MIC_NAME'])

				G_log.debug(insert_query)

				ret, err = mysql.execute(insert_query)
				if (ret == False) :
					result = 'FAILURE'
					if err is not None:
						reason = 'DB Insert Failure. [{}]'.format(err)
					else:
						reason = 'DB Insert Failure.'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('ADD-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
				result = 'FAILURE'
				reason = 'DB INSERT FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# check inserted information
			data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_WL_MIC", "*" , ';')
			if int(data_cnt) is 0 :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			elif err is not None:
				result = 'FAILURE'
				reason = "Select Failure. [{}]".format(err)
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				total_body=""" {:^9} | {:^9} | {:^13} | {:^14} | {:^19} |\n""" .format('WL_MIC_ID', 'USED_FLAG', 'WL_MIC_NAME', 'TMP_MIC_NAME', 'WL_DEVICE_SER')

				total_body = total_body + ('-' * MMC_SEND_LINE_WLMIC_CNT)
				for thr_num in range(len(db_data)) :
					wl_mic_id = db_data[thr_num]["WL_MIC_ID"]
					used_flag = db_data[thr_num]["USED_FLAG"]
					if used_flag is '0':
					    used_flag = "OFF"
					else:
						used_flag = "ON"
					wl_mic_name = db_data[thr_num]["WL_MIC_NAME"]
					tmp_mic_name = db_data[thr_num]["TMP_MIC_NAME"]
					tmp_wl_device_id = db_data[thr_num]["MINUTES_WL_DEVICE_SER"]
					data_cnt2, db_data2, err2= MMCLib.Select_query(mysql, "MINUTES_WL_DEVICE", "DEVICE_NAME", "WHERE MINUTES_WL_DEVICE_SER={}".format(tmp_wl_device_id))
					if int(data_cnt2) is 0:
						wl_device_name = "UNKNOWN"
					else:
						wl_device_name = str(tmp_wl_device_id) + ' (' + db_data2[0]["DEVICE_NAME"] + ')'

					row_buf="\n {:9} | {:9} | {:13} | {:11} | {:19} |" .format(wl_mic_id, used_flag, wl_mic_name, tmp_mic_name, wl_device_name)
					total_body = total_body + row_buf 

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("ADD-WLMIC-INFO(), NoSectionError : [{}]".format(e))
		reason='ADD-WLMIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("ADD-WLMIC-INFO(), Config read error: [{}]".format(e))
		reason='ADD-WLMIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='ADD-WLMIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('ADD-WLMIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='ADD-WLMIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

