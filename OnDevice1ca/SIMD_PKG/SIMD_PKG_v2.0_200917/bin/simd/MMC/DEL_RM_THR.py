#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DEL_RM_THR_MIN_PARAMETER = 1
DEL_RM_THR_MAX_PARAMETER = 1

param_list = ['SYS_RSC_THR_SER']
mandatory_list = ['SYS_RSC_THR_SER']

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DEL-RM-THR [SYS_RSC_THR_SER=a]

 [Parameter]
  a = DECIMAL	(1:9999999999)

 [Usage]
  DEL-RM-THR [a]
   ex) DEL-RM-THR 1
  DEL-SYS-NAME [SYS_RSC_THR_SER=a]
   ex) DEL-RM-THR SYS_RSC_THR_SER=1

 [Result]
 <SUCCESS>
 Date time
 MMC    = DEL-RM-THR
 Result = SUCCESS
 ==========================================================================
  SER |       SYS_NAME        |    SORTATION    | MINOR | MAJOR | CRITICAL
 --------------------------------------------------------------------------
    1 | CLOUD_DEV             | CPU             |    70 |    80 |       90
 ==========================================================================

 <FAILURE>
 Date time
 MMC    = DEL-RM-THR
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['SYS_RSC_THR_SER'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'SYS_RSC_THR_SER', arg_data['SYS_RSC_THR_SER'], G_log)
		if (ret == False) : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DEL_RM_THR_MAX_PARAMETER, DEL_RM_THR_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)
	
			try :
				data_cnt, db_data, err = MMCLib.Select_query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", " where SYS_RSC_THR_SER={}".format(Parsing_Dict["SYS_RSC_THR_SER"]))
				if int(data_cnt) is 0:
					result = 'FAILURE'
					reason = "DB_DATA DOES NOT EXIST"
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
				if err is not None:
					result = 'FAILURE'
					reason = "Select Failure. [{}]".format(err)
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DEL-RM-THR(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB SELECT FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			try : 
				ret, err = MMCLib.Delete_query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "SYS_RSC_THR_SER={}".format(Parsing_Dict["SYS_RSC_THR_SER"]))
				if ret is False:
					result = "FAILURE"
					if err is not None:
						reason = "DELETE FAILURE. [{}]".format(err)
					else:
						reason = "DELETE FAILURE."
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				total_body=" {:^3} | {:^21} | {:^15} | {:^5} | {:^5} | {:^8}\n".format("SER", "SYS_NAME", "SORTATION", "MINOR", "MAJOR", "CRITICAL")
				total_body = total_body + ('-' * MMC_SEND_LINE_RM_THR_SUCCESS_CNT)
				row_buf = "\n {:3} | {:21} | {:15} | {:5} | {:5} | {:8}".format(db_data[0]["SYS_RSC_THR_SER"], db_data[0]["SYSTEM_NAME"], db_data[0]["SORTATION"], db_data[0]["THRESHOLD_MINOR"], db_data[0]["THRESHOLD_MAJOR"], db_data[0]["THRESHOLD_CRITICAL"])
				total_body = total_body + row_buf
			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DEL-RM-THR(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB DELETE FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except Exception as e:
		G_log.error('ADD_SYS_NAME(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, result, reason, total_body)


