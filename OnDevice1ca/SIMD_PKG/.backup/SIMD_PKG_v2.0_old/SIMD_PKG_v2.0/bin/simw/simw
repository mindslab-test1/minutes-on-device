#! /usr/bin/python
#-*- coding:utf-8 -*-

################################################################
import os
import sys
import json
import zmq
import re
import time
import readline
import ConfigParser
import traceback
import ipaddress
import socket
from datetime import datetime
from collections import OrderedDict
from flask import Flask, render_template, redirect, url_for, request
################################################################

app = Flask(__name__)

class ZmqPipline :
	def __init__(self):
		self.context = None
		self.socket = None

	# zmq producer로 동작
	def connect(self, collector_ip, collector_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PUSH)
		self.socket.connect("tcp://%s:%s" % (collector_ip, collector_port))

	def send(self, msg, flags=0):
		self.socket.send(msg, flags-flags)

	# zmq consumer로 동작
	def bind(self, bind_port):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		self.socket.bind("tcp://*:%s" % bind_port)
	
	def bind_random_port(self):
		self.context = zmq.Context()
		self.socket = self.context.socket(zmq.PULL)
		port=self.socket.bind_to_random_port("tcp://*")
		return port

	def recv(self, flags=0):
		return self.socket.recv(flags=flags)

	def close(self):
		self.socket.close()
		self.context.term()

def check_Ip_Address(value):
	print 'IP Address [{}]'.format(value)
	try:
		socket.inet_aton(value)
		print "[{}] is IP Address.".format(value)
		return True
	except ValueError:
		print "[{}] is not IP Address Format.".format(value)
		sys.exit(1)

def check_Decimal_And_Range(min, max, value):
	print type(value)
	if value.isdigit() == False :
		print "Only Use Unsigned Decimal"
		sys.exit(1)
	if int(value) > max or int(value) < min :
		print "[{}] is INVALID Range.".format(value)
		sys.exit(1)
	return True

######## Create Message
def req_msg(ReqMsgId, simw_ip, simw_port):
	ReqMsg = {}
	ReqMsg['msg_header'] = {}
	ReqMsg['msg_body'] = {}
	ReqMsg['msg_body']['client_ip'] = simw_ip
	ReqMsg['msg_body']['client_port'] = str(simw_port)
	ReqMsg['msg_header']['msg_id'] = ReqMsgId

	return ReqMsg


def init_process():
	### SIMw ConfigParser 객체 생성 #############################
	try:
		config = ConfigParser.RawConfigParser()
		config_read = config.read(os.getcwd() + '/simc.conf')
		if not config_read :
			config_read = config.read(os.getenv('MAUM_ROOT') + '/etc/simc.conf')
			if not config_read :
				print("CHeck the Config_file path")
				print("Config_file location is 'simw.py' path or '$MAUM_ROOT/etc'")
				sys.exit(1)
			else:
				pass
		else:
			pass

	### config_file 구조가 잘못 된 경우 예외처리
	except ConfigParser.ParsingError as e:
		print("Config_file is invalid. Check Config_file")
		sys.exit(1)

	return config

@app.route('/', methods=['GET', 'POST'])
def main() :
	try:
		config = init_process()

		######### simc.conf
		## simd_list 생성
		simd_list = OrderedDict()
		simd_value_list = []
		simd_items = config.items('SIMD_LIST')

		for i in range(len(simd_items)):
			simd_list[simd_items[i][0]] = simd_items[i][1]
			#print simd_list

			#### check server name
			if ("." not in simd_items[i][0]) :
				print "Invalid simd server name. [{}]".format(simd_items[i][0])
				sys.exit(1)

			#### check server ip:port
			if (":" not in simd_list[simd_items[i][0]]):
				print "Invalid simd server ip:port. [{}]".format(simd_list[simd_items[i][0]])
				sys.exit(1)
			#### check duplication for server ip:port
			if (simd_items[i][1] in simd_value_list):
				print "[{}] is duplicated".format(simd_items[i][1])
				sys.exit(1)
			else:
				simd_value_list.append(simd_items[i][1])

			#### get server number and name
			server_num, server_name = simd_items[i][0].split(".")

			#### get server ip and port
			simd_ip, simd_port = simd_items[i][1].split(":")	
			#### check ip
			check_Ip_Address(str(simd_ip))
			#### check port
			check_Decimal_And_Range(1025, 65536, simd_port)
			print "{}.{} = {}:{}".format(server_num, server_name, simd_ip, simd_port)

		## simw_ip
		simw_ip = config.get('CONF', 'my_ip')
		#### check ip
		check_Ip_Address(str(simw_ip))

	except ConfigParser.NoSectionError as e:
		print (e)
		print ("check the section of the Config_File")
		print ("SECTION IS 'CONF' or 'SIMD_LIST'")
		sys.exit(1)

	except ConfigParser.NoOptionError as e:
		print (e)
		print ("The Option in the 'CONF' Section : my_ip, port_range")
		sys.exit(1)

	except Exception as e :
		print ("System error : [{}]".format(e))
		print (traceback.format_exc())
		sys.exit(1)

	######################################################################################

			
	######################################################################################
	# SIMd select loop start
	######################################################################################
	notice = []
	error_message = ''
	simd_name = ''
	dsp_cnt = config.get('CONF', 'simw_display_cnt')
	dsp_path = config.get('CONF', 'simw_tmp_path')

	################ make temp buffer file
	now = time.localtime()
	nowdate = '.%04d%02d%02d%02d%02d%02d' % (now.tm_year, now.tm_mon, now.tm_mday, now.tm_hour, now.tm_min, now.tm_sec)
	simw_pid = os.getpid()
	dsp_path = dsp_path + '/' + nowdate + '_' + str(simw_pid) + '.txt'
	print dsp_cnt, dsp_path

	for list_option in sorted(simd_list):
		notice.append(" {} ( {} )".format(list_option, simd_list[list_option]))

	if request.method == 'POST' :
		simd = request.form['server']
		print 'input simd : ' + simd
		if len(simd) is not 0:
			# 1. input type decimal
			if simd.isdecimal() == True:
				id = str(simd) + '.'
				# Find SIMd Server Number
				exist = False
				for name in sorted(simd_list):
					if id in name :
						exist = True
						value = simd_list[name]
						ip, port = value.split(":")
						simd_name = name
						break
				if exist == True:
					return redirect(url_for('simd_command', simw_ip=simw_ip, simd_ip=ip, simd_port=port, simd_name=name, dsp_cnt=dsp_cnt, dsp_path=dsp_path))
				else:
					error_message = "'" + simd + "' is Not Exist"
			else:
				error_message = 'Please Input Number'
				
	
	return render_template('main.html', notice = notice, error_message = error_message)


@app.route("/simd_command", methods=["GET", "POST"])
def simd_command() :

	simw_ip = request.args.get('simw_ip')
	SimdZmqPull = ZmqPipline()
	simw_port = SimdZmqPull.bind_random_port()
	print ''
	print 'SIMw : ' + str(simw_ip) + ':' + str(simw_port)

	simd_ip = request.args.get('simd_ip')
	simd_port = request.args.get('simd_port')
	simd_name = request.args.get('simd_name')
	print 'simd : ' + simd_ip + ',' + simd_port
	simd_name = simd_name.split('.')
	simd_info = simd_name[1] + "(" + simd_ip + ":" + simd_port + ")"

	dsp_cnt = request.args.get('dsp_cnt')
	dsp_path = request.args.get('dsp_path')

	#create push_object
	SimdZmqPush = ZmqPipline()
	SimdZmqPush.connect(simd_ip, simd_port)

	################# connect_msg
	### connect_req send
	ReqMsgId = "Connection_Request"
	ConnectReq = req_msg(ReqMsgId, simw_ip, simw_port)
	JsonConnectReq = json.dumps(ConnectReq)
	SimdZmqPush.send(JsonConnectReq)
	send_time = time.time()

	## connect_MSG RECV
	while True:
		try :
			ConnectRsp = json.loads(SimdZmqPull.recv(flags=zmq.NOBLOCK))
			#ConnectRsp = json.loads(SimdZmqPull.recv())
			print("CONNECT_RESP : {}".format(ConnectRsp))
			server_connect_flag = True
			break
		except zmq.Again as e:
			#### Rsp timeout in 3 seconds
			if (time.time() - send_time > 5 ):
				print "Not exist Response from SIMd"
				error_message = 'Not exist Response from SIMd'
				SimdZmqPull.close()
				SimdZmqPush.socket.linger=1
				SimdZmqPush.close()
				return redirect(url_for('main'))
			else:
				time.sleep(0.001)
				continue

	## Command
	clear_flag = False
	MmcDatas = ''
	result = ''
	final_result = ['']
	tmp_result = []
	rsp_data = ''

	if request.method == 'POST':
		Command = request.form['command']
		print 'input command : ' + Command
		if (len(Command) == 0) :
			clear_flag = True

		SepCommand = Command.split(' ', 1)
		if len(SepCommand) < 1 :
			result = 'ERROR : Invalid Command'	
		elif len(SepCommand) < 2 :
			MmcCommand = SepCommand[0].lower()
			if (MmcCommand == 'exit') or (MmcCommand == 'quit') :
				ReqMsgId = "Close_MSG"
				CloseReq = req_msg(ReqMsgId, simw_ip, simw_port)
				jsonCloseReq = json.dumps(CloseReq)
				SimdZmqPush.send(jsonCloseReq)
				SimdZmqPush.close()
				# delete display file
				if os.path.exists(dsp_path):
					os.remove(dsp_path)
				return redirect(url_for('main'))
			if (MmcCommand == 'help') or ('dis-' in MmcCommand) or ('add-' in MmcCommand) or ('chg-' in MmcCommand) or ('del-' in MmcCommand) or ('exe-' in MmcCommand):
				MmcData = ''
			else:
				result = 'ERROR : Invalid Command'
		else:
			MmcCommand = SepCommand[0].lower()
			arg = SepCommand[1].strip()
			if (arg == ''):
				MmcDatas = ''
			elif (MmcCommand == 'help') and (arg != '') :
				MmcDatas = arg.lower()
			else :
				MmcDatas = arg.split(',')
				for i in range(len(MmcDatas)) :
					MmcDatas[i] = MmcDatas[i].replace(' ', '')

			print MmcDatas

		########################################################
		ReqMsgId = "MMC_Request"
		MmcReq = req_msg(ReqMsgId, simw_ip, simw_port)
		if (len(SepCommand) > 1) and (MmcCommand == 'help'):
			if (MmcDatas == '') or (MmcDatas == ' '):
				MmcReq['msg_body']['mmc'] = MmcCommand
				MmcReq['msg_body']['arg'] = MmcDatas
			else:
				MmcReq['msg_body']['mmc'] = MmcDatas
				MmcReq['msg_body']['arg'] = MmcCommand
		else:
			MmcReq['msg_body']['mmc'] = MmcCommand
			MmcReq['msg_body']['arg'] = MmcDatas

		JsonMmcReq = json.dumps(MmcReq)
		SimdZmqPush.send(JsonMmcReq)
		send_time = time.time()

		## command_MSG RECV
		while True:
			try :
				MmcRsp = json.loads(SimdZmqPull.recv())
				print("CONNECT_RESP : {}".format(ConnectRsp))
				break
			except zmq.Again as e:
				#### Mmc timeout in 3 seconds
				if (time.time() - send_time > 5 ):
					print "Not exist Response from SIMd"
					SimdZmqPull.close()
					SimdZmqPush.socket.linger=1
					SimdZmqPush.close()
					sys.exit(1)
				else:
					time.sleep(0.001)
					continue

		result = MmcRsp['msg_body']['data']
		print "'" + result + "'"

		#### temporary store data in file
		if os.path.exists(dsp_path):
			with open(dsp_path, "r") as file :
				while True:
					sentence = file.readline()
					if sentence:
						print sentence
						tmp_result.append(sentence)
					else:
						break

		# first
		##################### write buffer
		with open(dsp_path, "w") as file:
			file.write('\n')
			file.write(result)
			file.write('\n')
			file.close()

		# next
		##################### write tmp buffer
		with open(dsp_path, "a+") as file:
			file.write('\n')
			for sentence in tmp_result:
				file.write(sentence)
			file.write('\n')

		now_cnt = 0
		with open(dsp_path, "r") as file :
			while True:
				sentence = file.readline()
				if sentence:
					print sentence
					now_cnt += 1
					final_result.append(sentence)
				else:
					break
		print now_cnt

	# Connection Close After completion Command
	ReqMsgId = "Close_MSG"
	CloseReq = req_msg(ReqMsgId, simw_ip, simw_port)
	jsonCloseReq = json.dumps(CloseReq)
	SimdZmqPush.send(jsonCloseReq)
	SimdZmqPush.close()

	if clear_flag == True:
		final_result = ''
		with open(dsp_path, "w") as file:
			file.write('\n')

	return render_template('command.html', simd_info=simd_info, result=final_result)

if __name__ == "__main__":
	app.run(host="10.122.64.196")
