#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import traceback
import bcrypt
import pymysql
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_DB_PWD_MIN_PARAMETER = 2
CHG_DB_PWD_MAX_PARAMETER = 2

param_list = ['USER_ID', 'PASSWORD']
mandatory_list = ['USER_ID', 'PASSWORD']

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  CHG-DB-PWD [USER_ID=a], [PASSWORD=b]

 [Parameter]
  a = STRING    (1:100)
  b = STRING    (1:20)

 [Usage]
  CHG-DB-PWD [a, b]
   ex) CHG-DB-PWD minds@mindslab.ai, msl1234~
  CHG-DB-PWD [USER_ID=a], [PASSWORD=b]
   ex) CHG-DB-PWD USER_ID=minds@mindslab.ai, PASSWORD=msl1234~

 [Column information]
  USER_ID         : User Identity
  PASSWORD        : User Password

 [Result]
 <SUCCESS>
 Date time
 MMC    = CHG-DB-PWD
 Result = SUCCESS
 ====================================================
  USER_ID = ID
 ----------------------------------------------------
  PASSWORD : before -> after
 ====================================================
 
 <FAILURE>
 Date time
 MMC    = CHG-DB-PWD
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list) :

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['USER_ID'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'USER_ID', arg_data['USER_ID'], G_log)
		if (ret == False) : return False, reason

	if arg_data['PASSWORD'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING20_LENGTH, 'PASSWORD', arg_data['PASSWORD'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, CHG_DB_PWD_MIN_PARAMETER, CHG_DB_PWD_MAX_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')
			
			data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_USER", "USER_PW", "WHERE USER_ID = '{}'".format(Parsing_Dict['USER_ID']))
			if int(data_cnt) is 0:
				reason = "USER_ID [{}] DOES NOT EXIST.".format(Parsing_Dict['USER_ID'])
				return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
			if err is not None:
				reason = "Select Failure."
				return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

			pwd = b"{}".format(Parsing_Dict['PASSWORD'])
			enc_pwd = bcrypt.hashpw(pwd, bcrypt.gensalt(rounds=10, prefix=b"2a"))

			update_query = "update MINUTES_USER set USER_PW='{}' where USER_ID = '{}';".format(enc_pwd, Parsing_Dict['USER_ID'])
			ret, err = mysql.execute_for_simd(update_query)
			if ret is False:
				result = 'FAILURE'	
				if err is not None:
					reason = "DB UPDATE FAILURE. [{}]".format(err)
				else:
					reason = "DB UPDATE FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			total_body = total_body + ' USER_ID = {}\n'.format(Parsing_Dict['USER_ID'])
			total_body = total_body + ('-' * MMC_SEND_LINE_DB_PWD_CNT)
			total_body = total_body + '\n PASSWORD = {} \n         -> {} ({})'.format(db_data[0]['USER_PW'], enc_pwd, Parsing_Dict['PASSWORD'])

			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-DB-PWD(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_DB_PWD(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason = 'SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

