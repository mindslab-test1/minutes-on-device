#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import libmmc as MMCLib
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

ADD_RPC_INFO_MIN_PARAMETER = 2
ADD_RPC_INFO_MAX_PARAMETER = 2

param_list = ['RPC_NAME', 'COMMAND']
mandatory_list = ['RPC_NAME', 'COMMAND']

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  ADD-RPC-INFO [RPC_NAME=a], [COMMAND=b]

 [Parameter]
  a = STRING    (1:100)
  b = STRING    (1:100)

 [Usage]
  ADD-RPC-INFO [a, b]
  ex) ADD-RPC-INFO STT-RESTART, /home/minds/sh/stt_restart.sh
  ex) ADD-RPC-INFO RVC-DOWN, svctl_stop_rvcd
  ADD-RPC-INFO [RPC_NAME=a], [COMMAND=b]
  ex) ADD-RPC-INFO [RPC_NAME=STT-RESTART], [COMMAND=/home/minds/sh/stt_restart.sh]
  ex) ADD-RPC-INFO [RPC_NAME=RVC-DOWN], [COMMAND=svctl_stop_rvcd]
   
   * You have to input '_' as a space.

 [Section Options Configuration]
  RPC_NAME             : Remote Process Command Name
  COMMAND              : Real Command

 [Result]
  <SUCCESS>
  Date time
  MMC = ADD-RPC-INFO
  Result = SUCCESS
  =============================================
   RPC List
   ...
   ...
  =============================================

  <FAILURE>
  Date time
  MMC = ADD-RPC-INFO
  Result = FAILURE
  =============================================
  Reason = Reason for error
  =============================================

"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Prameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	#### Mandatory Parameter ####
	if arg_data['RPC_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'RPC_NAME', arg_data['RPC_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['COMMAND'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'COMMAND', arg_data['COMMAND'], G_log)
		if (ret == False) : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body = ''
	
	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/rpc_info.conf)
			rpc_config = ConfigParser.RawConfigParser()
			rpc_config.read(G_rpc_cfg_path)

			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, ADD_RPC_INFO_MAX_PARAMETER, ADD_RPC_INFO_MIN_PARAMETER)
			if ret == False:
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if(ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if(ret == False):
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get data from '/home/minds/MP/etc/rpc_info.conf' ###
			data = OrderedDict()
			org_item_value = rpc_config.items('RPC_COMMAND')
			for i in range(len(org_item_value)) :
				if org_item_value[i][0] == Parsing_Dict['RPC_NAME'].lower():
					reason = '[{}] is Already Exist.'.format(Parsing_Dict['RPC_NAME'].lower())
					return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))

			## add new rpc info
			Parsing_Dict['COMMAND'] = Parsing_Dict['COMMAND'].replace('_', ' ')
			value = rpc_config.set('RPC_COMMAND', Parsing_Dict['RPC_NAME'].lower(), Parsing_Dict['COMMAND'])
			if value == False :
				reason = 'Change Config File Failure. [{}]'.format(item)
				return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
			else :
				with open(G_rpc_cfg_path, 'w') as configfile :
					rpc_config.write(configfile)

			row = ''
			for item in data :
				if (row == ''):
					row = ' {0:20} = {1}'.format(item, data[item])
				else :
					row = '\n {0:20} = {1}'.format(item, data[item])
				total_body = total_body + row
			row = '\n {0:20} = {1}'.format(Parsing_Dict['RPC_NAME'].lower(), Parsing_Dict['COMMAND'])
			total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('ADD_RPC_INFO() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("ADD-RPC-INFO(), NoSectionError : [{}]".format(e))
		reason='ADD-RPC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error('ADD_RPC_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='ADD_RPC_INFO(), Config_Read error [{}]'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('ADD_RPC_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='ADD_RPC_INFO(), [{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


