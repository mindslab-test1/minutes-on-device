#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import common.logger as logger
import libmmc as MMCLib
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

EXEC_RPC_MIN_PARAMETER = 1
EXEC_RPC_MAX_PARAMETER = 1

param_list = ['RPC_NAME']
mandatory_list = ['RPC_NAME']

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  EXE-RPC-INFO [RPC_NAME=a]

 [Parameter]
  a = STRING    (1:100)

 [Usage]
  EXE-RPC-INFO [a]
  ex) EXE-RPC-INFO RVC-DOWN
  EXE-RPC-INFO [RPC_NAME=a]
  ex) EXE-RPC-INFO [RPC_NAME=RVC-DOWN]
   
 [Section Options Configuration]
  RPC_NAME     : Remote Process Command Name

 [Result]
  <SUCCESS>
  Date time
  MMC = EXE-RPC-INFO
  Result = SUCCESS
  =============================================
    RPC INFO
  =============================================

  <FAILURE>
  Date time
  MMC = EXE-RPC-INFO
  Result = FAILURE
  =============================================
  Reason = Reason for error
  =============================================

"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body = ''
	
	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/rpc_info.conf)
			rpc_config = ConfigParser.RawConfigParser()
			rpc_config.read(G_rpc_cfg_path)

			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, EXEC_RPC_MIN_PARAMETER, EXEC_RPC_MAX_PARAMETER)
			if ret == False:
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			if(ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if(ret == False):
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			### get data from '/home/minds/MP/etc/rpc_info.conf' ###
			data = OrderedDict()
			org_item_value = rpc_config.items('RPC_COMMAND')
			for i in range(len(org_item_value)) :
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))

			## get command
			command = data[Parsing_Dict['RPC_NAME'].lower()]
			print command
			os.system(command)

			row = ''
			row = ' {0:20} = {1}'.format(Parsing_Dict['RPC_NAME'].lower(), data[Parsing_Dict['RPC_NAME'].lower()])
			total_body = total_body + row

			result = 'SUCCESS'
			reason = ''
			G_log.info('EXE-RPC-INFO() Complete!!')
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("EXE-RPC-INFO(), NoSectionError : [{}]".format(e))
		reason='EXE-RPC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error('EXE-RPC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='EXE-RPC-INFO(), Config_Read error [{}]'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('EXE-RPC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='EXE-RPC-INFO(), [{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


