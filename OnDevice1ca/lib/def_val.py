#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser


DEF_MTYPE_UPLOAD=0
DEF_MTYPE_REALTIME=1
DEF_MTYPE_ALWAYS=99

DEF_STR_FALSE='0'
DEF_STR_TRUE='1'
DEF_FALSE=False
DEF_TRUE=True

DEF_NOTUSE_VRMIC='0'
DEF_USE_VRMIC='1'


def load_config(proc_name, conf_path) :
	try :
		var=dict()

		# COMMON
		var['proc_name']=proc_name.upper()

		conf=ConfigParser.ConfigParser()
		conf.read(conf_path)

		items=conf.items(var['proc_name'])
		for name, value in items :
			var[name]=value

		print("[Print Config]")
		for i in var :
			print( '  {} = {}'.format(i,var[i]))
		print("\n")

		return var

	except Exception as e:
		print("thread: exception raise fail. <%s>", e)
		return None


def get_log_level(proc_name, conf_path) :

	log.critical("main: CHG_LOG_LEVEL [{}]".format(json_msg))

	conf=ConfigParser.ConfigParser()
	conf.read(conf_path)

	#proc_name=g_var['proc_name'].upper()
	items=conf.items(proc_name.upper())
	for name, value in items :
		if name == 'log_level' :
			return value


