#from lexrankr import LexRank
#lexrank = LexRank()
#lexrank.summarize('''최근 ‘자연어 처리(Natural Language Processing, NLP)’ 기술이 더 정확해지고, 더 널리 보급되고 있다. 이는 음성 및 텍스트 기능으로 주류 기술을 강화하고 있다. 이를테면 이메일을 자연스러운 음성으로 읽어주거나 엑셀에서 스프레드시트 데이터에 관한 질문을 입력하면 자동 생성 차트와 피벗테이블 형태로 답을 받는 것이다. ''')
#summariex = lexrank.probe(None)
#for i, summary in enumerate(summariex) :
#	print(str(i), summary)

from gensim.summarization.summarizer import summarize
from newspaper import Article

#url = ' https://steemit.com/dclick/@wonsama/-181023--1540308198584'
#news = Article('''최근 ‘자연어 처리(Natural Language Processing, NLP)’ 기술이 더 정확해지고, 더 널리 보급되고 있다. 이는 음성 >및 텍스트 기능으로 주류 기술을 강화하고 있다. 이를테면 이메일을 자연스러운 음성으로 읽어주거나 엑셀에서 스프레드시트 데이터에 관한 질문을 입력하면 자동 생성 차트와 피벗테이블 형태로 답을 받는 것이다. ''', language='ko')
#news.download()
#news.parse()
#print(news.text)
text= '''슈퍼브에이아이가 미국 실리콘밸리 산 마테오에 위치한 현지 법인을 거점으로 북미 시장 대상 세일즈 및 마케팅 활동을 진행한다고 27일 발표했다.

인공지능 산업이 가장 활성화된 지역으로 꼽히는 미국은 관련 기업 약 2000여개가 현재 운영 중이며, 스타트업도 1400개에 달하고 있다. 

슈퍼브에이아이는 머신러닝 데이터 플랫폼 ‘스위트(Suite)’를 필두로 미국에 진출한다. 스위트는 인공지능 개발의 장벽을 해결하는 사업자간 거래(B2B) 서비스형 소프트웨어(SaaS)로, 고도화된 자체 ‘오토라벨링(Auto Labeling)’ 기술 구현한다. 스위트에는 '오토라벨링'을 비롯해 '통계 분석 툴', '파이썬 SDK·CLI 개발자 도구 연동' 등의 기능들이 탑재돼 있다.슈퍼브에이아이는 '이미지 분석 및 오토라벨링 디바이스 활용에 사용 가능한 딥러닝 네트워크를 훈련시키는 학습용 데이터를 생성하는 방법’ 등 미국 특허 5개를 출원 완료했다. 이번에 출원된 특허는 스위트 정식 버전 ‘스위트 1.0’에 탑재돼, 인공지능 데이터 가공 생산성을 강화하는데 중요한 역할을 하고 있다.

오픈소스 데이터 세트를 활용할 수 있는 별도 허브 페이지도 함께 운영된다. 슈퍼브에이아이 '데이터 세트(Data sets)' 섹션은 글로벌 오픈소스 데이터를 자유롭게 다운 받을 수 있는 공간인 동시에, 유저들이 직접 만든 데이터 세트를 서로 공유함으로써 인공지능 개발자들이 서로 교류할 수 있는 커뮤니티의 역할도 함께 수행한다. 

김현수 슈퍼브에이아이 대표는 “스위트를 통해 인공지능 세계 1위 국가인 미국에서 먼저 괄목할만한 성과를 내고자 한다"며 "인공지능 개발 과정의 최대 장벽 중 하나인 데이터 문제를 효율적으로 해결하고, 데이터 플랫폼 시장 내 비중 있는 개척자가 되기 위해 최선을 다하겠다"고 말했다.'''
#print(summarize(text, word_count=100))
#print(summarize(text, ratio=0.3))
print(summarize(text, ratio=0.7))

