DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop minutes-text-summarize
sleep 1
cp ${DIR}/mtsd.py $HOME/MP/bin/mtsd
svctl start minutes-text-summarize
