#!/usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, json 
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser

#####################################################################################
# MINDSLAB COMMON
####################################################################################
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
from common.dblib import DBLib
from common.def_val import *

g_var={}
g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
log=''

def check_delete_meta(mysql, ipc) :
	result, rowcnt, rows, error =SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_DELETE) 
	if(result == False) :
		log.critical('Delete Meta :: Select STT_META Failure [{}]' .format(error))
	for stt_meta in rows :
		if 'CREATE_TIME' in stt_meta:
			if stt_meta['CREATE_TIME'] :
				stt_meta['CREATE_TIME']=stt_meta['CREATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')
		if 'UPDATE_TIME' in stt_meta:
			if stt_meta['UPDATE_TIME'] :
				stt_meta['UPDATE_TIME']=stt_meta['UPDATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')

		#기존에 회의중일수도 있을수도 있으므로 END 메시지를 보내자
		meta_ser = stt_meta['STT_META_SER']
		minutes_type = stt_meta['MINUTES_TYPE']

		if minutes_type == SQL.META_TYPE_REALTIME :
			event_push(mysql, ipc, stt_meta, '#DELETE#')

		log.debug("[Delete META] Delete STT_META [{}]".format(meta_ser))

		try :
			log.debug("[Delete META] <{}> Delete SrcFile[{}]".format(meta_ser, stt_meta['SRC_FILE_PATH']))
			os.remove(stt_meta['SRC_FILE_PATH'])
		except Exception as e :
			log.critical("[Delete META] <{}> Delete Src File Falure [{}]".format(meta_ser, e))

		if stt_meta['DST_FILE_PATH'] :
			try :
				dst_file_split=stt_meta['DST_FILE_PATH'].split('.')
				dst_file_split[-1] ='wav'
				wav_path= ".".join(dst_file_split)
				dst_file_split[-1] ='mp3'
				mp3_path= ".".join(dst_file_split)
			except Exception as e :
				pass

		try :
			log.debug("[Delete META] <{}> Delete DST wav File[{}]".format(meta_ser, wav_path))
			os.remove(wav_path)
		except Exception as e :
			log.critical("[Delete META] <{}> Delete Dst File Falure [{}]".format(meta_ser, e))

		try :
			log.debug("[Delete META] <{}> Delete DST mp3 File[{}]".format(meta_ser, mp3_path))
			os.remove(mp3_path)
		except Exception as e :
			log.critical("[Delete META] <{}> Delete Dst File Falure [{}]".format(meta_ser, e))

		ret, error = SQL.delete_tmp_mic_byid(mysql, stt_meta['MINUTES_ID'])
		if ret == False :
			log.critical("[Delete META] <{}> Delete TEMP_MIC Falure [{}]".format(meta_ser, error))

		ret, error = SQL.delete_stt_result_bymeta(mysql, meta_ser)
		if ret == False :
			log.critical("[Delete META] <{}> Delete STT_RESULT Falure [{}]".format(meta_ser, error))

		ret, error = SQL.delete_summarize(mysql, meta_ser)
		if ret == False :
			log.critical("[Delete META] <{}> Delete SUMMARIZE Falure [{}]".format(meta_ser, error))

		ret, error = SQL.delete_stt_meta_bymeta(mysql, meta_ser)
		if ret == False :
			log.critical("[Delete META] <{}> Delete STT_META Falure [{}]".format(meta_ser, error))
		ret, error = SQL.delete_stt_meta_bk_bymeta(mysql, meta_ser)
		if ret == False :
			log.critical("[Delete META] <{}> Delete STT_META_BK Falure [{}]".format(meta_ser, error))

		

def event_push(mysql, ipc, stt_meta, status) :
	minutes_type=stt_meta['MINUTES_TYPE']

	if minutes_type == SQL.META_TYPE_UPLOAD :
		send_target='mccd'
		container=dict()
	elif minutes_type == SQL.META_TYPE_REALTIME :
		send_target='rsrd'
		container=dict()
		if status == '#START#' :
			container['MINUTES_NAME']=stt_meta['MINUTES_NAME']
			container['TOPIC']=stt_meta['MINUTES_TOPIC']
			container['MEETINGROOM']=stt_meta['MINUTES_MEETINGROOM']
			container['START_TIME']=stt_meta['START_TIME']
			container['MIC_INFO']=SQL.make_mic_info_str(mysql, stt_meta)

	send_msg=IPC.MAKE_Event_Collection_Push(stt_meta['STT_META_SER'], status, stt_meta, container)
	#send_msg=make_event_push_buf(stt_meta, status, container)

	try :
		json_string = json.dumps(send_msg)
		ipc.IPC_Send(send_target, json_string.encode('utf-8'))
		log.critical('Send STT_META SUCCESS :: (STT_META_SER : {})'.format(stt_meta['STT_META_SER']))
		return True

	except Exception as e:
		log.critical(traceback.format_exc())
		log.critical('Send STT_META FAIL :: (STT_META_SER : {})'.format(stt_meta['STT_META_SER']))
		return False

def update_meta_status(mysql, stt_meta, meta_status) :
	meta_ser=stt_meta['STT_META_SER']
	#ret, error=SQL.update_stt_meta_status(mysql, meta_ser, SQL.META_STAT_WORKING)
	ret, error=SQL.update_stt_meta_status(mysql, meta_ser, meta_status)
	if ret == True :
		log.info("Send and Update META SUCC:: (STT_META_SER={}/STATUS={})".format(meta_ser, meta_status))
	else :
		log.critical("Send and Update META FAIL:: (STT_META_SER={}), {}".format(meta_ser, error))
		return False

	return True

def init_process():
	global g_var

	try:
		# Init Config
		try:
			proc_name=os.path.basename(sys.argv[0])
			g_var=load_config(proc_name, g_conf_path)
			g_var['select_interval'] = 1
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return False, None, None, None

		# Init Logger
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', g_var['proc_name'].lower(), g_var['log_level'], True)

		# MySql Class 생성
		mysql = DBLib(log)
		# MySql DB 접속
		db_conn = mysql.connect()

		ipc=IPC.MindsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			return False, None, None, None
		ret=ipc.IPC_Regi_Process('mccd')
		if ret == False :
			log.critical("IPC_REGI_PROCESS FAIL")
			return False, None, None, None
		ret=ipc.IPC_Regi_Process('rsrd')
		if ret == False :
			log.critical("IPC_REGI_PROCESS FAIL")
			return False, None, None, None

		# Init Signal
		#signal.signal(signal.SIGINT, sig_int_handler)

	except Exception as e:
		log.critical("main: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, None, None, None
	else:
		return True, mysql, log, ipc


def update_meetingroom_status(mysql, meetingroom_ser, status) :

	if status == '#START#' :
		if meetingroom_ser :
			ret, error=SQL.update_minutes_meetingroom_status(mysql, meetingroom_ser, SQL.MEETINGROOM_STATUS_USED)
			if ret == False :
				log.critical("MEETINGROOM STATUS UPDATE FAILURE [{}]".format(error))
				return False

	elif status == '#END#' :
		if meetingroom_ser :
			ret, error=SQL.update_minutes_meetingroom_status(mysql, meetingroom_ser, SQL.MEETINGROOM_STATUS_NONE)
			if ret == False :
				log.critical("MEETINGROOM STATUS UPDATE FAILURE [{}]".format(error))
				return False

	elif status == '#ERROR#' :
		if meetingroom_ser :
			ret, error=SQL.update_minutes_meetingroom_status(mysql, meetingroom_ser, SQL.MEETINGROOM_STATUS_NONE)
			if ret == False :
				log.critical("MEETINGROOM STATUS UPDATE FAILURE [{}]".format(error))
				return False

	return True

def proc_event(mysql, ipc, stt_meta, meta_event_str) :

	meta_ser = stt_meta['STT_META_SER']
	meetingroom_ser = stt_meta['MINUTES_MEETINGROOM_SER']
	minutes_type = stt_meta['MINUTES_TYPE']
	if minutes_type == SQL.META_TYPE_REALTIME :
		result, mic_info_list=SQL.get_mic_list_from_meta(mysql, stt_meta)
		if result == False  :
			log.critical("[META EVENT] REALTIME. Select MIC Failure (META : {} / STATUS : {})".format(meta_ser, meta_event_str))
		elif len(mic_info_list) == 0 : 
			log.critical("[META EVENT] REALTIME. Select MIC Not Found (META : {} / STATUS : {})".format(meta_ser, meta_event_str))
			return False

	update_meetingroom_status(mysql, meetingroom_ser, meta_event_str)

	if meta_event_str == '#START#' :

		if minutes_type == SQL.META_TYPE_REALTIME :
			for mic_info in mic_info_list :
				if mic_info['USED_FLAG'] == DEF_STR_FALSE : 
					continue
				if mic_info['VIRTUAL_MIC_FLAG'] == DEF_STR_FALSE :
					continue
				else :
					SQL.update_mic_split_status(mysql, mic_info['MINUTES_MIC_SER'], SQL.SPLIT_STATUS_INIT)

		ret=update_meta_status(mysql, stt_meta, SQL.META_STAT_WORKING)
		if ret == False :
			log.critical("Update Meta Status Failure [{}]".format(meta_ser))
			return False

	elif meta_event_str == '#END#' :

		if minutes_type == SQL.META_TYPE_REALTIME :
			for mic_info in mic_info_list :
				if mic_info['USED_FLAG'] == DEF_STR_FALSE : 
					continue
				if mic_info['VIRTUAL_MIC_FLAG'] == DEF_STR_FALSE :
					continue
				else :
					SQL.update_mic_split_status(mysql, mic_info['MINUTES_MIC_SER'], SQL.SPLIT_STATUS_RELEASE)

		ret=update_meta_status(mysql, stt_meta, SQL.META_STAT_STOPING)
		if ret == False :
			log.critical("Update Meta Status Failure [{}]".format(meta_ser))
			return False

	elif meta_event_str == '#ERROR#' :

		if minutes_type == SQL.META_TYPE_REALTIME :
			for mic_info in mic_info_list :
				if mic_info['USED_FLAG'] == DEF_STR_FALSE : 
					continue
				if mic_info['VIRTUAL_MIC_FLAG'] == DEF_STR_FALSE :
					continue
				else :
					SQL.update_mic_split_status(mysql, mic_info['MINUTES_MIC_SER'], SQL.SPLIT_STATUS_RELEASE)

		ret=update_meta_status(mysql, stt_meta, SQL.META_STAT_GENERAL_ERROR)
		if ret == False :
			log.critical("Update Meta Status Failure [{}]".format(meta_ser))
			return False

	return True 

def garbage_collection(mysql) :
	work_stat_list=list()
	work_stat_list.append(SQL.META_STAT_WORKING)
	work_stat_list.append(SQL.META_STAT_STOPING)

	restart_rows=list()
	for work_stat in work_stat_list :
		# Select `FILEUPLOAD` START and `REALTIME` START Event
		result, rowcnt, rows, error =SQL.select_stt_meta_bystatus(mysql, work_stat) 
		if(result == False) :
			log.critical('START :: Select STT_META Failure [{}]' .format(error))
		else :
			if(rowcnt > 0) :
				log.critical('Select [{}] Incomplete Meta [STATUS:{}]' .format(rowcnt, work_stat))
				restart_rows.extend(rows)
			else :
				continue

		log.critical('Total Stoping META Count is [{}]' .format(len(restart_rows)))
		for row in restart_rows :
			if row['MINUTES_TYPE'] == SQL.META_TYPE_UPLOAD :
				update_meta_status(mysql, row, SQL.META_STAT_START)
			elif row['MINUTES_TYPE'] == SQL.META_TYPE_REALTIME:
				update_meta_status(mysql, row, SQL.META_STAT_STOP)


def main():
	global log

	# Init Process
	result, mysql, log, ipc = init_process()
	if result == False :
		log.critical("[MECd] STARTING FAILURE ")
		sys.exit(1)

	log.critical("[MECd] STARTING.... " )
	garbage_collection(mysql)

	# Start MainLoop
	while True:
		try:

			time.sleep(g_var['select_interval'])
			msg=ipc.IPC_Recv()
			if msg :
				json_msg = json.loads(msg)
				log.critical("main: JSON MSG recved [{}]".format(json_msg))
				if 'msg_header' not in json_msg or 'msg_body' not in json_msg :
					log.critical("main: Invalid JSON MSG recved [{}]".format(json_msg))
					continue

				elif json_msg['msg_header']['msg_id'] == 'CHG_LOG_LEVEL' :
					g_var['log_level'] = get_log_level(g_var['proc_name'], g_conf_path)
					logger.change_logger_level(log, g_var['log_level'])
					log.critical("main: CHG_LOG_LEVEL [{}]".format(g_var['log_level']))
					continue

				else :
					log.info("recv :: Unknown MSG : {} " .format(json_msg))
	
			################################################################
			#                 EVENT_COLLECTION                             # 
			################################################################
			check_delete_meta(mysql, ipc)
			
			# Select `FILEUPLOAD` START and `REALTIME` START Event
			result, rowcnt, rows, error =SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_START) 
			if(result == False) :
				log.critical('START :: Select STT_META Failure [{}]' .format(error))
			else :
				if(rowcnt > 0) :
					log.info('START :: find [{}] new data' .format(rowcnt))
				else :
					pass

				for row in rows :
					stt_meta=row
					# Datatime Attr Remove
					if 'CREATE_TIME' in stt_meta:
						if stt_meta['CREATE_TIME'] :
							stt_meta['CREATE_TIME']=stt_meta['CREATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')
					if 'UPDATE_TIME' in stt_meta:
						if stt_meta['UPDATE_TIME'] :
							stt_meta['UPDATE_TIME']=stt_meta['UPDATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')

					meta_ser=stt_meta['STT_META_SER']

					ret=proc_event(mysql, ipc, stt_meta, '#START#')
					if ret == True :
						log.critical("[EVENT_PUSH] PROC Event Success (META : {} / STATUS : {})".format(meta_ser, "#START#"))
						event_push(mysql, ipc, stt_meta, '#START#')
					else :
						log.critical("[EVENT_PUSH] PROC Event Failure (META : {} / STATUS : {})".format(meta_ser, "#START#"))
						ret=proc_event(mysql, ipc, stt_meta, '#ERROR#')
						if ret == True :
							log.critical("[EVENT_PUSH] PROC Event Success (META : {} / STATUS : {})".format(meta_ser, "#ERROR#"))
						else :
							log.critical("[EVENT_PUSH] PROC Event Failure (META : {} / STATUS : {})".format(meta_ser, "#ERROR#"))
							meetingroom_ser = stt_meta['MINUTES_MEETINGROOM_SER']
							update_meta_status(mysql, stt_meta, SQL.META_STAT_GENERAL_ERROR)
							update_meetingroom_status(mysql, meetingroom_ser, '#END#')

						event_push(mysql, ipc, stt_meta, '#ERROR#')


			# Select `REALTIME` STOP Event
			result, rowcnt, rows, error =SQL.select_stt_meta_bystatus(mysql, SQL.META_STAT_STOP) 
			if(result == False) :
				log.critical('STOP :: Select STT_META Failure [{}]' .format(error))
			else :
				if(rowcnt > 0) :
					log.info('STOP :: find [{}] new data' .format(rowcnt))
				else :
					pass

				for row in rows :
					stt_meta=row
					# Datatime Attr Remove
					if 'CREATE_TIME' in stt_meta:
						if stt_meta['CREATE_TIME'] :
							stt_meta['CREATE_TIME']=stt_meta['CREATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')
					if 'UPDATE_TIME' in stt_meta:
						if stt_meta['UPDATE_TIME'] :
							stt_meta['UPDATE_TIME']=stt_meta['UPDATE_TIME'].strftime('%Y-%m-%d %H:%M:%S')

					meta_ser=stt_meta['STT_META_SER']

					ret=proc_event(mysql, ipc, stt_meta, '#END#')
					if ret == True :
						log.critical("[EVENT_PUSH] PROC Event Success (META : {} / STATUS : {})".format(meta_ser, "#END#"))
						event_push(mysql, ipc, stt_meta, '#END#')
					else :
						log.critical("[EVENT_PUSH] PROC Event Failure (META : {} / STATUS : {})".format(meta_ser, "#END#"))
						ret=proc_event(mysql, ipc, stt_meta, '#ERROR#')
						if ret == True :
							log.critical("[EVENT_PUSH] PROC Event Success (META : {} / STATUS : {})".format(meta_ser, "#ERROR#"))
						else :
							log.critical("[EVENT_PUSH] PROC Event Failure (META : {} / STATUS : {})".format(meta_ser, "#ERROR#"))
							meetingroom_ser = stt_meta['MINUTES_MEETINGROOM_SER']
							update_meta_status(mysql, stt_meta, SQL.META_STAT_GENERAL_ERROR)
							update_meetingroom_status(mysql, meetingroom_ser, '#END#')

						event_push(mysql, ipc, stt_meta, '#ERROR#')
							
		except Exception as e:
			log.critical("main: exception raise fail. <%s>", e)
			log.critical(traceback.format_exc())

	log.critical("main stooped...")
	
	ipc.IPC_Close()
	mysql.disconnect()
	return 0

if __name__ == '__main__':
	main()


