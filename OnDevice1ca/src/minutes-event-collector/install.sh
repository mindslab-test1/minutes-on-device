DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop minutes-event-collector
sleep 1
cp ${DIR}/mecd.py $HOME/MP/bin/mecd
svctl start minutes-event-collector
