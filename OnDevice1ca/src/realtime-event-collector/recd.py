#!/usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, json, socket, signal
import traceback
import multiprocessing
from multiprocessing import Value, Array
from ctypes import c_bool

import re

is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser

#####################################################################################
# MINDSLAB COMMON
####################################################################################
import common.logger as logger
from common.dblib import DBLib
import common.minds_ipc as IPC
import common.minutes_sql as SQL


g_var={}
log=None
main_work_flag=Value(c_bool,True)

def send_mic_event(ipc, mic_ser, mic_id, mic_status) :
	try :
		send_msg=dict()
		send_msg['msg_header']=dict()
		send_msg['msg_header']['msg_id']='MIC_EVENT_COLLECTION_PUSH'
		send_msg['msg_body']=dict()
		send_msg['msg_body']['mic_ser']=mic_ser
		send_msg['msg_body']['virtual_mic_id']=mic_id
		send_msg['msg_body']['status']=mic_status

		json_string = json.dumps(send_msg)
		ipc.IPC_Send('rsrd', json_string.encode('utf-8'))
		log.info("[MIC_EVENT_PUSH] : {}" .format(send_msg))
		return True
	except Exception as e :
		log.critical("Exception raise : <%s>", e)
		log.critical(traceback.format_exc())
		return False


def tcp_connection(mysql, work_flag, ipaddr, port, host_id, host_name) :
	connecting_cnt=0
	while work_flag.value == True :
		try :
			log.critical('trying Connection [{}/{}]!!!!'.format(host_id, host_name))
			#cli_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM )
			cli_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM | socket.SOCK_NONBLOCK)
			cli_socket.settimeout(1)
			cli_socket.connect((ipaddr,int(port)))
			break
		except ConnectionRefusedError :
			log.info('Connection refused###########')

		except Exception as e :
			log.critical("Exception raise : <%s>", e)
			log.critical(traceback.format_exc())

		connecting_cnt=connecting_cnt+1
		if connecting_cnt > 3 :
			ret, error = SQL.update_mic_split_status(mysql, host_id, SQL.SPLIT_STATUS_FAILED)
			if ret == False :
				log.critical("WL_DEVICE STATUS UPDATE(FAILED) FAILURE[{}]".format(error))

		time.sleep(int(g_var['reconnect_interval']))

	return cli_socket

def load_config():
	global g_var

	g_var['proc_name'] = os.path.basename(sys.argv[0])
	g_var['select_interval'] = 1
	proc_name=g_var['proc_name'].upper()
	g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)
	#sections=conf.sections()
	items=conf.items(proc_name)
	for name, value in items :
		g_var[name]=value

	# RECORD
	for i in g_var :
		print( '{} = {}'.format(i,g_var[i]))

	return


def get_mic_list(mysql, mic_ser) :
	mic_info_list=list()

	result, rowcnt, rows, error = SQL.select_minutes_virtual_mic(mysql, mic_ser)
	if result == False :
		log.critical("Select VIRTUAL_MIC FAILURE [{}]/[{}]".format(mic_ser, error))
		return None
	elif rowcnt== 0 :
		log.critical("VIRTUAL_MIC Not Found [{}/{}]".format(mic_ser))
		return None

	for row in rows:
		vr_mic_ser=row['MINUTES_VIRTUAL_MIC_SER']
		vr_mic_id=row['VIRTUAL_MIC_ID']
		vr_mic_name=row['VIRTUAL_MIC_NAME']
		vr_tmp_mic_name=row['VIRTUAL_TMP_MIC_NAME']
		if row['USED_FLAG'].upper() == '0' :
			log.critical("MINUTES_MIC[{}] Not Use [{}/{}]".format(mic_group, vr_mic_ser, vr_mic_name))
			continue
		else :
			mic_info=dict()
			mic_info['SER'] = vr_mic_ser
			mic_info['ID'] = vr_mic_id
			mic_info['NAME'] = vr_mic_name
			mic_info['STATUS'] = None
			mic_info_list.append(mic_info)


	log.info("[MIC_LIST] (MIC_SER : {})" .format(mic_ser))
	for mic_info in mic_info_list :
		log.info("  ID : {}, NAME : {}".format(mic_info['ID'], mic_info['NAME']))


	if len(mic_info_list) > 0 :
		return mic_info_list
	else :
		return None




def init_process():
	global g_var
	global log

	try:
		# Init Config
		try:
			load_config()
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return False, None, None, None

		# Init Logger
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', g_var['proc_name'], g_var['log_level'], True)

		# MySql Class 생성
		mysql = DBLib(log)
		# MySql DB 접속
		db_conn = mysql.connect()

		ipc=IPC.MindsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			return False, None, None, None
		ret=ipc.IPC_Regi_Process('rsrd')
		if ret == False :
			log.critical("IPC_REGI_PROCESS FAIL")
			return False, None, None, None
	
		# Init Signal
		signal.signal(signal.SIGTERM, sig_term_handler)

	except Exception as e:
		log.critical("main: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, None, None, None
	else:
		return True, mysql, log, ipc


def sig_term_handler(signum, data) :
	log.critical("{}/{}".format(signum, data))
	main_work_flag.value=False

def tcp_send_recv(cli_socket, send_buf) :
	while True :
		send_len=0
		send_len=cli_socket.send(send_buf[send_len:])
		if send_len < len(send_buf) :
			continue
		else :
			break
	
	# Mayby Blocking?
	recv_buf=cli_socket.recv(4096)
	if len(recv_buf) == 0 :
		return False, None
	
	return True, recv_buf
	

def devision_cmd(cmd_buf) :
	cmd_list=cmd_buf.split(">")

	for i in range(len(cmd_list)) :
		cmd_list[i]=cmd_list[i].replace("<", "")

	while "" in cmd_list :
		cmd_list.remove("")

	return cmd_list

def devision_attr(attr_buf) :
	try :
		if attr_buf[0] == " " :
			attr_buf=attr_buf.replace(" ","",1)

		attr_list=attr_buf.split(" ", 3)
	#	while True : 
	#		log.critical("{}/{}" .format(attr_list, len(attr_list)))
	#		if	"" in attr_list :
	#			attr_list.remove("")
	#		else :
	#			break
		return attr_list

	except Exception as e :
		log.critical("tcp_control: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return None


	
def Send_Msg(cli_socket, msg_buf) :
	if True :
		#send_buf=bytearray(msg_buf.encode('cp949'))
		send_buf=bytearray(len(msg_buf))
		for i in range(len(msg_buf)) :
			send_buf[i]=ord(msg_buf[i])
		send_len=cli_socket.send(send_buf)
		return True 
	else :
		send_len=0
		while True :
			send_buf=bytearray(msg_buf.encode('cp949'))
			send_len=cli_socket.send(send_buf[send_len:])
			if send_len < len(send_buf) :
				log.info("1")
				continue
			else :
				log.info("2")
				break
	return True

def proc_mic_status(ipc, mic_ser, vr_mic_list , cmd_attr_list) :
	if len(cmd_attr_list) == 4 :
		cmd_type=cmd_attr_list[0]
		target_id=cmd_attr_list[1]
		cmd_str=cmd_attr_list[2]
		cmd_status=cmd_attr_list[3]
		for vr_mic in vr_mic_list :
			log.info(" target_id {} == vr_mic_id {}" .format(target_id, vr_mic['ID']))
			if str(target_id) == str(vr_mic['ID']) :
				vr_mic['STATUS']=cmd_status.upper()
				send_mic_event(ipc, mic_ser, vr_mic['SER'], cmd_status.upper())
				log.critical(vr_mic)
				break
	else :
		log.critical("[MIC_STATUS] Invalid Command String [{}/{}]".format(cmd_attr_list, len(cmd_attr_list)))

def run_worker(work_flag, mic_info) :

	ipc=IPC.MindsIPCs(log, g_var['proc_name'])
	ret=ipc.IPC_Regi_Process('rsrd')
	if ret == False :
		log.critical("IPC_REGI_PROCESS FAIL")
		return False

	# MySql Class 생성
	mysql = DBLib(log)
	# MySql DB 접속
	db_conn = mysql.connect()

	cli_socket = None
	#last_check_available=0
	mic_ser=mic_info['MINUTES_MIC_SER']
	mic_id=mic_info['MIC_ID']
	mic_name=mic_info['MIC_NAME']
	ip=mic_info['VIRTUAL_SPLIT_IP']
	port=mic_info['VIRTUAL_SPLIT_PORT']

	log.critical(" Process Start [{}/{}] {}:{}".format(mic_ser, mic_name, ip, port))

	ret, error = SQL.update_mic_split_status(mysql, mic_ser, SQL.SPLIT_STATUS_CONNECTING)
	if ret == False :
		log.critical("update_mic_split_status(CONNECTING) FAILURE[{}]".format(error))

	while work_flag.value == True:
		try :
			if cli_socket :
				log.critical("socket reinit")
				cli_socket.close()
				cli_socket=None

			# CONNECTION !!!!!!!!!
			cli_socket=tcp_connection(mysql, work_flag, ip, port, mic_ser, mic_name)
			if work_flag.value == False :
				break
			log.critical("tcp_connection SUCCESS [{}]" .format(cli_socket))

			#time.sleep(5)
			ret, error = SQL.update_mic_split_status(mysql, mic_ser, SQL.SPLIT_STATUS_CONNECTED)
			if ret == False :
				log.critical("update_mic_split_status(CONNECTED) FAILURE[{}]".format(error))


			#cmd="< SET 1 SEAT_NAME {" + mic_list[0]['NAME'] + "} >"
			vr_mic_list=get_mic_list(mysql, mic_ser)
			if not vr_mic_list :
				log.critical(" Select MIC_LIST FAIL [{}/{}] {}:{}".format(mic_ser, mic_name, ip, port))
				continue

			time.sleep(2)
			for vr_mic_info  in vr_mic_list :
				cmd="< SET " + str(vr_mic_info['ID']) + " SEAT_NAME " + "{" + str(vr_mic_info['NAME']) + "} >"
				log.info(cmd)
				ret=Send_Msg(cli_socket, cmd)
				if ret == False :
					log.critical("send Msg Failure [{}]".format(cmd))


			while work_flag.value == True :

				try :
					#cmd="< GET 0 UNIT_AVAILABLE >"

					#cmd="< GET 0 SEAT_NAME >"
					#log.info(cmd)
					#ret=Send_Msg(cli_socket, cmd)
					#if ret == False :
					#	log.critical("Send Msg Failure [1]")

					#cmd="< GET 0 UNIT_AVAILABLE >"
					#ret=Send_Msg(cli_socket, cmd)
					#cur_time=time.time()
					#if  (int(last_check_available) + int(g_var['status_check_interval'])) < cur_time :
					#	cmd="< GET 0 UNIT_AVAILABLE >"
					#	log.critical(cmd)
					#	ret=Send_Msg(cli_socket, cmd)
					#	last_check_available=cur_time
						

					#cmd="< GET 0 UNIT_AVAILABLE >"
					#log.critical(cmd)
					#ret=Send_Msg(cli_socket, cmd)
					recv_buf=cli_socket.recv(4096)
					if recv_buf :
						pass

					cmd_list=devision_cmd(recv_buf.decode('cp949'))
					for cmd in cmd_list :
						log.info("COMMAND : {}".format(cmd))
						attr_list=devision_attr(cmd)
						cmd_str= attr_list[2]
						
						if "MIC_STATUS" in cmd_str :
							proc_mic_status(ipc, mic_ser, mic_list, attr_list)

						else :
							#log.critical(" Invalid Command String [{}]".format(cmd))
							continue

					#	time.sleep(0.01)

					#log.info(recv_buf)

				except socket.timeout :
					#time.sleep(0.01)
					continue
				except Exception as e :
					time.sleep(1)
					log.critical("tcp_recv_data: exception raise fail. <%s>", e)
					log.critical(traceback.format_exc())
					break

		except Exception as e :
			log.critical("tcp_connection: exception raise fail. <%s>", e)
			log.critical(traceback.format_exc())
			ret, error = SQL.update_mic_split_status(mysql, mic_ser, SQL.SPLIT_STATUS_FAILED)
			if ret == False :
				log.critical("update_mic_split_status(FAILED) FAILURE[{}]".format(error))

		ret, error = SQL.update_mic_split_status(mysql, mic_ser, SQL.SPLIT_STATUS_NONE)
		if ret == False :
			log.critical("update_mic_split_status(NONE) FAILURE[{}]".format(error))
		log.critical("Stop Thread!!!!!!!!!!!!!!!!!!")


def main () :
	#global log
	result, mysql, log, ipc = init_process()
	if result == False :
		print("[MECd] STARTING FAILURE ")
		#log.critical("[MECd] STARTING FAILURE ")
		sys.exit(1)

	log.critical("[MECd] STARTING.... " )

	worker_dict=dict()

	#host_list=[]
	#host_list.append({'ID':'1', 'NAME':'TEST1', 'IP':'10.122.65.200', 'PORT':'2202'})
	#while True :
	while main_work_flag.value == True :
		try :

			time.sleep(1)

			result, rowcnt, rows, error  = SQL.select_minutes_mic_bysplit_status(mysql, SQL.SPLIT_STATUS_INIT)
			if result == False :
				log.critical("Select MINUTES_MIC_BYSPLIT_STATUS(INIT) Failure [{}]".format(error))
			for row in rows :

				mic_ser=row['MINUTES_MIC_SER']
				mic_id=row['MIC_ID']
				mic_name=row['MIC_NAME']
				ip=row['VIRTUAL_SPLIT_IP']
				port=row['VIRTUAL_SPLIT_PORT']

				if mic_ser in worker_dict:
					log.critical(" MIC[{}/{}] is Already Opend".format(mic_ser,mic_name))
					continue

				log.info("[{}/{}] {}:{} STARTING !!!".format(mic_ser, mic_name, ip, port))

				work_flag=Value(c_bool,True)
				p = multiprocessing.Process(target=run_worker, args=(work_flag, row))
				p.daemon = True
				p.start()

				worker_dict[mic_ser]=dict()
				worker_dict[mic_ser]['process']=p
				worker_dict[mic_ser]['work_flag']=work_flag
				worker_dict[mic_ser]['mic_row']=row

			result, rowcnt, rows, error  = SQL.select_minutes_mic_bysplit_status(mysql, SQL.SPLIT_STATUS_RELEASE)
			if result == False :
				log.critical("Select MINUTES_MIC_BYSPLIT_STATUS(INIT) Failure [{}]".format(error))
			for row in rows :

				mic_ser=row['MINUTES_MIC_SER']
				mic_id=row['MIC_ID']
				mic_name=row['MIC_NAME']
				ip=row['VIRTUAL_SPLIT_IP']
				port=row['VIRTUAL_SPLIT_PORT']

				if mic_ser not in worker_dict:
					log.critical(" MIC [{}/{}] is Not Opend" .format(mic_ser, mic_name))

					ret, error = SQL.update_mic_split_status(mysql, mic_ser, SQL.SPLIT_STATUS_NONE)
					if ret == False :
						log.critical("update_mic_split_status(NONE) FAILURE[{}]".format(error))
					continue

				worker_dict[mic_ser]['work_flag'].value=False
				worker_dict[mic_ser]['process'].join()
				del worker_dict[mic_ser]
				log.info("[{}/{}] {}:{} STOP !!!".format(mic_ser, mic_name, ip, port))

		except Exception as e :
			log.critical("Exception raise : <%s>", e)
			log.critical(traceback.format_exc())

	for worker_id in worker_dict :
		worker_dict[worker_id]['work_flag'].value=False
		worker_dict[worker_id]['process'].join()
	log.critical("Process Terminate!!!!!!!!!!!!!!!!!")




if __name__ == '__main__':
	main()
