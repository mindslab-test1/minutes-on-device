#ifndef WORKER_POOL_H
#define WORKER_POOL_H

#include <thread>
#include <queue>
#include <condition_variable>
#include <mutex>
#include <vector>
#include <string>

/**
 * @brief thread-safe하도록 구현된 Queue
 *
 */
template <typename DATA>
class SafeQueue {
 public:
  SafeQueue() {
  }

  ~SafeQueue() {
  }

  void push(DATA const& data) {
    std::unique_lock<std::mutex> lock(lock_);
    queue_.push(data);
    lock.unlock();
    cond_.notify_one();
  }

  bool empty() {
    std::unique_lock<std::mutex> lock(lock_);
    return queue_.empty();
  }

  void clear() {
    std::unique_lock<std::mutex> lock(lock_);
    std::swap(queue_, empty_queue_);
  }

  bool try_pop(DATA& popped_value) {
    std::unique_lock<std::mutex> lock(lock_);
    if (queue_.empty()) {
      return false;
    }

    popped_value = queue_.front();
    queue_.pop();
    return true;
  }

  void wait_and_pop(DATA &popped_value) {
    std::unique_lock<std::mutex> lock(lock_);
    while (queue_.empty()) {
      cond_.wait(lock);
    }

    popped_value = queue_.front();
    queue_.pop();
  }

 private:
  std::queue<DATA> queue_;
  std::queue<DATA> empty_queue_;
  std::condition_variable cond_;
  std::mutex lock_;
};

struct Task;

class TaskHandler {
 public:
  TaskHandler() {}
  virtual ~TaskHandler() {}

  // virtual void handle_task(int index, int event, std::string data) {}
  virtual void handle_task(int index, Task *task) {}
};

struct Task {
  TaskHandler *handler;
  int data_type;
  std::string data;
  void *user_data;
};

// typedef SafeQueue<Task *> TaskQueue;
// using TaskQueue = SafeQueue<Task *>;

// class WorkerThread {
//  public:
//   WorkerThread();
//   ~WorkerThread();

//   void Run();

//   int Index;

//  private:
//   std::thread thrd_;
//   SafeQueue<Task> queue_;
// };

// struct VoiceChannel {
//   void Process(std::string) {}
//   // std::string key;
// };

// struct VoiceSession {
//   void Regist(std::string key) {}
//   void Deregist(std::string key) {}
//   void Stop() {}

//   std::string key;
//   std::string channel_session_key;
//   std::vector<VoiceChannel> channels;
//   bool is_stopped;
// };

class WorkerPool {
 public:
  WorkerPool();
  ~WorkerPool();

  void Start(int thread_num);
  void Enqueue(int index, Task *task);
  int  GetWorkerNumber();

 private:
  void Process(int index);

  std::vector<std::thread> worker_list_;
  int worker_num_;
  int worker_seq_;
  SafeQueue<Task *> *worker_q_;

  bool is_done_;
};

#endif /* WORKER_POOL_H */
