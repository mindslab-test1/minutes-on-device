#include "rec-event-receiver.h"
#include "stt-client.h"
#include "zhelpers.h"
#include "rapidjson/document.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/writer.h"
#include "rapidjson/prettywriter.h"

#include <arpa/inet.h>

using namespace rapidjson;

#if 0
class EventTask : public Task {
 public:
  EventTask() {
  }
  virtual ~EventTask() {
    g_var.console->debug("dtor EventTask");
  }

  rapidjson::Document document;
};
#endif

// record event receiver
RecEventReceiver::RecEventReceiver(RecSessionManager *manager) {
  session_manager_ = manager;
}

RecEventReceiver::~RecEventReceiver() {
}

void RecEventReceiver::Start() {
  thrd_ = std::thread(&RecEventReceiver::run, this);
}

void RecEventReceiver::run() {
  char buf[8192];
  int size;
  int cur_call = 0;

  void *context = zmq_ctx_new();
  void *receiver = zmq_socket(context, ZMQ_PULL);
  zmq_bind(receiver, g_var.event_recv_addr.c_str());

  while (true) 
  {
    size = zmq_recv(receiver, buf, 8192, 0);
    buf[size] = 0x00;
    g_var.console->info("got message: {}", buf);

    int index;
    int event = SessionEvent::EV_UNKNOWN;
    std::string key;
    std::string msg_id="";
    std::string call_id="";
    std::string status="";
    std::unique_ptr<rapidjson::Document> document(new rapidjson::Document);
    document->Parse(buf);

    auto it = document->FindMember("msg_header");
    if (it != document->MemberEnd() && it->value.IsObject()) 
	{
		const Value& msg_header=it->value.GetObject();
		if(msg_header.HasMember("msg_id") && msg_header.HasMember("call_id") && msg_header.HasMember("status"))
		{
			msg_id=msg_header["msg_id"].GetString();
			call_id=msg_header["call_id"].GetString();
			status=msg_header["status"].GetString();
			g_var.console->info("msg_id[{}] / call id[{}] / status[{}]", msg_id, call_id, status);
		}
	}


   	if (msg_id != "EVENT_COLLECTION_PUSH" || call_id=="" || status=="")
	{
		g_var.console->error("recv invalid msg [{}]||[{}]||[{}]", msg_id, call_id, status);
		continue;
	}

	g_var.console->debug("SESS_DEBUG: {} / {}", cur_call, g_var.max_proc_call);

	if (status== "START") 
	{
		if (cur_call >= g_var.max_proc_call) 
		{
			g_var.console->error("SESS_DEBUG: max proc count over");
			continue;
		}

		event = SessionEvent::EV_START;
		auto session = session_manager_->GetSession(call_id, index);
		if (session == NULL) 
		{
			// session is max
			g_var.console->error("session is full");
			continue;
		}
		cur_call++;
		g_var.console->debug("SESS_DEBUG++: {} / {}", cur_call, g_var.max_proc_call);
	}
	else if (status == "STOP") 
	{
		event = SessionEvent::EV_STOP;
		auto session = session_manager_->FindSession(call_id, index);
		if (session == NULL) 
		{
			// session is max
			g_var.console->error("session is none");
			continue;
		}
		cur_call--;
		g_var.console->debug("SESS_DEBUG--: {} / {}", cur_call, g_var.max_proc_call);
	} 
	else 
	{
		// unknown event
		g_var.console->error("unknown event");
		continue;
	}
	Task *task = new Task { this, event, std::string(buf, size) };
	//task->user_data = (void *)document.release();
    document->Parse(buf);
	task->user_data = (void *)document.release();
	//task->user_data = (void *)buf;
	session_manager_->Enqueue(index, task);
  }

  zmq_close(receiver);
  zmq_ctx_destroy(context);
}

void RecEventReceiver::handle_task(int index, Task *task)
{
	if (task->data_type == SessionEvent::EV_START) 
	{  // start
		RecSession *session = session_manager_->GetSession(index);
		g_var.console->info("SESSION START : index = {}", index);
		if (!session->is_stopped) {
			// 같은 Start 이벤트 반복해서 온 경우 (ex:RTP based event)
			g_var.console->debug("RTP based event 2");
			session_manager_->ReleaseSession(index);
			return;
		}
		// do something long work
		std::unique_ptr<rapidjson::Document> document((rapidjson::Document *)task->user_data);
		rapidjson::StringBuffer buffer;
		rapidjson::PrettyWriter<rapidjson::StringBuffer> writer(buffer);
		document->Accept(writer);
		const char* json = buffer.GetString();
		LOGGER()->info("Message is {}", json);
		g_var.console->info("Message is {}", json);

		std::string msg_id="";
		std::string call_id="";
		std::string status="";
		std::string mic_info="";
		std::string customer_num="";
		std::string second_key="";

		/* Added by KHY 2020-01-09 */
		auto it = document->FindMember("msg_header");
		if (it != document->MemberEnd() && it->value.IsObject()) 
		{
			const Value& msg_header=it->value.GetObject();
			if(msg_header.HasMember("msg_id") && msg_header.HasMember("call_id") && msg_header.HasMember("status"))
			{
				msg_id=msg_header["msg_id"].GetString();
				call_id=msg_header["call_id"].GetString();
				status=msg_header["status"].GetString();
				g_var.console->info("msg_id[{}] / call id[{}] / status[{}]", msg_id, call_id, status);
			}
		}
		it = document->FindMember("msg_body");
		if (it != document->MemberEnd() && it->value.IsObject()) 
		{
			const Value& msg_body=it->value.GetObject();
			if(msg_body.HasMember("mic_info") && msg_body.HasMember("customer_number") )
			{
				mic_info=msg_body["mic_info"].GetString();
				second_key = mic_info;

				customer_num=msg_body["customer_number"].GetString();
				g_var.console->info("mic_info[{}] / customer_num[{}]", mic_info, customer_num);
			}
		}
		/* Added end 2020-01-09*/
		#if 1

		g_var.console->info("session start {}", index);
		g_var.console->info("key is {}", call_id);
		g_var.console->debug("inet is {}", second_key);

		if (second_key == "" ) {
			// invalid address
			#if 0
			session->Stop();
			session->key.clear();
			session->second_key = 0;
			#endif
			session_manager_->ReleaseSession(index);
			g_var.console->critical("Invalid AGENT-IP");
			LOGGER()->error("Invalid AGENT-IP");
			g_var.console->info("release session {}", index);
			return;
		}

		g_var.console->info("key         : {}", call_id.c_str());
		g_var.console->info("second_key  : {}", second_key);
		session->key = call_id;
		session->second_key = second_key;
		// 전화는 Tx, Rx 2개의 채널을 기본으로 한다
		//session->channels.emplace_back(new RecChannel);
		//session->channels.emplace_back(new RecChannel);
		session->channels.emplace_back(new RecChannel(call_id, mic_info, customer_num, "A"));
		//session->channels.emplace_back(new RecChannel(call_id, agent_ip, customer_num, "C"));

		#else
		g_var.console->info("session start {}", index);
		g_var.console->info("key is {}", (*document)["CALL-ID"].GetString());
		std::string key = (*document)["CALL-ID"].GetString();
		uint32_t second_key = INADDR_NONE;
		auto it = document->FindMember("AGENT-IP");
		if (it != document->MemberEnd() && it->value.IsString()) {
			//yhlee
			//string dst_ip = it->value.GetString();
			//second_key = inet_addr(dst_ip.c_str());
			//g_var.console->debug("inet is {}", second_key);
			string local_ip = it->value.GetString();
			second_key = inet_addr(local_ip.c_str());
			g_var.console->debug("inet is {}", second_key);
		}

		if (second_key == INADDR_NONE) {
			// invalid address
			session_manager_->ReleaseSession(index);
			LOGGER()->error("Invalid AGENT-IP");
			return;
		}

		g_var.console->info("key         : {}", key.c_str());
		g_var.console->info("second_key  : {}", second_key);
		session->key = key;
		session->second_key = second_key;
		// 전화는 Tx, Rx 2개의 채널을 기본으로 한다
		//session->channels.emplace_back(new RecChannel);
		//session->channels.emplace_back(new RecChannel);
		session->channels.emplace_back(new RecChannel(key, (*document)["AGENT-IP"].GetString(), (*document)["CUSTOMER-NUMBER"].GetString(), "A"));
		session->channels.emplace_back(new RecChannel(key, (*document)["AGENT-IP"].GetString(), (*document)["CUSTOMER-NUMBER"].GetString(), "C"));
		#endif

		session_manager_->SetSession(call_id, second_key, index);
		gettimeofday(&session->LastEventTime, NULL);
		session->is_stopped = false;

	}
	else if (task->data_type == SessionEvent::EV_STOP) 
	{  // stop
		g_var.console->debug("receive stop event {}", index);
		RecSession *session = session_manager_->GetSession(index);
		session->Stop();
		session_manager_->ReleaseSession(index);
		session->key.clear();
		session->second_key = "";
		g_var.console->info("release session {}", index);
	}
	else if (task->data_type == SessionEvent::IS_EXPIRED) 
	{  // timeout
		g_var.console->info("session expired {}", index);
		RecSession *session = session_manager_->GetSession(index);
		//g_var.console->info("release session cid:{}", session->channels.unique_id_);
		session->Stop();
		session_manager_->ReleaseSession(index);
		session->key.clear();
		session->second_key = "";
		g_var.console->info("release session {}", index);
	}
}

void RecEventReceiver::StartCall(char* msg, int len) {
#if 0
  int index;
  PDS_REC1 *rec1 = (PDS_REC1 *)msg;
  std::string session_key(rec1->Common.KEY, 14);
  g_var.console->debug("key is {}", session_key);

  auto session = session_manager_->GetSession(session_key, index);
  if (session == NULL) {
    // session is max
    g_var.console->error("session is full");
  } else {
    if (session->is_stopped) {
      session->is_stopped = false;
    } else {
      // previouse session exist
      // update session?
    }

    session->is_stopped = false;
    // session->channel[0].stt = StartSTT(session_key, id);
    // session->channel[1].stt = StartSTT(session_key, id);
    session->key = session_key;
    session->second_key = session_key;
    // 전화는 Tx, Rx 2개의 채널을 기본으로 한다
    session->channels.emplace_back(new PdsChannel);
    session->channels.emplace_back(new PdsChannel);

    session_manager_->SetSession(session_key, session_key, index);

    Task *task = new Task { this, SessionEvent::EV_START, std::string(msg, len) };
    session_manager_->Enqueue(index, task);
  }
#endif
}

void RecEventReceiver::StopCall(char* msg, int len) {
#if 0
  int index;
  PDS_REC3* rec3 = (PDS_REC3*)msg;
  std::string key(rec3->Common.KEY, 14);
  auto session = session_manager_->FindSession(key, index /* output */);

  if (session == NULL) {
    // there is no session
    g_var.console->error("there is no session");
  } else {
    Task *task = new Task { this, SessionEvent::EV_STOP, std::string(msg, len) };
    session_manager_->Enqueue(index, task);
  }

  // g_var.lock.lock();
  // auto it = g_var.call_record_map.find(key);
  // if (it != g_var.call_record_map.end()) {
  //   CALL_RECORD record = it->second;
  //   g_var.call_record_map.erase(rec3->Common.KEY);
  //   timeval tv = {0, 0};
  //   record.SttRx->Stop(tv);
  //   record.SttTx->Stop(tv);
  //   g_var.lock.unlock();
  //   delete record.SttRx;
  //   delete record.SttTx;
  // } else {
  //   g_var.lock.unlock();
  //   LOGGER()->error("Invalid key: {}", key);
  // }
#endif
}

void RecEventReceiver::ProcessCallInfo(char* msg, int len) {
}

