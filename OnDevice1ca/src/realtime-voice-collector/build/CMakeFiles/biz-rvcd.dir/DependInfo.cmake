# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/minds/git/minutes/src/realtime-voice-collector/g711.c" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/g711.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/srv/maum/include"
  "../rvc-session"
  "../"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/minds/git/minutes/src/realtime-voice-collector/app-variable.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/app-variable.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/main.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/main.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/pcap-collector.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/pcap-collector.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/proto-rtp.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/proto-rtp.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/rec-event-receiver.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/rec-event-receiver.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/rec-session.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/rec-session.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/rvc-session/worker-pool.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/rvc-session/worker-pool.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/stt-client.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/stt-client.cc.o"
  "/home/minds/git/minutes/src/realtime-voice-collector/udp-packet.cc" "/home/minds/git/minutes/src/realtime-voice-collector/build/CMakeFiles/biz-rvcd.dir/udp-packet.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/srv/maum/include"
  "../rvc-session"
  "../"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
