#svctl stop lidd
#cp lid.py /srv/maum/bin/lidd
#sleep 1
#svctl start lidd
DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop minutes-information-distributor
cp ${DIR}/midd.py $HOME/MP/bin/midd
sleep 1
svctl start minutes-information-distributor
