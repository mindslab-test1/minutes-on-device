#! /usr/local/bin/python
# -*- coding: utf-8 -*-

'''
[프로그램 설명]
1. 회의록 시스템 main job 수행. 
2. MECD로 부터 메시지 수신
3. 화자분리
4. STT 분석
4. MIPD로 메시지 전달.
'''

#####################################################################################
# PYTHON COMMON
#####################################################################################
import os, sys, time, traceback
import wave, contextlib
import datetime
import json
import multiprocessing

is_py2 = sys.version[0] == '2'
if is_py2 :
	import Queue
	import ConfigParser
else :
	import queue as Queue
	import configparser as ConfigParser


#####################################################################################
# MINDSLAB COMMON
#####################################################################################
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
import common.stt_client as STT
import common.pcm_control as pc
from common.dblib import DBLib
from common.def_val import *



#####################################################################################
# LOCAL CLASS
#####################################################################################
class UserError(Exception) :
	def __init(self) :
		super().__init('SYSTEM FAILURE')


#####################################################################################
# CONSTANT VALUE
#####################################################################################
#환경변수
g_var = {}
g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'

#def load_config():
#	global g_var
#	global g_conf_path
#
#	# COMMON
#	g_var['proc_name'] = os.path.basename(sys.argv[0])
#	proc_name=g_var['proc_name'].upper()
#	g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
#	conf=ConfigParser.ConfigParser()
#	conf.read(g_conf_path)
#	#sections=conf.sections()
#	items=conf.items(proc_name)
#	for name, value in items :
#		g_var[name]=value
#	#g_log_level.value=g_var['log_level']
#
#	# RECORD
#	for i in g_var :
#		print( '{} = {}'.format(i,g_var[i]))
#
#	return


def conver_wav_to_mp3(input_file) :
	try :
		output_file=input_file[:-4] + '.mp3'
		log.critical("[{}]!!!!!".format(output_file))
		#DST_FILE_PATH=os.path.join(input_path, output_name)
		command="echo 'Y' | ffmpeg -i " + input_file + ' -acodec libmp3lame -ab 64k -vn -ac 1 ' + output_file + ' > /dev/null  2>&1'
		#command="echo 'Y' | ffmpeg -i " + input_file + ' -acodec libmp3lame -ab 64k -vn -ac 1 ' + output_file 

		process = os.popen(command)
		result=process.read()
		return output_file

	except Exception as e:
		log.critical("conv MP3 Failure [{}]".format(e))
		return None

def convert_to_wav(mysql, input_file, sr, team_ser) :
	try :
		input_name=input_file.split('/')[-1]
		output_name=input_name.split('.')[0] + '_conv.wav'

		result, rowcnt, rows, error = SQL.select_minutes_common(mysql)
		if result == False :
			return False, ''
		for row in rows :
			result_path=row['RESULT_DIR'] +'/'+ str(team_ser) + '/' + datetime.datetime.today().strftime('%Y%m%d')
			if not os.path.exists(result_path):
				os.makedirs(result_path)

			DST_FILE_PATH=os.path.join(result_path, output_name)

		command="echo 'Y' | ffmpeg -i " + input_file + ' -vn -ac 1 -ar ' + sr + ' ' + DST_FILE_PATH + ' > /dev/null  2>&1'
		process = os.popen(command)
		result=process.read()

	except Exception as e:
		log.critical("thread: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, ''

	return True, DST_FILE_PATH 

def make_publisher_start(stt_meta_ser, container=None) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#START#'
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['SPEAKER']='None'
	data['msg_body']['CONTAINER']=container

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_end(stt_meta_ser, container=None) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#END#'
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['SPEAKER']='None'
	data['msg_body']['CONTAINER']=container

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_error(stt_meta_ser, container=None) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#ERROR#'
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['SPEAKER']='None'
	data['msg_body']['CONTAINER']=container

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')

def make_publisher_data(stt_meta_ser, stt_result, container=None) :
	data={}
	data['msg_header']={}
	data['msg_header']['msg_id']='EVENT_COLLECTION_PUBLISH'
	data['msg_body']={}
	data['msg_body']['TYPE']='#DATA#'
	data['msg_body']['CALL_ID']=str(stt_meta_ser)
	data['msg_body']['SPEAKER']='None'
	data['msg_body']['STT_RESULT']=stt_result
	data['msg_body']['CONTAINER']=container

	json_msg=json.dumps(data,ensure_ascii=False)
	return json_msg.encode('utf-8')


def send_result_mipd(ipc, json_msg) :

	#send result text to MIPd
	try :
		#log.info(json_msg)
		ipc.IPC_Send('mipd', json_msg)

	except Exception as e:
		log.critical("thread: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False

	return True

	
def make_stt_list(mysql) :

	stt_list=[]

	try :
		result, rowcnt, rows, error=SQL.select_stt_model_all(mysql)
		if result == False :
			log.critical("select_stt_model_all Failure[{}]".format(error))
			return False, None

		for row in rows :
			stt_data={}
			stt_data['stt_model_ser']=row['STT_MODEL_SER']
			stt_data['stt_ip']=row['STT_SERVER_IP']
			stt_data['stt_port']=row['STT_SERVER_PORT']
			stt_data['stt_model']=row['STT_MODEL_NAME']
			stt_data['stt_lang']=row['STT_MODEL_LANG']
			stt_data['stt_rate']=row['STT_MODEL_RATE']
			#stt_data['stt_type']=row['DEEP_LEARNING_TYPE']
			if row['DEEP_LEARNING_TYPE'].upper() == 'LSTM' :
				stt_data['stt_type']=STT.STT_TYPE_LSTM
			elif row['DEEP_LEARNING_TYPE'].upper() == 'DNN':
				stt_data['stt_type']=STT.STT_TYPE_DNN
			elif row['DEEP_LEARNING_TYPE'].upper() == 'CNN':
				stt_data['stt_type']=STT.STT_TYPE_CNN
			else :
				log.critical("Unkown DEEP_LEARING_TYPE[{}] - Can't Use Thist Model". format(row['DEEP_LEARNING_TYPE'].upper()))
				continue

			stt_list.append(stt_data)

	except Exception as e:
		log.critical(e)
		log.critical(traceback.format_exc())
		return False, stt_list

	return True, stt_list

def stt_client_connect(stt_data) :
	remote_addr  = '{}:{}'.format(stt_data['stt_ip'], stt_data['stt_port'])
	lang         = stt_data['stt_lang'].lower()
	model        = stt_data['stt_model']
	samplerate   = stt_data['stt_rate']
	stt_type	 = stt_data['stt_type']

	try :
		stt_client=STT.SttClient(remote_addr=remote_addr, stt_type=stt_type, lang=lang, model=model, samplerate=samplerate)
	except Exception as e:
		log.critical(traceback.format_exc())
		return None

	return stt_client


def run_worker(worker_q, idx):

	# MySql Class 생성
	mysql = DBLib(log)
	# MySql DB 접속
	db_conn = mysql.connect()

	# IPC Connection
	ipc=IPC.MindsIPCs(log, g_var['proc_name'])

	# 해당 Process 는 Recv 없이 Send 만 하므로 Not Open
	#ret=ipc.IPC_Open(idx)
	#if ret == False :
	#	log.critical("[THREAD:%d] IPC Open Failure.... " %(idx))

	ret=ipc.IPC_Regi_Process('mipd')
	if ret == False :
		log.critical("IPC_REGI_PROCESS FAIL")

	log.critical("[THREAD:{}] STARTING.... " .format(idx))

	while True:
		try:

			# 로그 레벨 변경 체크
			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			# 최대 1초 blocking
			recv_buf=worker_q.get(True, 1)
			result, msg_id, msg_body = IPC.get_ipc_msg(recv_buf)

			recv_stt_meta=msg_body['STT_META']
			if 'CONTAINER' in msg_body :
				container = msg_body['CONTAINER']
			else :
				container = None

			log.info("[DEQ] %s" %(recv_buf))

			#stt_meta_ser=recv_stt_meta['STT_META_SER']
			stt_meta_ser=msg_body['CALL_ID']
			#stt_meta_ser=recv_stt_meta['call_id']

			log.info("[THREAD{}] START META[{}], " .format(idx, stt_meta_ser))
			#json_msg=make_publisher_start(stt_meta_ser, container)
			#send_result_mipd(ipc, json_msg)
			send_msg=IPC.MAKE_Event_Collection_Publish(stt_meta_ser, '#START#', 'None', None, None, None, None, container)
			json_msg=json.dumps(send_msg, ensure_ascii=False)
			send_result_mipd(ipc, json_msg.encode('utf-8'))


			#STT_MODEL 찾기
			#stt=find_stt_model(mysql, recv_stt_meta['MINUTES_TEAM_SER'], recv_stt_meta['MINUTES_LANG'])
			result, stt_list = make_stt_list(mysql)
			if stt_list :
				stt=stt_list[0]
			else :
				log.critical("make_stt_list Failure")
				#json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				#send_result_mipd(ipc, json_msg)
				#continue
				raise UserError

			#file_converting 
			result, dst_file_path=convert_to_wav(mysql, recv_stt_meta['SRC_FILE_PATH'], stt['stt_rate'], recv_stt_meta['MINUTES_TEAM_SER'])
			if result == False :
				# file convert failure
				log.critical("File converting Failure [META:{}]/[FILE:{}]!!!" .format(recv_stt_meta['STT_META_SER'], recv_stt_meta['SRC_FILE_PATH']))
				#json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				#send_result_mipd(ipc, json_msg)
				#continue
				raise UserError


			#화자 분석
			#log.info("[do_diarization] %s" %(stt_wav))
			#try :
			#	#DIAR=STT.do_diarization(stt_wav, '127.0.0.1:16002')
			#	DIAR=STT.do_diarization(stt_wav, '10.122.64.90:16002')
			#except Exception as e:
			#	DIAR=list()
			#	log.critical("[do_diarization] FAILURE [{}]" .format(e))

			# STT 결과 및 화자분리 결과 통합
			log.info("[do_STT] %s" %(dst_file_path))
			#stt_result=stt['stt_client'].detail_recognize_with_diarize(stt_wav, None)
			stt_client=stt_client_connect(stt)
			if stt_client == None:
				log.critical("STT CLIENT CONNECTION FAILURE[META:{}]]!!!" .format(recv_stt_meta['STT_META_SER']))
				#json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
				#send_result_mipd(ipc, json_msg)
				#continue
				raise UserError
			else :
				log.info("[Client Connect Success]")
				stt_result=stt_client.detail_recognize(dst_file_path)
				if len(stt_result) == 0 :
					log.critical("Invalid STT_RESULT")
				stt_client.close()
				if g_var['merge_result'].upper() == 'TRUE' :
					stt_result=STT.merge_stt_result(stt_result)
			# DIAR 삭제
			#for result in stt_result :
			#	result['diar'] ='None'

			#make_result_txt(stt_wav, stt_result)
			
			#Send MIPd (STT, DIAR Result)
			for result in stt_result:
				conv_result=dict()
				start=result['start']
				end=result['end']

				stt_base_time = datetime.datetime.strptime(recv_stt_meta['START_TIME'], "%Y-%m-%d %H:%M:%S")
				start_date=stt_base_time + datetime.timedelta(milliseconds=int(start)*10)
				end_date=stt_base_time + datetime.timedelta(milliseconds=int(end)*10)

				#start=sess_start_time + float(result['start'])/100
				#end=sess_start_time + float(result['end'])/100

				#start_date=datetime.datetime.fromtimestamp(start)
				#end_date=datetime.datetime.fromtimestamp(end)
				str_start=start_date.strftime("%Y-%m-%d %H:%M:%S")
				str_end=end_date.strftime("%Y-%m-%d %H:%M:%S")

				if is_py2 :
					res_buf="[{}] : {} ~ {} || {}\n" .format(stt_meta_ser, str_start, str_end, result['txt'].encode('utf-8'))
				else :
					res_buf="[{}] : {} ~ {} || {}\n" .format(stt_meta_ser, str_start, str_end, result['txt'])
				log.info("{}".format(res_buf))


				conv_result['start']=str_start
				conv_result['end']=str_end
				conv_result['start_f']=start
				conv_result['end_f']=end
				conv_result['txt']=result['txt']

				#json_msg=make_publisher_data(stt_meta_ser, conv_result, container)
				#ret = send_result_mipd(ipc, json_msg)

				send_msg=IPC.MAKE_Event_Collection_Publish(stt_meta_ser, '#DATA#', 'None', None, None, None, conv_result, container)
				json_msg=json.dumps(send_msg, ensure_ascii=False)
				send_result_mipd(ipc, json_msg.encode('utf-8'))
				if ret == False :
					log.critical('Send MIPd or Record Result Failure, [{}]',recv_stt_meta['SRC_FILE_PATH'])

				time.sleep(0.01)

			#json_msg=make_publisher_end(stt_meta_ser, container)
			#send_result_mipd(ipc, json_msg)

		#	send_msg=IPC.MAKE_Event_Collection_Publish(stt_meta_ser, '#END#', 'None', None, container)
			send_msg=IPC.MAKE_Event_Collection_Publish(stt_meta_ser, '#END#', 'None', None, None, None, None, container)
			json_msg=json.dumps(send_msg, ensure_ascii=False)
			send_result_mipd(ipc, json_msg.encode('utf-8'))

			#UPDATE STT_META Table
			rec_time=pc.calculate_play_time(dst_file_path)
			mp3_dst_file_path=conver_wav_to_mp3(dst_file_path)
			if mp3_dst_file_path :
				log.critical("Conv MP3 [{}] to [{}]".format(dst_file_path, mp3_dst_file_path))
				#Need MP3 and WAV (Chrom use wav, ie use mp3),(Not Delete Wav)
				#os.remove(dst_file_path)
			else :
				log.critical("NOTTTTTT Conv MP3")
				mp3_dst_file_path=dst_file_path

			#ret, error=SQL.update_stt_meta_complete(mysql, stt_meta_ser, rec_time, mp3_dst_file_path, SQL.META_STAT_COMPLETE)
			ret, error=SQL.update_stt_meta_complete(mysql, stt_meta_ser, rec_time, dst_file_path, SQL.META_STAT_COMPLETE)
			if ret == False :
				log.critical('META_STATUS Update Error FAIL [META:{}], [{}]'.format(stt_meta_ser, error))
			else :
				log.critical("[COMPLETE] META:{}, START:{}, USER:{}, REC_TIME:{}" .format(stt_meta_ser, recv_stt_meta['START_TIME'], recv_stt_meta['CREATE_USER'], rec_time))


			#try :
			#	for result in stt_result:
			#		json_msg=make_publisher_data(stt_meta_ser, result)
			#		ret = send_result_mipd(ipc, json_msg)
			#		if ret == False :
			#			log.critical('Send MIPd or Record Result Failure, [{}]',recv_stt_meta['SRC_FILE_PATH'])

			#	json_msg=make_publisher_end(stt_meta_ser, stt_wav)
			#	send_result_mipd(ipc, json_msg)

			#except Exception as e:
			#	log.critical("thread: exception raise fail. <%s>", e)
			#	log.critical("SEND Publiser to ERROR[{}]".format(recv_stt_meta))
			#	log.critical(traceback.format_exc())
			#	json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'])
			#	send_result_mipd(ipc, json_msg)

		except Queue.Empty:
			continue

		except Exception as e:
			log.critical("thread: exception raise fail. <%s>", e)
			log.critical("SEND Publiser to ERROR[{}]".format(recv_stt_meta))
			log.critical(traceback.format_exc())

			json_msg=make_publisher_error(recv_stt_meta['STT_META_SER'], container)
			send_result_mipd(ipc, json_msg)

			ret, error=SQL.update_stt_meta_complete(mysql, stt_meta_ser, 0, '', SQL.META_STAT_GENERAL_ERROR)
			if ret == False :
				log.critical('META_STATUS Update Error FAIL [META:{}], [{}]'.format(stt_meta_ser, error))
			else :
				log.critical("[COMPLETE] META:{}, START:{}, USER:{}, REC_TIME:{}" .format(stt_meta_ser, recv_stt_meta['START_TIME'], recv_stt_meta['CREATE_USER'], rec_time))

	# Terminate Process
	log.critical("[THREAD:%d] STOPPING.... " %(idx))
	return


def init_process():
	global log
	global g_var

	try:
		# Init Config
		try:
			proc_name = os.path.basename(sys.argv[0])
			g_var=load_config(proc_name, g_conf_path)
		except Exception as e:
			print("main: exception raise fail. <%s>", e)
			print(traceback.format_exc())
			return False, None, None, None

		# Init Logger
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', proc_name, g_var['log_level'], True)
		log.info('Load Config Success')

		# Init Signal
		#signal.signal(signal.SIGINT, sig_int_handler)

		# zmq consumer Init
		#zmq_pull = ZmqPipline(log)
		#zmq_pull.bind(g_var['my_zmq_port'])

		ipc=IPC.MindsIPCs(log, g_var['proc_name'])
		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			return False, None, None, None


		worker_list = list()
		worker_cnt = int(g_var['thr_cnt'])

		# MultiProcessing을 사용하여 Multi-Thread와 같은 구조로 동작
		worker_q = multiprocessing.Queue()
		for idx in range(worker_cnt):
			p = multiprocessing.Process(target=run_worker, args=(worker_q, idx))
			p.daemon = True
			p.start()
			worker_list.append(p)
		log.info('Worker Pool Start')

	except Exception as e:
		log.critical("init: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False, None, None, None
	else:
		return True, worker_q, worker_list, ipc


def main():
	# Init Process
	result, worker_q, worker_list, ipc = init_process()
	if result == False :
		log.critical("Process init Failure")
		sys.exit(1)

	# Start MainLoop
	while True:
		try:
			recv_msg = ipc.IPC_Recv()
			if recv_msg :
				json_msg = json.loads(recv_msg)
				result, msg_id, msg_body = IPC.get_ipc_msg(json_msg)
				if result == False :
					log.critical("main: Invalid JSON MSG recved [{}]".format(json_msg))
					continue
			else :
				time.sleep(0.1)
				continue

			if msg_id == IPC.EVENT_COLLECTION_PUSH :
				#log.info("[ENQ] %s" %(json_msg['msg_body']))
				worker_q.put(json_msg)

			elif msg_id == 'CHG_LOG_LEVEL' :
				log.critical("main: CHG_LOG_LEVEL [{}]".format(json_msg))
				conf=ConfigParser.ConfigParser()
				conf.read(g_conf_path)
				g_var['log_level'] = get_log_level(g_var['proc_name'], conf_path)
				logger.change_logger_level(log, g_var['log_level'])

			#	items=conf.items(proc_name)
			#	for name, value in items :
			#		if name == 'log_level' :
			#			g_var['log_level']=value
			#	logger.change_logger_level(log, g_var['log_level'])

			else :
				log.critical("main: Unknown JSON MSG recved [{}]".format(json_msg))

		except Exception as e:
			log.critical("main: exception raise fail. <%s>", e)
			log.critical(traceback.format_exc())

	# Terminate Process
	ipc.IPC_Close()

	for p in worker_list:
		p.join()

	log.critical('*%30s : %s -> %d threads', "STOPPED", g_var['proc_name'], g_var['thr_cnt'])
	return 0

# main 함수 시작
if __name__ == "__main__":
    main()

