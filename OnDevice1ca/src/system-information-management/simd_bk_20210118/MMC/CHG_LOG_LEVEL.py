#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import re
import json
import common.logger as logger
import libmmc as MMCLib
import traceback
#import ConfigParser
import configparser as ConfigParser

from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

CHG_LOG_LEVEL_MIN_PARAMETER = 2
CHG_LOG_LEVEL_MAX_PARAMETER = 2

param_list = ['PROCESS_NAME', 'LOG_LEVEL']
mandatory_list = ['PROCESS_NAME', 'LOG_LEVEL']
level_list = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']


def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-LOG-LEVEL [PROCESS_NAME=a] [LOG_LEVEL=b]

 [Parameter]
  a = STRING 	(1:100)
  b = ENUM      (DEBUG|INFO|WARNING|ERROR|CRITICAL)

 [Usage]
  CHG-LOG-LEVEL [a, b]
   ex) CHG-LOG-LEVEL SIMD, DEBUG
  CHG-LOG-LEVEL [PROCESS_NAME=a], [LOG_LEVEL=b]
   ex) CHG-LOG-LEVEL PROCESS_NAME=SIMD, LOG_LEVEL=DEBUG
 
 [Config_File Organization]
  [Section]
   MECD       : minutes-event-collector
   MCCD       : minutes-center-control
   MIPD       : minutes-information-publisher 
   MIDD       : minutes-information-distributor
   SIMD       : system-information-management 
   CNN_SERVER : cnn-server
  [Option]
   LOG_LEVEL  : LOG_LEVEL

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-LOG-LEVEL
  Result = SUCCESS
  ============================================
        PROCESS       |       LOG LEVEL
  --------------------------------------------
      Process Name    |    before -> after
  ============================================


  <FAILURE>
  Date time
  MMC = CHG-PROC-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list) :

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)
		
	##### Mandatory Parameter #####
	if arg_data['LOG_LEVEL'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'PROCESS_NAME', arg_data['PROCESS_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['LOG_LEVEL'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'LOG_LEVEL', arg_data['LOG_LEVEL'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Enum(level_list, 'LOG_LEVEL', arg_data['LOG_LEVEL'].upper(), G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, ipc):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = "SUCCESS"
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
	
		else :
		    # load simd config file (/srv/maum/etc/process_info.conf)
			proc_config = ConfigParser.RawConfigParser() 
			proc_config.read(G_proc_cfg_path)	
			section_list = proc_config.sections()

			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, CHG_LOG_LEVEL_MAX_PARAMETER, CHG_LOG_LEVEL_MIN_PARAMETER)
			if (ret == False):
				result = 'FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			# Check section name
			if Parsing_Dict['PROCESS_NAME'].upper() not in section_list :
				result = 'FAILURE'
				reason = 'Section Name [{}] is not exist.'.format(Parsing_Dict['PROCESS_NAME'].upper())
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)

			data = OrderedDict()		
			org_item_value = proc_config.items(Parsing_Dict['PROCESS_NAME'].upper())
			for i in range(len(org_item_value)):
				data[org_item_value[i][0]] = org_item_value[i][1]
			G_log.info('conf_data = {}'.format(data))

			# change config file data
			G_log.debug(Parsing_Dict['PROCESS_NAME'])
			G_log.debug(Parsing_Dict['LOG_LEVEL'])
			if Parsing_Dict['LOG_LEVEL'] is not None :
				value = proc_config.set(Parsing_Dict['PROCESS_NAME'].upper(), 'log_level', Parsing_Dict['LOG_LEVEL'].lower())
				if value == False:
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARV, result, 'Change Config File Failure [log_level]')
				else:
					with open(G_proc_cfg_path, 'w') as configfile :
						proc_config.write(configfile)

			# make response message
			total_body=""" {:^20} | {:^20} \n""".format('PROCESS', 'LOG LEVEL')
			total_body = total_body + '--------------------------------------------'
			total_body = total_body + '\n {:^20} | {:^8} -> {:^8}'.format(Parsing_Dict['PROCESS_NAME'].upper(), data['log_level'], Parsing_Dict['LOG_LEVEL'])
			G_log.info('CHG-LOG-LEVEL Complete!!')
			
			##### ipc 
			ipc_msg = {}
			ipc_msg['msg_header'] = {}
			ipc_msg['msg_header']['msg_id'] = "CHG_LOG_LEVEL"
			ipc_msg['msg_body'] = "" 
			
			for proc in section_list :
				if proc is Parsing_Dict['PROCESS_NAME'].upper():
					ipc.IPC_Send(proc, json.dumps(ipc_msg))	
					G_log.info("IPC Message Send to [{}]".format(proc)) 

			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)
	
	except ConfigParser.NoSectionError as e :
		G_log.error("{}, NoSectionError : [{}]".format(MMC,e))
		reason='{}, NoSectionError'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('{}, ERROR Occured [{}]'.format(MMC,e))
		G_log.error(traceback.format_exc())
		reason='Config_File read error [{}]'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('{}, ERROR Occured [{}]'.format(MMC,e))
		G_log.error(traceback.format_exc())
		reason='[{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


