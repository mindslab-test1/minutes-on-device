#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import libmmc as MMCLib
#import ConfigParser
import configparser as ConfigParser

import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DIS_RM_THR_MIN_PARAMETER = 0
DIS_RM_THR_MAX_PARAMETER = 0

param_list = {}
mandatory_list = {}

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DIS-RM_THR

 [Parameter]
  N/A

 [Usage]
  DIS-RM-THR

 [Options Configuration]
  SYS_RSC_THR_SER    = Serial Number
  SYSTEM_NAME        = Server information 
  SORTATION          = equipment separator 
  THRESHOLD_MINOR    = Caution steps
  THRESHOLD_MAJOR    = Warning steps
  THRESHOLD_CRITICAL = Critical steps
 	
 [Result]
 <SUCCESS>
 Date time
 MMC    = DIS-RM-THR
 Result = SUCCESS
 ==========================================================================
  SER |       SYS_NAME        |    SORTATION    | MINOR | MAJOR | CRITICAL
 --------------------------------------------------------------------------
    1 | CLOUD_DEV             | CPU             |    70 |    80 |       90
 ==========================================================================

 
 <FAILURE>
 Date time
 MMC    = DIS-RM-THR
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return make_result(MMC, ARG, result, reason, '')
		
			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = Argument_Parsing(ARG, param_list, DIS_RM_THR_MAX_PARAMETER, DIS_RM_THR_MIN_PARAMETER)
			if (ret == False) :
				result = 'FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARV, result, reason, '')
	
			try:
				db_data = DIS_Query(mysql, "SYSTEM_RESOURCE_THRESHOLD", "*", ';')
				G_log.info("DB_data = {}".format(db_data))
				if not db_data :
					result = 'FAILURE'
					reason = "DB_DATA DOES NOT EXIST"
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)
				else :
					total_body=" {:^3} | {:^21} | {:^15} | {:^5} | {:^5} | {:^8}\n".format("SER", "SYS_NAME", "SORTATION", "MINOR", "MAJOR", "CRITICAL")
					total_body = total_body + ('-' * MMC_SEND_LINE_RM_THR_SUCCESS_CNT)
					for thr_num in range(len(db_data)) :
						thr_ser = db_data[thr_num]["SYS_RSC_THR_SER"]
						sys_nm = db_data[thr_num]["SYSTEM_NAME"]
						sortation = db_data[thr_num]["SORTATION"]
						minor = db_data[thr_num]["THRESHOLD_MINOR"]
						major = db_data[thr_num]["THRESHOLD_MAJOR"]
						critical = db_data[thr_num]["THRESHOLD_CRITICAL"]
						row_buf = "\n {:3} | {:21} | {:15} | {:5} | {:5} | {:8}".format(thr_ser, sys_nm, sortation, minor, major, critical)
						total_body = total_body + row_buf
					
					return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DIS-RM-THR(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB SELECT FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-RM-THR(), NoSectionError : [{}]".format(e))
		reason='DIS-RM-THR(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-RM-THR(), Config read error: [{}]".format(e))
		reason='DIS-RM-THR(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason = 'DIS-RM_THR(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS-RM-THR(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, result, reason, total_body)


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {}".format(column, table)
	G_log.debug("query = {}".format(DIS_All_Query))
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt))
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

