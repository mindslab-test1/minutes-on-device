#! /usr/bin/python
#-*- coding:utf-8 -*-
#######################################################################
# PYTHON COMMON MODULE
#######################################################################
import os, sys, io
import json
import socket
import uuid
import time
import traceback
from multiprocessing import Process

#######################################################################
# INSTALL MODULE
#######################################################################
#import ConfigParser
import configparser as ConfigParser
import tornado.web
import tornado.websocket
import websocket
from tornado.websocket import websocket_connect

#######################################################################
# COMMON LIBRARY
#######################################################################
import common.logger as logger
import common.resource_monitor as monitor
from common.dblib import DBLib

def resource_monitor(res_monitor, log, wsc_loc, proc_name, sys_name, root_path):
	### resource_monitor log 객체 생성, log를 따로 생성함
	res_config = ConfigParser.RawConfigParser()
	res_config.read(root_path + '/etc/simd.conf')
	res_log = logger.create_logger(root_path + '/logs', 'resource_monitor', 'debug', False)
	
	db_use_flag = res_config.get('SYS', 'db_use_flag').lower()
	if (db_use_flag == 'on'):
		mysql = DBLib(res_log)
		db_conn = mysql.connect()
	else :
		pass

	before_time = int(time.time())
	before_time2 = time.time()
	off_flag = True
	db_off_flag = True	
	while True :
		try :		
			res_config.read(root_path + '/etc/simd.conf')
			if (res_config.get('HARDWARE_MONITORING', 'use_flag').lower() == 'on'): 
				current_time = int(time.time())
				if (current_time != before_time):
					before_time = int(time.time())
					monitoring_interval = int(res_config.get('HARDWARE_MONITORING', 'monitoring_interval'))
					if ((before_time % monitoring_interval) == 0): 
						cpu_percent = res_monitor.cpu_percent_2()
						mem_total = float(res_monitor.mem_total())
						mem_used = float(res_monitor.mem_used())
						mem_percent = round((mem_used)/(mem_total) * 100,1)
						disk_total = float(res_monitor.disk_total('/'))
						disk_used = float(res_monitor.disk_used('/'))
						disk_percent = round((disk_used/disk_total) * 100,1)
				
						ws_msg = {}
						ws_msg['system'] = sys_name
						ws_msg['cpu'] = {}
						ws_msg['cpu']['percent'] = float(cpu_percent)
						ws_msg['mem'] = {} 
						ws_msg['mem']['total'] = int(mem_total)
						ws_msg['mem']['used'] = int(mem_used)
						ws_msg['disk'] = {}
						ws_msg['disk']['total'] = int(disk_total)
						ws_msg['disk']['used'] = int(disk_used)
						ws_msg['gpu'] = {}
						ws_msg['gpu_mem'] = {}
					
						gpu_list = 'GPU' 
						gpu_mem_list = 'GPU_MEM'
						gpus = res_monitor.gpu_stat3()
					
						if (gpus == None):
							gpu_list = gpu_list + '[{}]'.format(gpus)
							gpu_mem_list = gpu_mem_list + '[{}]'.format(gpus)
							ws_msg['gpu'] = None
							ws_msg['gpu_mem'] = None
						else :	
							for i in range(len(gpus)) :
								gpu_mem_per = round((gpus[i]["gpu_total_used"]/gpus[i]["gpu_total_mem"]) * 100,1)
								gpu_row = '[({}){} %] '.format(i, gpus[i]["gpu_percent"])
								gpu_list = gpu_list + gpu_row 
								gpu_mem_row = '[({}){} %] '.format(i, gpu_mem_per)
								gpu_mem_list = gpu_mem_list + gpu_mem_row
				
								ws_msg['gpu'] = {}
								ws_msg['gpu_mem'] = {}
								ws_msg['gpu'][i] = {}
								ws_msg['gpu'][i]['percent'] = gpus[i]["gpu_percent"]
								ws_msg['gpu_mem'][i] = {}
								ws_msg['gpu_mem'][i]['total'] = int(gpus[i]["gpu_total_mem"])
								ws_msg['gpu_mem'][i]['used'] = int(gpus[i]["gpu_total_used"])
						
						res_log.info('CPU[{} %] MEM[{} %] DISK[{} %] {}{}'.format(cpu_percent, mem_percent, disk_percent, gpu_list, gpu_mem_list))
				
						#if res_config.get('HA', 'use_flag').lower() == 'off' or (res_config.get('HA', 'use_flag').lower() == 'on' and (res_config.get('HA', 'ha_mode').lower() == 'active')):

						if res_config.get('HA', 'use_flag').lower() == 'on' :
							ws_msg['ha_mode'] = res_config.get('HA', 'ha_mode').lower()
						else :
							ws_msg['ha_mode'] = 'active'

						json_ws_msg = json.dumps(ws_msg)	
						wsc_loc.send(json_ws_msg)	

						if (res_config.get("HARDWARE_MONITORING", "db_insert_flag").lower() != 'on') :
							if (db_off_flag == True):
								res_log.info('db_insert_flag is off')
								db_off_flag = False
							else :
								pass
						else :
							db_off_flag = True	
							#current_time2 = time.time()
							insert_interval = int(res_config.get("HARDWARE_MONITORING", "db_insert_interval"))
							#if (current_time2 > before_time2 + insert_interval) :
							if (current_time >= before_time2 + insert_interval) :
								before_time2 = int(time.time())
								insert_query(mysql, sys_name, proc_name, cpu_percent, mem_total, mem_used, disk_total, disk_used, gpus, res_log)
							else :
								pass
					else :
						time.sleep(0.05)
						continue
				else :
					time.sleep(0.05)
					continue
			else :
				if (off_flag == True):
					res_log.info('Resource_Monitor Off')
					off_flag = False
				else :
					pass
				time.sleep(1)
				continue

		except ConfigParser.MissingSectionHeaderError as e :
			log.error("simd.conf read error : [{}]".format(e))
		except Exception as e :
			log.error("resource_monitor error : [{}]".format(e))
			log.error(traceback.format_exc())

def insert_query(mysql, sys_name, proc_name, cpu_percent, mem_total, mem_used, disk_total, disk_used, gpus, res_log):
	try :

		if (gpus == None):
			sql = "insert into SYSTEM_RESOURCE(CREATE_USER, SYSTEM_NAME, CPU, MEM_TOTAL, MEM_USED, DISK_TOTAL, DISK_USED) values('{}', '{}', {}, {}, {}, {}, {});".format(proc_name, sys_name, cpu_percent, int(mem_total), int(mem_used), int(disk_total), int(disk_used))
		elif (len(gpus) == 1):
			sql = "insert into SYSTEM_RESOURCE(CREATE_USER, SYSTEM_NAME, CPU, MEM_TOTAL, MEM_USED, DISK_TOTAL, DISK_USED, GPU, GPU_TOTAL, GPU_USED) values('{}', '{}', {}, {}, {}, {}, {}, {}, {}, {});".format(proc_name, sys_name, cpu_percent, int(mem_total), int(mem_used), int(disk_total), int(disk_used), gpus[0]["gpu_percent"], gpus[0]["gpu_total_mem"], gpus[0]["gpu_total_used"]) 
		else :
			gpu_percent = []
			for i in range(2) :
				gpu_mem_per = round((gpus[i]["gpu_total_used"]/gpus[i]["gpu_total_mem"]) * 100,1)
				gpu_percent.append(gpu_mem_per)
			sql = "insert into SYSTEM_RESOURCE(CREATE_USER, CPU, MEM_TOTAL, MEM_USED, DISK_TOTAL, DISK_USED, GPU, GPU_TOTAL, GPU_USED, GPU2, GPU2_TOTAL, GPU2_USED) values('{}', '{}', {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {});".format(proc_name, sys_name, cpu_percent, mem_total, mem_used, disk_total, disk_used, gpu_percent[0], gpus[0]["gpu_total_mem"], gpus[0]["gpu_total_used"], gpu_percent[1], gpus[1]["gpu_total_mem"], gpus[1]["gpu_total_used"]) 
			
		mysql.execute(sql, True)
		#res_log.info('DB_INSERT')
		return True

	except Exception as e :
		print("DB INSERT ERROR : {}".format(e))
		res_log.error('DB_INSERT ERROR')
		res_log.error(traceback.format_exc())
		return False
		
class WS_Handler(tornado.websocket.WebSocketHandler):
	clients = set()
	def open(self):
		if self not in self.clients:
			self.id = uuid.uuid4()
			self.clients.add(self)
			#log.critical("WS_Handler::open(%s) client connected", self.id)
			print("WS_Handler::open(%s) client connected", self.id)
	
	def on_close(self):
		if self in self.clients:
			self.clients.remove(self)
			#log.critical("WS_Handler::on_close(%s) client removed", self.id)
			print("WS_Handler::on_close(%s) client removed", self.id)
	
	def on_message(self, msg):
		#log.info("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		#print("WS_Handler::on_message(%s)\n => %s", self.id, msg)
		self.SendAll(self.id, msg)
	
	def check_origin(self, origin):
		return True
	
	def SendAll(self, send_id, msg):
		for client in self.clients:
			if not client.ws_connection.stream.socket:
				self.clients.remove(client)
				#log.critical("WS_Handler::SendAll(%s) client removed", self.id)
				print("WS_Handler::SendAll(%s) client removed", self.id)
			else:
				if client.id == send_id:
					continue

				#log.info("[TX] {} => {}" .format(msg, client.id))
				#print("[TX] {} => {}" .format(msg, client.id))
				client.write_message(msg)

class WS_Server(Process):
	def __init__(self, __log, ws_port):
		Process.__init__(self)
		self.log=__log
		self.ws_port=ws_port
	
	def run(self):
		self.log.critical("WS_Server::run() START [port:{}]" .format(self.ws_port))
		try:
			app = tornado.web.Application([(r"/websocket", WS_Handler)])
			app.listen(int(self.ws_port))
			tornado.ioloop.IOLoop.instance().start()
		except Exception as e:
			self.log.critical("WS_Server() Exception => %s", e)
			pass

		self.log.critical("WS_Server::run STOP")
class WS_Client():
	def __init__(self, url, log):
		self.log= log
		self.url = url
		self.wsc = None
	
	def connect(self):
		self.log.critical("WS_Client::connect() => %s", self.url)
		try:
			self.wsc = websocket.WebSocket()
			self.wsc.connect(self.url, setdefaulttimeout=0)
		except socket.error as e:
			if e.errno == 111: # [errno 111] Connection refused
				self.log.critical("WS_Client::connect refused fail [%s]", e)
				pass
			else:
				self.log.critical("WS_Client::connect socket fail [%s]", e)
				self.close()
		except Exception as e:
			self.log.critical("WS_Client::connect fail [%s]", e)
			self.log.critical(traceback.format_exc())
			self.close()

	def send(self, msg):
		if self.wsc is None:
			self.connect()

		if self.wsc:
			try:
				self.wsc.send(msg)
			except Exception as e:
				self.log.critical("WS_Client::connect fail [%s]", e)
				self.close()

	def close(self):
		self.log.critical("WS_Client::close()")
		self.wsc.close()
		self.wsc = None

class WS_Sender():
	def __init__(self, log, proc_name, sys_name, ws_port, root_path):

		## websocket client Init
		self.log=log
		self.proc_name = proc_name
		self.sys_name = sys_name
		self.root_path = root_path
		self.wsc_loc = WS_Client("ws://127.0.0.1:%s/websocket" %(ws_port),log)
		self.res_monitor = monitor.ResourceMonitor()
		## queue Init
		#self.ws_sender_q=ws_sender_q
	
	def run(self):
		self.log.critical("WS_Sender::run()")
		while True:
			try:
				resource_monitor(self.res_monitor, self.log, self.wsc_loc, self.proc_name, self.sys_name, self.root_path)
	
			except KeyboardInterrupt:
				self.log.info("interrupt received, stopping...")
		
			except Exception as e:
				self.log.error("main: exception raise fail. <%s>", e)
				self.log.error(traceback.format_exc())

	def close(self):
		self.log.critical("WS_Sender::close()")
		self.wsc_loc.Close()
		#self.wsc_voice.Close()

def ws_server_proc(log, ws_port):
	wss = WS_Server(log, ws_port)
	wss.start() #Process Spawn: run() method 호출

def ws_client_proc(log, proc_name, sys_name, ws_port, root_path):

	#init , IP port
	ipc_receiver = WS_Sender(log, proc_name, sys_name, ws_port, root_path)
	#ipc_receiver.init()

	#blocking here
	ipc_receiver.run()
	ipc_receiver.close()
	log.critical("Process stopped...")

	return 0

