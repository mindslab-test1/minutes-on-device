#! /usr/bin/python
#-*- coding:utf-8 -*-

import common.logger as logger
import common.dblib as DBLib
import libmmc as MMCLib
import ConfigParser
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from libmmc import *

DIS_PRC_INFO_MIN_PARAMETER = 0
DIS_PRC_INFO_MAX_PARAMETER = 1

param_list = ["PRICING_NAME"]
mandatory_list = {}

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DIS-PRC-INFO (PRICING_NAME=a)
 
 [Parameter]
  a = STRING (1:200)

 [Usage]
  DIS-PRC-INFO 
  DIS-PRC-INFO (PRICING_NAME=a)
   ex) DIS-PRC-INFO PRICING_NAME=MAUM
  
 [Column information]
  PRICING_NAME        : Pricing class name
  PRICING_LEVEL       : Pricing class level
  PERIOD_DAY          : Pricing class period 
  USE_TIME            : Audio file time available for minutes (unit : minute)
  MAU                 : Usage fee
  DESCRIPT            : Explain cost

 [Result]
 <SUCCESS>
 Date time
 MMC    = DIS-PRC-INFO 
 Result = SUCCESS
 ==================================================================================================
         PRICING_NAME        | PRICING_LEVEL | PERIOD_DAY | USE_TIME |   MAU   | DESCRIPT
 --------------------------------------------------------------------------------------------------
   value                     | value         |      value |    value |   value | value
   value                     | value         |      value |    value |   value | value
 ...                             
 ==================================================================================================

 <FAILURE>
 Date time
 MMC    = DIS-PRC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	##### Optional Parameter #####
	if arg_data['PRICING_NAME'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING_LENGTH, 'PRICING_NAME', arg_data['PRICING_NAME'], G_log)
		if ret == False:
			return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DIS_PRC_INFO_MAX_PARAMETER, DIS_PRC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result = 'FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) : 
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			try :
				total_body = ''

				if (ARG_CNT == 0): # select all
					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*", ';')
				else :
					db_data = DIS_Query(mysql, "MINUTES_PRICING", "*", "where PRICING_NAME = '{}'".format(Parsing_Dict["PRICING_NAME"]))
				# Not exist information
				if not db_data :
					result = 'FAILURE'
					reason = "PRICING_NAME [{}] does not exist.".format(Parsing_Dict['PRICING_NAME'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				total_body=""" {:^16} | {:^15} | {:^12} | {:^11} | {:^11} | {}\n""" .format('PRICING_NAME', 'PRICING_LEVEL', 'PERIOD_DAY', 'USE_TIME', 'MAU', 'DESCRIPT')

				total_body= total_body + ('-' * MMC_SEND_LINE_PRC_SUCCESS_CNT)
				for thr_num in range(len(db_data)) :
					pri_ser = db_data[thr_num]["MINUTES_PRICING_SER"]
					create_user = db_data[thr_num]["CREATE_USER"]
					create_time = db_data[thr_num]["CREATE_TIME"]
					update_user = db_data[thr_num]["UPDATE_USER"]
					update_time = db_data[thr_num]["UPDATE_TIME"]
					prc_level = db_data[thr_num]["PRICING_LEVEL"]
					prc_name = db_data[thr_num]["PRICING_NAME"]
					prc_day = db_data[thr_num]["PERIOD_DAY"]
					use_time = db_data[thr_num]["USE_TIME"]
					mau = db_data[thr_num]["MAU"]
					desc = db_data[thr_num]["DESCRIPT"]

					row_buf="\n {:16} | {:15} | {:12} | {:11} | {:11} | {}" .format(prc_name, prc_level, prc_day, use_time, mau, desc)
					total_body = total_body + row_buf 

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DIS-PRC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB Select Failure'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

	except ConfigParser.NoSectionError as e :
		G_log.error("DIS-PRC-INFO(), NoSectionError : [{}]".format(e))
		reason='DIS-PRC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DIS-PRC-INFO(), Config read error: [{}]".format(e))
		reason='DIS-PRC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DIS-PRC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DIS-PRC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DIS-PRC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = "select {} from {} {}".format(column, table, where)
	G_log.debug("query = {}".format(DIS_All_Query))
	try :
		if where[-1] != ';' :
			sql = DIS_All_Query + ';'
		else :
			sql = DIS_All_Query 

		rowcnt, rows = mysql.execute_query2(sql)
		G_log.debug("row cnt is [{}]".format(rowcnt));
		return rows

	except Exception as e :
		G_log.error('DIS_Query error : {}'.format(e))
		G_log.error(traceback.format_exc())
		return ''

