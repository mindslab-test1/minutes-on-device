#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import traceback
import ConfigParser
from datetime import datetime
from collections import OrderedDict
from simd_main import *

CHG_SYS_INFO_MIN_PARAMETER = 1
CHG_SYS_INFO_MAX_PARAMETER = 6

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   CHG-SYS-INFO (sys_name=a), (mmc_port=b), (db_use_flag=c), (upload_dir=d), (result_dir=e), (admin_email=f)

 [Parameter]
   a = STRING 	(1:200)
   b = INTEGER  (1025:65536)
   c = ENUM		(ON|OFF)
   d = STRING	(1:200)
   e = STRING	(1:200)
   f = STRING	(1:200)

 [Usage]
   CHG-SYS-INFO [a, b, c, d, e, f]
    ex) chg-sys-info CLOUD_DEV, 8889, ON, /DATA/record/upload, /DATA/record/result, greshaper@mindslab.ai
   CHG-SYS-INFO (sys_name=a), (mmc_port=b), (db_use_flag=c), (upload_dir=d), result_dir=e, admin_emailf=f
    ex) chg-sys-info sys_name=CLOUD_DEV, mmc_port=8889, db_use_flag=ON, upload_dir=/DATA/record/upload, result_dir=/DATA/record/result, admin_email=greshaper@mindslab.ai

 [Column information]
   sys_name            : System name
   mmc_port            : SIMc bind port
   db_use_flag         : Whether to use database
   UPLOAD_DIR          : Where to upload voice files from server
   RESULT_DIR          : Where the result informations are stored
   ADMIN_EMAIL         : Administrator email address

 [Result]
 <SUCCESS>
 Date time
 MMC = CHG-SYS-INFO
 Result = SUCCESS
 ===================================================
 [SYS_CONF]
  sys_name     = before -> after
  mmc_port     = before -> after
  db_use_flag  = before -> after
 [DB_INFO]
  UPLOAD_DIR   = before -> after
  RESULT_DIR   = before -> after
  ADMIN_EMAIL  = before -> after
  ===================================================

 <FAILURE>
 Date time
 MMC = CHG-SYS-INFO
 Result = FAILURE
 ===================================================
 Reason = Reason for error
 ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data):

	###### Optional Parameter ######
	if arg_data['sys_name'] != None :
		G_log.debug(arg_data['sys_name'])
		if len(arg_data['sys_name']) > 200 :
			return False, "'sys_name's Max length is 200"

	if arg_data['mmc_port'] != None :
		G_log.debug(arg_data['mmc_port'])
		if arg_data['mmc_port'].isdecimal() == False :
			return False, "'mmc_port' is Only Using Decimal"
		else :
			if len(arg_data['mmc_port']) < 4 or len(arg_data['mmc_port']) > 6:
				return False, "'mmc_port's Min length is 4 and  Max length is 6"
			else :
				if int(arg_data['mmc_port']) not in range(1025, 65536) :
					return False, "'mmc_port's Range is 1025 ~ 65536"

	if arg_data['db_use_flag'] != None :
		G_log.debug(arg_data['db_use_flag'])
		if (arg_data['db_use_flag'].lower() != 'on') and (arg_data['db_use_flag'].lower() != 'off'):
			return False, "'db_use_flag' is Only using 'ON' or 'OFF'"

	if arg_data['upload_dir'] != None :
		G_log.debug(arg_data['upload_dir'])
		if len(arg_data['upload_dir']) > 200 :
			return False, "'upload_dir's Max length is 200"

	if arg_data['result_dir'] != None :
		G_log.debug(arg_data['result_dir'])
		if len(arg_data['result_dir']) > 200 :
			return False, "'result_dir's Max length is 200"

	if arg_data['admin_email'] != None :
		G_log.debug(arg_data['admin_email'])
		p = re.compile('^[a-zA-Z0-9+-_.]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')
		if len(arg_data['admin_email']) > 200 :
			return False, "'admin_email's Max length is 200"
#		else :
#			if p.match(arg_data['admin_email']) != True :
#				return False, "'admin_email' is not email type. [{}]".format(arg_data['admin_email'])

	return True, ''


def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	arg_data = {}
	for item in org_list :
		arg_data[item] = None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"

	if ('=' in ARG[0]) :
		for item in ARG :
			if '=' not in item:
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not')"
			else:
				name, value = item.split('=', 1)
				# check each parameter's name validation
				if name not in org_list:
					return False, ARG_CNT, None, "PARAMETER Name is Invalid. '{}'".format(name)
				elif arg_data[name] != None :
					return False, ARG_CNT, None, "PARAMETER is duplicated. '{}'".format(name)
				else:
					arg_data[name] = value
	else:
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]] = ARG[idx]

	# check parameter's values
	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if(db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'"
				return make_result(MMC, ARG, result, reason, '')
				
			org_list = ["sys_name", "mmc_port", "db_use_flag" ,"upload_dir", "result_dir", "admin_email"]
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, org_list, CHG_SYS_INFO_MAX_PARAMETER, CHG_SYS_INFO_MIN_PARAMETER)
			if (ret == False):
				result = "FAILURE"
				return make_result(MMC, ARG, result, reason, '')
			else :
				### get old data from simd config file (/srv/maum/etc/simd.conf) ###
				org_data = OrderedDict()
				org_item_value = simd_config.items('SYS')
				for i in range(len(org_item_value)):
					org_data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(org_data))

				### get old data from 'MINUTES_COMMON' Table ###
				org_DB_data = DIS_Query(mysql, 'MINUTES_COMMON', '*', ';')
				if not org_DB_data :
					result='FAILURE'
					reason='Select Database Failure.'
					return make_result(MMC, ARG, result, reason, '')
				G_log.info('db_data = {}'.format(org_DB_data))

				## change config file data
				if Parsing_Dict['sys_name'] is not None :
					value = simd_config.set('SYS', 'sys_name', Parsing_Dict['sys_name'])
					if value == False :
						return make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [sys_name]', "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)	

				if Parsing_Dict['mmc_port'] is not None :
					value = simd_config.set('SYS', 'mmc_port', Parsing_Dict['mmc_port'])
					if value == False :
						return make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [mmc_port]', "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)	

				if Parsing_Dict['db_use_flag'] is not None :
					value = simd_config.set('SYS', 'db_use_flag', Parsing_Dict['db_use_flag'])
					if value == False :
						return make_result(MMC, ARG, 'FAILURE', 'Change Config File Failure [db_use_flag]', "")
					else :
						with open(G_simd_cfg_path, 'w') as configfile :
							simd_config.write(configfile)	

				## update DB data
				# make query
				G_log.debug('arg count ={}'.format(ARG_CNT))
				update_query_base = """ update MINUTES_COMMON set """
				if Parsing_Dict['upload_dir'] is not None :
					update_query = update_query_base + """ UPLOAD_DIR='{}'""".format(Parsing_Dict['upload_dir'])
					G_log.debug(update_query)
					ret = mysql.execute(update_query, True)
					if ret is False:
						result = 'FAILURE'
						reason = 'DB Update Failure'
						return make_result(MMC, ARG, result, reason, total_body)

				if Parsing_Dict['result_dir'] is not None :
					update_query = update_query_base + """ RESULT_DIR='{}'""".format(Parsing_Dict['result_dir'])
					G_log.debug(update_query)
					ret = mysql.execute(update_query, True)
					if ret is False:
						result = 'FAILURE'
						reason = 'DB Update Failure'
						return make_result(MMC, ARG, result, reason, total_body)

				if Parsing_Dict['admin_email'] is not None :
					update_query = update_query_base + """ ADMIN_EMAIL='{}'""".format(Parsing_Dict['admin_email'])
					G_log.debug(update_query)
					ret = mysql.execute(update_query, True)
					if ret is False:
						result = 'FAILURE'
						reason = 'DB Update Failure'
						return make_result(MMC, ARG, result, reason, total_body)

				# make response message
				total_body = total_body + ' [SYS_CONF]'
				if Parsing_Dict['sys_name'] is not None :
					total_body = total_body + '\n   sys_name       = {} -> {}'.format(org_data['sys_name'], Parsing_Dict['sys_name'])
				if Parsing_Dict['mmc_port'] is not None :
					total_body = total_body + '\n   mmc_port       = {} -> {}'.format(org_data['mmc_port'], Parsing_Dict['mmc_port'])
				if Parsing_Dict['db_use_flag'] is not None :
					total_body = total_body + '\n   db_use_flag    = {} -> {}'.format(org_data['db_use_flag'], Parsing_Dict['db_use_flag'])
				total_body = total_body + """\n----------------------------------------------------------"""
				total_body = total_body + '\n [DB_INFO]'
				if Parsing_Dict['upload_dir'] is not None :
					total_body = total_body + '\n   upload_dir     = {} -> {}'.format(org_DB_data[0]['UPLOAD_DIR'], Parsing_Dict['upload_dir'])
				if Parsing_Dict['result_dir'] is not None :
					total_body = total_body + '\n   result_dir     = {} -> {}'.format(org_DB_data[0]['RESULT_DIR'], Parsing_Dict['result_dir'])
				if Parsing_Dict['admin_email'] is not None :
					total_body = total_body + '\n   admin_email    = {} -> {}'.format(org_DB_data[0]['ADMIN_EMAIL'], Parsing_Dict['admin_email'])
			
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-SYS-INFO(), NoSectionError : [{}]".format(e))
		reason='CHG-SYS-INFO(), NoSectionError'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error("CHG-SYS-INFO(), Config read error: [{}]".format(e))
		reason='CHG-SYS-INFO(), Config read error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='CHG-SYS-INFO(), DB_connection error'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_SYS_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason = 'CHG-SYS-INFO(), SYSTEM FAILURE'
		return make_result(MMC, ARG, "FAILURE", reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)
	else :	
		if (result == 'FAILURE'):
			msg_body = """
==========================================================
 {}
==========================================================
""".format(reason)
	
		else :	
			msg_body = """
==========================================================
{}
==========================================================
""".format(total_body)
	
	return msg_body

def make_result(MMC, ARG, result, reason, total_body):
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help'):
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body 
	
	result_msg['msg_body']['data'] = data
	
	return result_msg


def DIS_Query(mysql, table, column, where):
	DIS_All_Query = """
		select {}
		from {}
		""".format(column, table)

	try :
		if where[-1] != ';' :
			where = where + ';'
		sql = DIS_All_Query + where
		rowcnt, rows = mysql.execute_query2(sql)

		for row in rows :
			for a in row :
				try :
					print('{} : {}' .format(a, row[a]))
				except Exception as e :
					pass
		return rows

	except Exception as e :
		print('Error Check {}' .format(e))
	return ''

def Update_Query(table, DB_data, ID, mysql):

	try :
		if (table == 'MINUTES_COMMON'):
			sql = """
				update MINUTES_COMMON set UPLOAD_DIR='{}', ADMIN_EMAIL='{}', RESULT_DIR='{}';
				""".format(DB_data['UPLOAD_DIR'], DB_data['ADMIN_EMAIL'], DB_data['RESULT_DIR'])

			mysql.execute(sql, True)

		return True

	except Exception as e :
		print('Error Check {}' .format(e))
		return False

