#! /usr/bin/python
#-*- coding:utf-8 -*-

##############################################################
# PYTHON COMMON MODULE
##############################################################
import sys
import os

##############################################################
# COMMON PATH
##############################################################
root_path = os.getenv("HOME") + '/MP'
exe_path = os.getenv("HOME") + "/MP/lib/simd"
mmc_path = os.getenv("HOME") + "/MP/lib/simd/MMC"
cfg_path = os.getenv("HOME") + "/MP/etc"

##############################################################
# SIMd
##############################################################
sys.path.append(exe_path)
sys.path.append(mmc_path)
from simd_main import simd_main

if __name__ == "__main__" :
	simd_main(root_path, exe_path, cfg_path)
	print("SIMd EXIT")
