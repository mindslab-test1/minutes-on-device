#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import re
import common.logger as logger
import traceback
import ConfigParser 
import socket
import ipaddress
from datetime import datetime
from collections import OrderedDict
from simd_main import *

CHG_DBCONF_INFO_MIN_PARAMETER = 1
CHG_DBCONF_INFO_MAX_PARAMETER = 9

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
   CHG-DBCONF-INFO	(TYPE=a), (PRI_HOST=b), (PRI_PORT=c), (SEC_HOST=d), (SEC_PORT=e), (USER=f), (PASSWD=g), (SID=h), (ENCODE=i)

 [Parameter]
   a = ENUM   	(mysql|maria|oracle)
   b = IPADDR 	(1:15)
   c = INTEGER  (1025:65536)
   d = IPADDR 	(1:15)
   e = INTEGER  (1025:65536)
   f = STRING 	(1:200)
   g = STRING 	(1:200)
   h = STRING 	(1:200)
   i = STRING 	(1:200)

 [Usage]
   CHG-DBCONF-INFO [a, b, c, d, e, f, g, h, i]
    ex) CHG-DBCONF-INFO mysql, 127.0.0.1, 3306, 127.0.0.1, 3306, minutes, minutes1234, minutes, utf8
   CHG-DBCONF-INFO (TYPE=a), (PRI_HOST=b), (PRI_PORT=c), (SEC_HOST=d), (SEC_PORT=e), (USER=f), (PASSWD=g), (SID=h), (ENCODE=i)
    ex) CHG-DBCONF-INFO TYPE=mysql, PRI_HOST=127.0.0.1, PRI_PORT=3306, SEC_HOST=127.0.0.1, SEC_PORT=3306, USER=minutes, PASSWD=minuete1234, SID=minutes, ENCODE=utf8

 [Column information]
  TYPE(db.type)             : Database type
  PRI_HOST(db.pri_host)     : Database primary_host
  PRI_PORT(db.pri_port)     : Database primary_port
  SEC_HOST(db.sec_host)     : Database second_host
  SEC_PORT(db.sec_port)     : Database second_port
  USER(db.user)             : Database user
  PASSWD(db.passwd)         : Database password
  SID(db.sid)               : Database sid
  ENCODE(db.encode)         : Database Encoding 
  
  ** Can be changed to option name in () when modifiying
  ** database.encode cannot be modified
  
 [Result]
   <SUCCESS>
  Date time
  MMC = CHG-DBCONF-INFO
  Result = SUCCESS
  ====================================================
  TYPE         = before -> after
  PRIMARY_HOST = before -> after
  PRIMARY_PORT = before -> after
  SECOND_HOST  = before -> after
  SECOND_PORT  = before -> after
  USER         = before -> after
  PASSWORD     = before -> after
  SID          = before -> after
  ====================================================
  
  <FAILURE>
  Date time
  MMC = CHG-DBCONF-INFO
  Result = FAILURE
  ====================================================
  Reason = Reason for error
  ====================================================
"""
	return total_body

def Check_Arg_Validation(arg_data):
	G_log.debug(arg_data)

	###### Optional Prameter ######
	if arg_data['db.type'] != None :
		G_log.debug(arg_data['db.type'])
		dbname_list = ['mysql', 'maria', 'oracle']
		if arg_data['db.type'].lower() not in dbname_list:
			return False, "'TYPE' is Only using 'mysql' or 'maria' or 'oracle'"

	if arg_data['db.pri_host'] != None :
		G_log.debug(arg_data['db.pri_host'])
		# do not parsing...just ask!!
		try:
			ip = ipaddress.ip_address(arg_data['db.pri_host'])
			G_log.debug("{} is a correct IP{} address.".format(ip, ip.version))
		except ValueError:
			return False, "IP Address Format Invalid. [{}]".format(arg_data['db.pri_host'])

	if arg_data['db.sec_host'] != None :
		G_log.debug(arg_data['db.sec_host'])
		# do not parsing...just ask!!
		try:
			ip = ipaddress.ip_address(arg_data['db.sec_host'])
			G_log.debug("{} is a correct IP{} address.".format(ip, ip.version))
		except ValueError:
			return False, "IP Address Format Invalid. [{}]".format(arg_data['db.sec_host'])

	if arg_data['db.pri_port'] != None :
		if arg_data['db.pri_port'] != None:
			G_log.debug(arg_data['db.pri_port'])
			port = arg_data['db.pri_port']
		else : 
			G_log.debug(arg_data['db.sec_port'])
			port = arg_data['db.sec_port']

		if port.isdecimal() == False :
			return False, "Port Value is Only Using Decimal"
		else : 
			if len(port) < 4 or len(port) > 6:
				return False, "Port Value's Min length is 4 and Max length is 6"
			else:
				if int(port) not in range(1025, 65536) :
					return False, "Port Value's Range is 1025 ~ 65536"

	if arg_data['db.sec_port'] != None :
		if arg_data['db.pri_port'] != None:
			G_log.debug(arg_data['db.pri_port'])
			port = arg_data['db.pri_port']
		else : 
			G_log.debug(arg_data['db.sec_port'])
			port = arg_data['db.sec_port']

		if port.isdecimal() == False :
			return False, "Port Value is Only Using Decimal"
		else : 
			if len(port) < 4 or len(port) > 6:
				return False, "Port Value's Min length is 4 and Max length is 6"
			else:
				if int(port) not in range(1025, 65536) :
					return False, "Port Value's Range is 1025 ~ 65536"
	
	if arg_data['db.user'] != None :
		G_log.debug(arg_data['db.user'])
		if len(arg_data['db.user']) > 200 :
			return False, "USER's MAX Length is 200"
	
	if arg_data['db.passwd'] != None :
		G_log.debug(arg_data['db.passwd'])
		if len(arg_data['db.passwd']) > 200 :
			return False, "PASSWORD's MAX Length is 200"

	if arg_data['db.sid'] != None :
		G_log.debug(arg_data['db.sid'])
		if len(arg_data['db.sid']) > 200 :
			return False, "SID's MAX Length is 200"

	if arg_data['db.encode'] != None :
		G_log.debug(arg_data['db.encode'])
		if len(arg_data['db.encode']) > 200 :
			return False, "ENCODE's MAX Length is 200"
	
	return True, ""


def Arg_Parsing(ARG, org_list, max_cnt, min_cnt) :

	arg_data = {}
	for item in org_list :
		arg_data[item] = None

	# ARG Count Check
	ARG_CNT = len(ARG)
	if ARG_CNT > max_cnt or ARG_CNT < min_cnt :
		return False, ARG_CNT, None, "PARAMETER Count is Invalid"

	if ('=' in ARG[0]) :
		for item in ARG :
			if '=' not in item :
				return False, ARG_CNT, None, "PARAMETER Type is Invalid ('=' is used 'all' or 'not')"
			else : 
				name, value = item.split('=', 1)
				name = 'db.' + name.lower()
				G_log.debug(name)
				# check each parameters' name validation
				if name not in arg_data:
					return False, ARG_CNT, None, "PARAMETER Type is Invalid. '{}'".format(name)
				elif arg_data[name] != None :
					return False, ARG_CNT, None, "PARAMETER Type is duplication. '{}'".format(name)
				else:
					arg_data[name] = value
	else :
		# you have to input all parameter
		if ARG_CNT != len(org_list) :
			return False, ARG_CNT, None, "PARAMETER Count is Invalid"

		for idx in range(len(ARG)) :
			arg_data[org_list[idx]] = ARG[idx]


	# check parameter's values
	result, reason = Check_Arg_Validation(arg_data)

	return result, ARG_CNT, arg_data, reason

def proc_exec(MMC, ARG, mysql):
	
	total_body=''
	
	try :
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/dbconf.conf)
			db_config = ConfigParser.RawConfigParser()
			db_config.read(G_db_cfg_path)

			# make argument list (parsing and check validation)
			org_list = ["db.type", "db.pri_host", "db.pri_port", "db.sec_host", "db.sec_port", "db.user", "db.passwd", "db.sid", "db.encode"]
			ret, ARG_CNT, Parsing_Dict, reason = Arg_Parsing(ARG, org_list, CHG_DBCONF_INFO_MAX_PARAMETER, CHG_DBCONF_INFO_MIN_PARAMETER)
			if (ret == False):
				result = "FAILURE"
				return make_result(MMC, ARG, result, reason, Parsing_Dict)
			
			else :
				### get old data from DB config file (/srv/maum/etc/dbconf.conf) ###
				org_data = OrderedDict()
				org_item_value = db_config.items('DBCONF')
				for i in range(len(org_item_value)) :
					org_data[org_item_value[i][0]] = org_item_value[i][1]
				G_log.info('conf_data = {}'.format(org_data))
	
				## change config file data
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						G_log.debug(item, Parsing_Dict[item])
						value = db_config.set('DBCONF', item, Parsing_Dict[item])
						if value == False :
							reson = 'Change Config File Failure [{}]'.format(item)
							return make_result(MMC, ARG, 'FAILURE', reason, "")

				with open(DB_ConfPath, 'w') as configfile :
					db_config.write(configfile)

				total_body = total_body + ' [DBCONF]'
				for item in Parsing_Dict :
					if Parsing_Dict[item] != None :
						total_body = total_body + '\n {0:15}       = {1} -> {2}'.format(item, org_data[item], Parsing_Dict[item])
			
				G_log.info('CHG-DBCONF-INFO Complete!!')
				return make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.MissingSectionHeaderError  as e:
		G_log.error('CHG-DBCONF-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='Config_File read error [{}]'.format(DB_ConfPath)
	except Exception as e:
		G_log.error('CHG-DBCONF-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		result='FAILURE'
		reason='[{}] SYSTEM FAILURE'.format(MMC)

	return make_result(MMC, ARG, result, reason, total_body)

def make_result_header(MMC, result):
	now = datetime.now()
	msg_header = """
{}
MMC    = {}
RESULT = {}
""".format(now, MMC.upper(), result)

	return msg_header

def make_result_body(MMC, ARG, result, reason, total_body):
	if (ARG == 'help'):
		msg_body = "{}".format(total_body)

	else :
		if (result == 'FAILURE'):
			msg_body = """
====================================================
 {}
====================================================
""".format(reason)
		else :
			msg_body = """
====================================================
{}
====================================================
""".format(total_body)

	return msg_body

def make_result(MMC, ARG, result, reason, total_body) :
	result_msg={}
	result_msg['msg_header'] = {}
	result_msg['msg_header']['msg_id'] = 'MMC_Response'
	result_msg['msg_body'] = {}
	result_msg['msg_body']['mmc'] = MMC.upper()
	result_msg['msg_body']['result'] = result

	msg_header = make_result_header(MMC, result)
	msg_body = make_result_body(MMC, ARG, result, reason, total_body)

	if (ARG == 'help') :
		data = msg_header + msg_body
	else :
		data = msg_header + msg_body
	
	result_msg['msg_body']['data'] = data
	return result_msg


