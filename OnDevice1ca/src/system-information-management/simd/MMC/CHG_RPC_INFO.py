#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import re
import common.logger as logger
import traceback
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

CHG_RPC_INFO_MIN_PARAMETER = 2
CHG_RPC_INFO_MAX_PARAMETER = 2

param_list = ['RPC_NAME', 'COMMAND']
mandatory_list = ['RPC_NAME', 'COMMAND']

def mmc_help():
	total_body = """
============================================================
 [] = mandatory, () = optional
============================================================
 [Command]
  CHG-RPC-INFO [RPC_NAME=a], [COMMAND=b]

 [Parameter]
  a = STRING    (1:100)
  b = STRING    (1:100)

 [Usage]
  CHG-RPC-INFO [a, b]
   ex) CHG-RPC-INFO STT-RESTART, /home/minds/sh/stt_restart.sh
  CHG-RPC-INFO [RPC_NAME=a], [COMMAND=b]
   ex) CHG-RPC-INFO [RPC_NAME=STT-RESTART], [COMMAND=/home/minds/sh/stt_restart.sh]

 [Section Options Configuration]
  RPC_NAME             : Remote Process Command Name
  COMMAND              : Real Command

 [Result]
  <SUCCESS>
  Date time
  MMC = CHG-RPC-INFO
  Result = SUCCESS
  ===================================================
   RPC NAME             = a
   COMMAND              = before -> after
  ===================================================

  <FAILURE>
  Date time
  MMC = CHG-RPC-INFO
  Result = FAILURE
  ===================================================
  Reason = Reason for error
  ===================================================
"""
	return total_body

def Check_Arg_Validation(arg_data, mandatory_list):
	
	##### Mandatory Parameter #####
	if arg_data['RPC_NAME'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'RPC_NAME', arg_data['RPC_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['COMMAND'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'COMMAND', arg_data['COMMAND'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''
	
	try :
		# if client input 'help'
		if (ARG == "help"):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/rpc_info.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_rpc_cfg_path)
			
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, CHG_RPC_INFO_MAX_PARAMETER, CHG_RPC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result = "FAILURE"
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0):
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')
			
			### get old data from simd config file (/srv/maum/etc/rpc_info.conf) ###
			org_data = OrderedDict()
			org_item_value = simd_config.items('RPC_COMMAND')
			exist = False
			for i in range(len(org_item_value)):
				if Parsing_Dict['RPC_NAME'].lower() == org_item_value[i][0]:
					exist = True
				org_data[org_item_value[i][0]] = org_item_value[i][1]
				if i is len(org_item_value)-1 and exist is False:
					reason = '[{}] is not Exist.'.format(Parsing_Dict['RPC_NAME'].lower())
					return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
			G_log.info('conf_data = {}'.format(org_data))

			### change config file data
			Parsing_Dict['COMMAND'] = Parsing_Dict['COMMAND'].replace('_', ' ')
			value = simd_config.set('RPC_COMMAND', Parsing_Dict['RPC_NAME'].lower(), Parsing_Dict['COMMAND'])
			if value == False :
				reason = 'Change Config File Failure. [{}]'.format(item)
				return MMCLib.make_result(MMC, ARG, 'FAILURE', reason, "")
			else :
				with open(G_rpc_cfg_path, 'w') as configfile :
					simd_config.write(configfile)
				# make response message
				total_body = ' {:20} = {}\n'.format('RPC_NAME', Parsing_Dict['RPC_NAME'].upper())
				total_body = total_body + ' {:20} = {} -> {}'.format('COMMAND', org_data[Parsing_Dict['RPC_NAME'].lower()], Parsing_Dict['COMMAND'])

			G_log.info('CHG-RPC-INFO Complete!!')
			return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("CHG-RPC-INFO(), NoSectionError : [{}]".format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-RPC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e:
		G_log.error('CHG-RPC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='CHG-RPC-INFO(), Config_File read error [{}]'.format(G_rpc_cfg_path)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('CHG_RPC_INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='CHG_RPC_INFO(), [{}] SYSTEM FAILURE'.format(MMC)
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

	return MMCLib.make_result(MMC, ARG, result, reason, total_body)

