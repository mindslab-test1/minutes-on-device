#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

DEL_MIC_INFO_MIN_PARAMETER = 1
DEL_MIC_INFO_MAX_PARAMETER = 1

param_list = ["MINUTES_MIC_SER"]
mandatory_list = ["MINUTES_MIC_SER"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  DEL-MIC-INFO [MINUTES_MIC_SER=a]

 [Parameter]
  a = DECIMAL    (1:9999999999)

 [Usage]
  DEL-MIC-INFO [a]
   ex) DEL-MIC-INFO 3
  DEL-MIC-INFO [MINUTES_MIC_SER=a]
   ex) DEL-MIC-INFO MINUTES_MIC_SER=3
  
 [Column information]
  MINUTES_MIC_SER     : MIC Serial Number


 [Result]
 <SUCCESS>
 Date time
 MMC    = DEL-MIC-INFO 
 Result = SUCCESS
 ====================================================================================================================================================
  MIC_SER |  MIC_GROUP  |  MIC_TYPE  |     MIC_ID     | USED_FLAG |   MIC_IP   | STT_PORT | VIRTUAL_FLAG |  VIRTUAL_IP  | VIRTUAL_PORT | MIC_STATUS |
 ----------------------------------------------------------------------------------------------------------------------------------------------------
  value   | value       | value      | value          | value     | value      | value    | value        | value        | value        | value      |
  ....
 ====================================================================================================================================================


 <FAILURE>
 Date time
 MMC    = DEL-MIC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body

	
def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['MINUTES_MIC_SER'] != None:
		ret, reason = check_Decimal_And_Range(MIN_INT10_VALUE, MAX_INT10_VALUE, 'MINUTES_MIC_SER', arg_data['MINUTES_MIC_SER'], G_log)
		if (ret == False) : return False, reason

	return True, ''

def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
			# load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			# make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, DEL_MIC_INFO_MAX_PARAMETER, DEL_MIC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# delete pricing information
			try :
				data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_MIC", "*" , "where MINUTES_MIC_SER= {};".format(Parsing_Dict['MINUTES_MIC_SER']))
				print (data_cnt)
				if int(data_cnt) is 0 :
					result = 'FAILURE'
					reason = "MINUTES_MIC_ID [{}] does not exist".format(Parsing_Dict['MINUTES_MIC_SER'])
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				if err is not None:
					result = 'FAILURE'
					reason = "Select Failure."
					return MMCLib.make_result(MMC, ARG, result, reason, total_body)

				ret, err = MMCLib.Delete_query(mysql, "MINUTES_MIC", "MINUTES_MIC_SER={}".format(Parsing_Dict['MINUTES_MIC_SER']))
				if ret is False:
					result='FAILURE'
					if err is not None:
						reason='Delete Failure. [{}]'.format(err)
					else:
						reason='Delete Failure.'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('DEL-MIC-INFO(), ERROR Occured [{}]' .format(e))
				result='FAILURE'
				reason='DB Delete Failure'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_MIC", "*" , ';')
			if int(data_cnt) is 0 :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			elif err is not None:
				result = 'FAILURE'
				reason = "Select Failure."
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				G_log.debug(db_data)
				total_body=""" {:^7} | {:^13} | {:^12} | {:^20} | {:^10} | {:^18} | {:^6} | {:^14} | {:^18} | {:^14} | {:^12} |\n""" .format('MIC_SER', 'MIC_GROUP', 'MIC_TYPE', 'MIC_ID', 'USED_FLAG', 'MIC_IP', 'STT_PORT', 'VIRTUAL_FLAG', 'VIRTUAL_IP', 'VIRTUAL_PORT', 'MIC_STATUS')
				total_body= total_body + ('-' * MMC_SEND_LINE_MIC_CNT)
				for thr_num in range(len(db_data)) :
					mic_ser = db_data[thr_num]["MINUTES_MIC_SER"]
					mic_group = db_data[thr_num]["MIC_GROUP"]
					mic_type = db_data[thr_num]["MIC_TYPE"]
					if mic_type == 0:
						mic_type = "유선마이크"
					elif mic_type == 1:
						mic_type = "무선마이크"
					else:
						mic_type = "가상마이크"
					mic_id = db_data[thr_num]["MIC_ID"]
					used_flag = db_data[thr_num]["USED_FLAG"]
					if used_flag is '0':
						used_flag = "OFF"
					else:
						used_flag = "ON"
					mic_ip = db_data[thr_num]["MIC_IPADDR"]
					if mic_ip is None:
						mic_ip = "NULL"
					stt_port = db_data[thr_num]["STT_DST_PORT"]
					virtual_flag = db_data[thr_num]["VIRTUAL_MIC_FLAG"]
					if virtual_flag is '0':
						virtual_flag = "OFF"
					else:
						virtual_flag = "ON"
					virtual_ip = db_data[thr_num]["VIRTUAL_SPLIT_IP"]
					virtual_port = db_data[thr_num]["VIRTUAL_SPLIT_PORT"]
					mic_status = db_data[thr_num]["MIC_STATUS"]
					if mic_status is '0':
						mic_status = "NOT USE"
					else:
						mic_status = "USE"
					row_buf="\n  {:^7} | {:^13} | {:^12} | {:^20} | {:^10} | {:^18} | {:^6} | {:^14} | {:^18} | {:^14} | {:^12} |" .format(mic_ser, mic_group, mic_type, mic_id, used_flag, mic_ip, stt_port, virtual_flag, virtual_ip, virtual_port, mic_status)

					total_body = total_body + row_buf

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("DEL-MIC-INFO(), NoSectionError : [{}]".format(e))
		reason='DEL-MIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("DEL-MIC-INFO(), Config read error: [{}]".format(e))
		reason='DEL-MIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='DEL-MIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('DEL-MIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='DEL-MIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

