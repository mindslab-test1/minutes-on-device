#! /usr/bin/python
#-*- coding:utf-8 -*-

import os
import sys
import common.logger as logger
import common.dblib as DBLib
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import libmmc as MMCLib
else:
	import configparser as ConfigParser
	from . import libmmc as MMCLib
import pymysql
import traceback
import bcrypt
import re
from datetime import datetime
from collections import OrderedDict
from simd_main import *
from .libmmc import *

ADD_MIC_INFO_MIN_PARAMETER = 4
ADD_MIC_INFO_MAX_PARAMETER = 9

param_list = ["MIC_NAME", "MIC_TYPE", "USED_FLAG", "STT_PORT", "MIC_GROUP", "MIC_ID", "MIC_IP", "VIRTUAL_IP", "VIRTUAL_PORT"]
mandatory_list = ["MIC_NAME", "MIC_TYPE", "USED_FLAG", "STT_PORT"]
mic_type_list = ["유선마이크", "무선마이크", "가상마이크"]

def mmc_help():
	total_body = """
===========================================================
 [] = mandatory, () = optional
===========================================================
 [Command]
  ADD-MIC-INFO [MIC_NAME=a], [MIC_TYPE=b], [USED_FLAG=c], [STT_PORT=d], (MIC_GROUP=e), (MIC_ID=f), (MIC_IP=g), (VIRTUAL_IP=h), (VIRTUAL_PORT=i)

 [Parameter]
  a = STRING      (1:100)
  b = ENUM        (유선마이크|무선마이크|가상마이크)
  c = ENUM        (ON|OFF)
  d = INTEGER     (1025:65536)
  e = STRING      (1:100)
  f = STRING      (1:100)
  g = IPADDR      (1:15)
  h = IPADDR      (1:15)
  i = INTEGER     (1025:65536)

 [Usage]
  ADD-MIC-INFO [a, b, c, d, e, f, g, h, i]
   ex) ADD-MIC-INFO 유선마이크1, 유선마이크, ON, 50011, 대회의실, mindalab-minutes-001, 127.0.0.1, 127.0.0.1, 50021
       ADD-MIC-INFO 무선마이크1, 무선마이크, ON, 50012, 중회의실, mindalab-minutes-002, 127.0.0.1, 127.0.0.1, 50022
       ADD-MIC-INFO 무선마이크1, 무선마이크, ON, 50013, 소회의실, mindalab-minutes-003, 127.0.0.1, 127.0.0.1, 50023
  ADD-MIC-INFO [MIC_NAME=a], [MIC_TYPE=b], [USED_FLAG=c], [STT_PORT=d], (MIC_GROUP=e), (MIC_ID=f), (MIC_IP=g), (VIRTUAL_IP=h), (VIRTUAL_PORT=i)
   ex) ADD-MIC-INFO MIC_NAME=유선마이크1, MIC_TYPE=유선마이크, USED_FLAG=ON, MIC_IP=127.0.0.1, STT_PORT=50011
       ADD-MIC-INFO MIC_NAME=유선마이크1, MIC_TYPE=유선마이크, USED_FLAG=ON, MIC_IP=127.0.0.1, STT_PORT=50011, MIC_GROUP=대회의실, MIC_ID=mindslab-minutes-001
       ADD-MIC-INFO MIC_NAME=무선마이크1, MIC_TYPE=무선마이크, USED_FLAG=ON, STT_PORT=50011
       ADD-MIC-INFO MIC_NAME=무선마이크1, MIC_TYPE=무선마이크, USED_FLAG=ON, STT_PORT=50011, MIC_GROUP=대회의실, MIC_ID=mindslab-minutes-001
       ADD-MIC-INFO MIC_NAME=가상마이크1, MIC_TYPE=가상마이크, USED_FLAG=ON, STT_PORT=50011, MIC_IP=127.0.0.1, VIRTUAL_IP=127.0.0.1, VIRTUAL_PORT=50011
       ADD-MIC-INFO MIC_NAME=가상마이크1, MIC_TYPE=가상마이크, USED_FLAG=ON, STT_PORT=50011, MIC_GROUP=대회의실, MIC_ID=mindslab-minutes-001, MIC_IP=127.0.0.1, VIRTUAL_IP=127.0.0.1, VIRTUAL_PORT=50011
  
 [Column information]
  MIC_SER             : MIC Serial Number
  MIC_NAME            : MIC Name
  MIC_GROUP           : MIC Group
  MIC_TYPE            : MIC Type
  MIC_ID              : MIC Identity
  USED_FLAG           : Whether to use a MIC
  MIC_IP              : MIC IP Address
  STT_PORT            : STT Send Port Number
  VIRTUAL_FLAG        : whether to be virtual MIC
  VIRTUAL_IP          : Virtual MIC IP Address
  VIRTUAL_PORT        : Virtual MIC Port Number
  MIC_STATUS          : MIC Status

 [Result]
 <SUCCESS>
 Date time
 MMC    = ADD-MIC-INFO 
 Result = SUCCESS
 ===================================================================================================================================================================
  MIC_SER |     MIC_NAME    |  MIC_GROUP  |  MIC_TYPE  |     MIC_ID     | USED_FLAG |   MIC_IP   | STT_PORT | VIRTUAL_FLAG |  VIRTUAL_IP | VIRTUAL_PORT | MIC_STATUS |
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------
  value   | value           | value       | value      | value          | value     | value      | value    | value        | value       | value        | value      |
  ...
 ==================================================================================================================================================================

 <FAILURE>
 Date time
 MMC    = ADD-MIC-INFO
 Result = FAILURE
 ====================================================
 Reason = Reason for error
 ====================================================
"""
	return total_body


def Check_Arg_Validation(arg_data, mandatory_list):

	# Mandatory Parameter Check
	for item in mandatory_list:
		if (arg_data[item] == None) :
			return False, "'{}' is Mandatory Parameter.".format(item)

	##### Mandatory Parameter #####
	if arg_data['MIC_NAME'] != None :
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'MIC_NAME', arg_data['MIC_NAME'], G_log)
		if (ret == False) : return False, reason

	if arg_data['MIC_TYPE'] != None :
		ret, reason = check_Enum(mic_type_list, 'MIC_TYPE', arg_data['MIC_TYPE'], G_log)
		if (ret == False) : return False, reason

	if arg_data['USED_FLAG'] != None :
		ret, reason = check_Enum(flag_list, 'USED_FLAG', arg_data['USED_FLAG'], G_log)
		if (ret == False) : return False, reason

	if arg_data['STT_PORT'] != None:
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'STT_PORT', arg_data['STT_PORT'], G_log)
		if (ret == False) : return False, reason

	##### Optional Parameter #####
	if arg_data['MIC_GROUP'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'MIC_GROUP', arg_data['MIC_GROUP'], G_log)
		if (ret == False) : return False, reason

	if arg_data['MIC_ID'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'MIC_ID', arg_data['MIC_ID'], G_log)
		if (ret == False) : return False, reason

	if arg_data['MIC_IP'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'MIC_IP', arg_data['MIC_IP'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Ip_Address('MIC_IP', arg_data['MIC_IP'], G_log)
		if (ret == False) : return False, reason

	if arg_data['VIRTUAL_IP'] != None:
		ret, reason = check_String_Length(MIN_STRING_LENGTH, MAX_STRING100_LENGTH, 'VIRTUAL_IP', arg_data['VIRTUAL_IP'], G_log)
		if (ret == False) : return False, reason

		ret, reason = check_Ip_Address('VIRTUAL_IP', arg_data['VIRTUAL_IP'], G_log)
		if (ret == False) : return False, reason
	
	if arg_data['VIRTUAL_PORT'] != None:
		ret, reason = check_Decimal_And_Range(MIN_PORT_NUMBER, MAX_PORT_NUMBER, 'VIRTUAL_PORT', arg_data['VIRTUAL_PORT'], G_log)
		if (ret == False) : return False, reason

	return True, ''


def proc_exec(MMC, ARG, mysql):

	total_body=''

	try :
		# if client input 'help'
		if (ARG == 'help'):
			total_body = mmc_help()
			result = 'SUCCESS'
			reason = ''
			return MMCLib.make_result(MMC, ARG, result, reason, total_body)
		else :
		    # load simd config file (/srv/maum/etc/simd.conf)
			simd_config = ConfigParser.RawConfigParser()
			simd_config.read(G_simd_cfg_path)
			db_use_flag = simd_config.get('SYS', 'db_use_flag').lower()
			if (db_use_flag != 'on'):
				result = "FAILURE"
				reason = "'db_use_flag' in simd.conf is not 'on'" 
				return MMCLib.make_result(MMC, ARG, result, reason, '')

            # make argument list (parsing and check validation)
			ret, ARG_CNT, Parsing_Dict, reason = MMCLib.Argument_Parsing(ARG, param_list, ADD_MIC_INFO_MAX_PARAMETER, ADD_MIC_INFO_MIN_PARAMETER)
			if (ret == False) :
				result='FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, Parsing_Dict)

			if (ARG_CNT > 0) :
				ret, reason = Check_Arg_Validation(Parsing_Dict, mandatory_list)
				if (ret == False) :
					result = 'FAILURE'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			# insert new pricing information
			try :
				total_body = ''

				# change used flag value
				if Parsing_Dict['USED_FLAG'] is 'OFF':
					used_flag = '0'
				else:
					used_flag = '1'

				if Parsing_Dict['MIC_TYPE'] == '유선마이크':
					mic_type = 0
				elif Parsing_Dict['MIC_TYPE'] == '무선마이크':
					mic_type = 1
				else:
					mic_type = 2

				if mic_type is not 2 :
					virtual_flag = 0
					virtual_ip = '-'
					virtual_port = '-'
					virtual_status = 0
				else:
					virtual_flag = 1
					virtual_ip = Parsing_Dict['VIRTUAL_IP']
					virtual_port = Parsing_Dict['VIRTUAL_PORT']
					virtual_status = 0

				if Parsing_Dict['MIC_GROUP'] is None:
					mic_group = '-'
				else:
					mic_group = Parsing_Dict['MIC_GROUP']

				if Parsing_Dict['MIC_ID'] is None:
					mic_id = '-'
				else:
					mic_id = Parsing_Dict['MIC_ID']

				tmp_mic_name = ''

				if Parsing_Dict['MIC_IP'] is None:
					insert_query = """ INSERT INTO MINUTES_MIC value (NULL, 'simd', now(), NULL, NULL, '{}', {}, '{}', '{}', '{}', NULL ,'{}', '{}', '{}', '{}', '{}', '{}', '{}') """.format(used_flag, mic_type, mic_group, mic_id, Parsing_Dict['MIC_NAME'], Parsing_Dict['STT_PORT'], 0, tmp_mic_name, virtual_flag, virtual_ip, virtual_port, virtual_status) 
				else:
					mic_ip = Parsing_Dict['MIC_IP']
				insert_query = """ INSERT INTO MINUTES_MIC value (NULL, 'simd', now(), NULL, NULL, '{}', {}, '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}') """.format(used_flag, mic_type, mic_group, mic_id, Parsing_Dict['MIC_NAME'], mic_ip, Parsing_Dict['STT_PORT'], 0, tmp_mic_name, virtual_flag, virtual_ip, virtual_port, virtual_status) 

				G_log.debug(insert_query)

				ret, err = mysql.execute(insert_query)
				if (ret == False) :
					result = 'FAILURE'
					if err is not None:
						reason = 'DB Insert Failure. [{}]'.format(err)
					else:
						reason = 'DB Insert Failure.'
					return MMCLib.make_result(MMC, ARG, result, reason, '')

			except Exception as e:
				G_log.critical(traceback.format_exc())
				G_log.critical('ADD-MIC-INFO(), ERROR Occured [{}]' .format(e))
				result = 'FAILURE'
				reason = 'DB INSERT FAILURE'
				return MMCLib.make_result(MMC, ARG, result, reason, '')

			data_cnt, db_data, err = MMCLib.Select_query(mysql, "MINUTES_MIC", "*" , ';')
			if int(data_cnt) is 0 :
				result = 'FAILURE'
				reason = "DB_data does not exist"
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			elif err is not None:
				result = 'FAILURE'
				reason = "Select Failure."
				return MMCLib.make_result(MMC, ARG, result, reason, total_body)
			else :
				G_log.debug(db_data)
				total_body=""" {:^7} | {:^13} | {:^12} | {:^20} | {:^10} | {:^18} | {:^6} | {:^14} | {:^18} | {:^14} | {:^12} |\n""" .format('MIC_SER', 'MIC_GROUP', 'MIC_TYPE', 'MIC_ID', 'USED_FLAG', 'MIC_IP', 'STT_PORT', 'VIRTUAL_FLAG', 'VIRTUAL_IP', 'VIRTUAL_PORT', 'MIC_STATUS')
				total_body= total_body + ('-' * MMC_SEND_LINE_MIC_CNT)

				for thr_num in range(len(db_data)) :
					mic_ser = db_data[thr_num]["MINUTES_MIC_SER"]
					mic_group = db_data[thr_num]["MIC_GROUP"]
					mic_type = db_data[thr_num]["MIC_TYPE"]
					if mic_type == 0:
						mic_type = "유선마이크"
					elif mic_type == 1:
						mic_type = "무선마이크"
					else:
						mic_type = "가상마이크"
					mic_id = db_data[thr_num]["MIC_ID"]
					used_flag = db_data[thr_num]["USED_FLAG"]
					if used_flag is '0':
						used_flag = "OFF"
					else:
						used_flag = "ON"
					mic_ip = db_data[thr_num]["MIC_IPADDR"]
					if mic_ip is None:
						mic_ip = "NULL"
					stt_port = db_data[thr_num]["STT_DST_PORT"]
					virtual_flag = db_data[thr_num]["VIRTUAL_MIC_FLAG"]
					if virtual_flag is '0':
						virtual_flag = "OFF"
					else:
						virtual_flag = "ON"
					virtual_ip = db_data[thr_num]["VIRTUAL_SPLIT_IP"]
					virtual_port = db_data[thr_num]["VIRTUAL_SPLIT_PORT"]
					mic_status = db_data[thr_num]["MIC_STATUS"]
					if mic_status is '0':
						mic_status = "NOT USE"
					else:
						mic_status = "USE"

					row_buf="\n  {:^7} | {:^13} | {:^12} | {:^20} | {:^10} | {:^18} | {:^6} | {:^14} | {:^18} | {:^14} | {:^12} |" .format(mic_ser, mic_group, mic_type, mic_id, used_flag, mic_ip, stt_port, virtual_flag, virtual_ip, virtual_port, mic_status)
					total_body = total_body + row_buf 

				return MMCLib.make_result(MMC, ARG, 'SUCCESS', '', total_body)

	except ConfigParser.NoSectionError as e :
		G_log.error("ADD-MIC-INFO(), NoSectionError : [{}]".format(e))
		reason='ADD-MIC-INFO(), NoSectionError'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except ConfigParser.MissingSectionHeaderError as e :
		G_log.error("ADD-MIC-INFO(), Config read error: [{}]".format(e))
		reason='ADD-MIC-INFO(), Config read error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except pymysql.err.OperationalError as e:
		G_log.error("DB_connection error : [{}]".format(e))
		G_log.error("Change 'db_use_flag' of simd.conf to 'OFF'")
		reason='ADD-MIC-INFO(), DB_connection error'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)
	except Exception as e:
		G_log.error('ADD-MIC-INFO(), ERROR Occured [{}]' .format(e))
		G_log.error(traceback.format_exc())
		reason='ADD-MIC-INFO(), SYSTEM FAILURE'
		return MMCLib.make_result(MMC, ARG, "FAILURE", reason, total_body)

