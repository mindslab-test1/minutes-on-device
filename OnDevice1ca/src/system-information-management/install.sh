DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop system-information-management
sleep 1

#cp simc.py $MAUM_ROOT/bin/simc
#cp simd.py $MAUM_ROOT/bin/simd
#cp -rp ./simd/ $MAUM_ROOT/lib/

cp ${DIR}/simc.py $HOME/MP/bin/simc
cp ${DIR}/simd.py $HOME/MP/bin/simd
cp -rp ${DIR}/simd/ $HOME/MP/lib/

svctl start system-information-management
