#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import logging
import time
import grpc
import argparse
import webrtcvad

from concurrent import futures
from vad import frame_generator, vad_collector

from vad_pb2_grpc import add_VadServicer_to_server, VadServicer
from vad_pb2 import Segment

_ONE_DAY_IN_SECONDS = 60 * 60 * 24


class VadServicerImpl(VadServicer):
    def __init__(self, sample_rate, duration, aggressiveness):
        super().__init__()
        self.sample_rate = sample_rate
        self.duration = duration
        self.aggressiveness = aggressiveness
        self.padding = int(self.duration / 100 * sample_rate)

    def Detect(self, request_iterator, context):
        try:
            vad = webrtcvad.Vad(self.aggressiveness)
            frames = frame_generator(self.duration, request_iterator, self.sample_rate)
            previous_end = 0
            for start, end in vad_collector(self.sample_rate, self.duration, self.duration * 10, vad, frames):
                #logging.debug('response: {start: %s, end: %s}', start, end)
                logging.critical('response: {start: %s, end: %s}', start, end)
                print('response: {start: %s, end: %s}', start, end)
                start = max(previous_end, start - self.padding)
                yield Segment(start=start, end=end)
                previous_end = end
        except Exception as e:
            logging.exception(e)
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='vad server executor')
    parser.add_argument('-l', '--log_level',
                        nargs='?',
                        dest='log_level',
                        help='logger level',
                        type=str,
                        default='INFO')
    parser.add_argument('-p', '--port',
                        nargs='?',
                        dest='port',
                        help='grpc port',
                        type=int,
                        default=14001)
    parser.add_argument('-m', '--max_workers',
                        nargs='?',
                        dest='max_workers',
                        help='thread pool max workers',
                        type=int,
                        default=300)
    parser.add_argument('-s', '--sample_rate',
                        nargs='?',
                        dest='sample_rate',
                        help='pcm sample rate(Hz)',
                        type=int,
                        #default=16000)
                        default=8000)
    parser.add_argument('-d', '--duration',
                        nargs='?',
                        dest='duration',
                        help='frame\'s duration(10, 20, 30ms)',
                        type=int,
                        #default=30)
                        default=20)
                        #default=10)
    parser.add_argument('-a', '--aggressiveness',
                        nargs='?',
                        dest='aggressiveness',
                        help='aggressiveness mode(0~3)',
                        type=int,
                        #default=1)
                        #default=2)
                        default=3)

    args = parser.parse_args()

    vad_servicer = VadServicerImpl(args.sample_rate, args.duration, args.aggressiveness)

    server = grpc.server(futures.ThreadPoolExecutor(max_workers=args.max_workers),)
    add_VadServicer_to_server(vad_servicer, server)
    server.add_insecure_port('[::]:{}'.format(args.port))
    server.start()

    logging.basicConfig(
        level=getattr(logging, args.log_level),
        format='[%(levelname)s|%(filename)s:%(lineno)s][%(asctime)s] >>> %(message)s'
    )
    logging.info('vad starting at 0.0.0.0:%d', args.port)

    try:
        while True:
            # Sleep forever, since `start` doesn't block
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)
