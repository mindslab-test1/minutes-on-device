#!/usr/bin/env python3.5
# -*- coding: UTF-8 -*-

import argparse
import grpc

from vad_pb2_grpc import VadStub
from vad_pb2 import Speech


class VadClient(object):
    def __init__(self, remote='127.0.0.1:14001', chunk_size=1024*1024):
        channel = grpc.insecure_channel(remote)
        self.stub = VadStub(channel)
        self.chunk_size = chunk_size

    def detect(self, pcm_binary):
        pcm_binary = self._generate_wav_binary_iterator(pcm_binary)
        return self.stub.Detect(pcm_binary)

    def _generate_wav_binary_iterator(self, pcm_binary):
        for idx in range(0, len(pcm_binary), self.chunk_size):
            yield Speech(bin=pcm_binary[idx:idx+self.chunk_size])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='vad client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='127.0.0.1:14001')
    parser.add_argument('-i', '--input',
                        nargs='?',
                        help='input wav file',
                        type=str,
                        required=True)
    args = parser.parse_args()

    client = VadClient(args.remote)

    with open(args.input, 'rb') as rf:
        pcm_binary = rf.read()[44:]
        for segment in client.detect(pcm_binary):
            print(segment.start, segment.end)
