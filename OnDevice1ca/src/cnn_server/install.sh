DIR="$( cd "$( dirname "$0" )" && pwd )"

svctl stop cnn_server
sleep 1
cp ${DIR}/cnn_server.py $HOME/MP/bin/cnn_server
svctl start cnn_server
