#!/usr/bin/python
# -*- coding: utf-8 -*-

import os, sys, time
import traceback
from common.dblib import DBLib
import common.minutes_sql as SQL

is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
else :
	import configparser as ConfigParser

#import ConfigParser

########COMMON LIB###########
import common.logger as logger

#######Global variable#########
g_var={} 
g_log=None

def stop_docker(docker_name) :
	command="docker stop {}" .format(docker_name)
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("START DOCKER COMPLETE [{}]". format(docker_name))

def start_docker(docker_name) :
	command="docker start {}" .format(docker_name)
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("START DOCKER COMPLETE [{}]". format(docker_name))

def start_cnn_server(docker_name, server_path, server_conf_path, port) :
	# DOCKER's Process 정상 기동시 os.popen에서 대기
	#docker exec -it w2l_num2 /root/wav2letter/build/Server --flagsfile=/DATA/w2l/test/server.cfg --port=15001 --logtostderr=1
	command="""
	docker exec {} {} --flagfile={} --port={} --logtostderr=1
	""" .format(docker_name, server_path, server_conf_path, port)
	g_log.info("START DOCKER's CNN_SERVER \n {}" .format(command))
	process = os.popen(command)
	result=process.read()
	g_log.info(result)
	g_log.info("STOP DOCKER's CNN_SERVER [{}] [{}]". format(docker_name, port))

def load_config():
	global g_log
	g_var['proc_name'] = os.path.basename(sys.argv[0])
	g_var['proc_name'] = g_var['proc_name'].upper()
	g_var['log_level'] = 'debug'
	g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
	conf=ConfigParser.ConfigParser()
	conf.read(g_conf_path)

	items=conf.items(g_var['proc_name'])
	for name, value in items :
		g_var[name]=value
	
	print(g_var)

	g_log = logger.create_logger(os.getenv('HOME') + '/MP/logs', g_var['proc_name'], g_var['log_level'], True)
	g_log.critical("CNN_SERVER Process START!!!!!!!!!!!!!!!!")

	# MySql Class 생성
	mysql = DBLib(g_log)
	# MySql DB 접속
	db_conn = mysql.connect()

	
	if 'docker_name' not in g_var :
		print("[docker_name] value not found")
		return False, None, None
	if 'server_path' not in g_var :
		print("[server_path] value not found")
		return False
	#if 'server_conf_path' not in g_var :
	#	print("[server_conf_path] value not found")
	#	return False, None, None
	if 'server_port' not in g_var :
		print("[server_port] value not found")
		return False, None, None

	return True, mysql

		
def main() :
	ret, mysql=load_config()
	if ret == False :
		print("Load Config Failure [Check mendentory value]")
		return

	
	result, rowcnt, rows, error = SQL.select_minutes_common(mysql)
	if result == False :
		print("Load DB Failure [Check MINUTES_COMMON Table]")
		return 

	if 'MODEL_DIR' in rows[0] and 'MODEL_VERSION' in rows[0] :
		model_path=rows[0]['MODEL_DIR']
		model_version=rows[0]['MODEL_VERSION']
		model_full_path=os.path.join(model_path, model_version)
	elif 'server_conf_path' in g_var :
		model_full_path=g_var['server_conf_path']
	else :
		print("Load Server Config Path Failure [Check MINUTES_COMMON Table or process_info.conf]")
		return 
	
	server_conf_path=os.path.join(model_full_path, 'server.cfg')
	print(server_conf_path)

	stop_docker(g_var['docker_name'])
	start_docker(g_var['docker_name'])
	#START CNN SERVER FUNCTION에서 빠져나오는경우 CNN SERVER가 종료됨
	#start_cnn_server(g_var['docker_name'], g_var['server_path'], g_var['server_conf_path'], g_var['server_port'])
	start_cnn_server(g_var['docker_name'], g_var['server_path'], server_conf_path, g_var['server_port'])

	g_log.critical("CNN_SERVER Process STOP!!!!!!!!!!!!!!!!")

	
if __name__ == '__main__' :
	main()



