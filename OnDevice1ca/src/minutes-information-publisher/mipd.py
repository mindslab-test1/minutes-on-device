#! /usr/bin/python
# -*- coding: utf-8 -*-

#####################################################################################
# PYTHON COMMON
####################################################################################
import os, sys, time, datetime, json, multiprocessing 
import wave, contextlib
import traceback
from multiprocessing import Value, Array
is_py2 = sys.version[0] == '2'
if is_py2 :
	import ConfigParser
	import Queue
else :
	import configparser as ConfigParser
	import queue as Queue

#####################################################################################
# MINDSLAB COMMON
####################################################################################
#import common.simd_config as simd_conf
import common.logger as logger
import common.minds_ipc as IPC
import common.minutes_sql as SQL
import common.lp_trans as lp_trans
from common.dblib import DBLib
from common.def_val import *


#####################################################################################
# INSTALL MODULE
####################################################################################
# IPC Module : https://pypi.org/project/pyzmq
#import zmq

#####################################################################################
# GLOBAL VARIABLE
####################################################################################
#환경변수
g_var = {}
g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'

#로그레벨 공유를 위한 최대 20byte length 문자열
log=None

def ts_to_datetimestr(timestamp):
	local_time = datetime.datetime.fromtimestamp(timestamp)
	return local_time.strftime("%Y%m%d%H%M%S")

def ts_to_datetime(timestamp):
	local_time = datetime.datetime.fromtimestamp(timestamp)
	return local_time

def send_distribute(ipc, msg_type, call_id, speaker=None, speaker_ser=None, vr_speaker_ser=None, speaker_type=None, stt_result=None) :
	try :

		data={}
		data['msg_header']={}
		data['msg_header']['msg_id']='EVENT_COLLECTION_DISTRIBUTE'
		data['msg_body']={}
		data['msg_body']['TYPE']=msg_type
		data['msg_body']['CALL_ID']=call_id
		if speaker :
			data['msg_body']['SPEAKER']=speaker

		if speaker_ser :
		    data['msg_body']['SPEAKER_SER']=speaker_ser
		else :
		    data['msg_body']['SPEAKER_SER']=None

		if vr_speaker_ser :
		    data['msg_body']['VR_SPEAKER_SER']=vr_speaker_ser
		else :
		    data['msg_body']['VR_SPEAKER_SER']=None

		if speaker_type :
		    data['msg_body']['SPEAKER_TYPE']=speaker_type
		else :
		    data['msg_body']['SPEAKER_TYPE']=None

		if stt_result :
			data['msg_body']['STT_RESULT']=stt_result

		json_msg=json.dumps(data,ensure_ascii=False)

		ipc.IPC_Send('midd', json_msg.encode('utf-8'))

	except Exception as e:
		log.critical("thread: exception raise fail. <%s>", e)
		log.critical(traceback.format_exc())
		return False

	return True

def get_replace(mysql) :
	result, rowcnt, rows, error = SQL.select_replace(mysql)
	if result == False :
		log.critical("Select Replace Table FAILURE[{}]".format(error))
		return dict()
	elif rowcnt == 0 :
		log.critical("Replace Table's Data is None")
		return dict()
	#for row in rows :
	#	log.critical("[{}] : [{}] to [{}]" .format(row['MINUTES_ALL_REPLACE_SER'], row['BEFORE_WORD'], row['AFTER_WORD']))
	return rows


def run_worker(worker_q, idx):

	try :
		mysql = DBLib(log)
		# MySql DB 접속
		db_conn = mysql.connect()
	except Exception as e:
		pass


	ipc=IPC.MindsIPCs(log, g_var['proc_name'])
	ret=ipc.IPC_Open(idx)
	if ret == False :
		print("IPC OPEN FAILURE")
		sys.exit(1)

	ret=ipc.IPC_Regi_Process('midd')
	if ret == False :
		print("IPC_REGI_PROCESS FAIL")
		sys.exit(1)

	log.critical("[%d] STARTING.... " %(idx))

	no_msg_cnt=0
	while True:
		try:
			# Check Log Level
			changed_loglevel=logger.watch_logger_changed(log)
			if changed_loglevel :
				log.critical('[THREAD:{}] LogLevel Changed -> [{}]'.format(idx, changed_loglevel))

			# Check IPC Msg
			message = ipc.IPC_Recv()
			if message :
				recv_json_msg = json.loads(message)
			# Check Queue Msg
			else :
				recv_json_msg=worker_q.get(True,1)
				if recv_json_msg :
					pass
					#log.critical(recv_json_msg)
				else :
					if(no_msg_cnt >= 6000) :
						stt_sq = mysql.get_last_auto_increment('STT_RESULT_SER', 'STT_RESULT')
						log.info("THREAD[{}] msg_not_recv[{}] .. last_result_ser=[{}]".format(idx, no_msg_cnt, stt_sq))
						no_msg_cnt = 0
					else :
						no_msg_cnt+=1
						time.sleep(0.01)
					continue
			result, msg_id, msg_body = IPC.get_ipc_msg(recv_json_msg)
			log.critical(msg_body)
			if result == False :
				log.critical("Invalid MSG Recved :: {}" .format(recv_json_msg))
				continue

			json_msg=msg_body


			#if len(call_id_data) >= 2 :
			#	meta_ser=call_id_data[0]
			#	mic_id=call_id_data[1]
			#else :
			#	meta_ser=call_id
			#	mic_id="None"
				
			call_id=json_msg['CALL_ID']
			msg_type=json_msg['TYPE']
			if 'CONTAINER' in json_msg :
				speaker=json_msg['CONTAINER']
			else :
				speaker=None
			if 'SPEAKER' in json_msg :
				speaker=json_msg['SPEAKER']
			else :
				speaker=None

			if 'SPEAKER_SER' in json_msg :
				speaker_ser=json_msg['SPEAKER_SER']
			else :
				speaker_ser=None

			if 'VR_SPEAKER_SER' in json_msg :
				vr_speaker_ser=json_msg['VR_SPEAKER_SER']
			else :
				vr_speaker_ser=None

			if 'SPEAKER_TYPE' in json_msg :
				speaker_type=json_msg['SPEAKER_TYPE']
			else :
				speaker_type=None

			if 'STT_RESULT' in json_msg :
				stt_result=json_msg['STT_RESULT']
			else :
				stt_result=None
			if 'CONTAINER' in json_msg :
				container=json_msg['CONTAINER']
			else :
				container=None


			if msg_type == "#DATA#" :
				meta_ser=call_id

				#start, end, base = conv_stttime_to_timestamp(stt_result['start'], stt_result['end'],stt_meta['START_TIME'])
				stt_result=json_msg['STT_RESULT']

				start=stt_result['start']
				end=stt_result['end']

				result_txt=str(stt_result['txt'])
				result_txt=result_txt.replace('\n','')
				result_txt=result_txt.replace('\r\n','')

				replace_dict=get_replace(mysql)
				for row in replace_dict :
					#log.critical("Check [{}][{}] ".format( type(row['BEFORE_WORD']), type(stt_result)))
					if row['BEFORE_WORD'] in result_txt :
						result_txt=result_txt.replace(row['BEFORE_WORD'], row['AFTER_WORD'])
						log.critical(" word replace [{}] -> [{}]".format(row['BEFORE_WORD'], row['AFTER_WORD']))

				log.critical("[{}] {} : {} ~ {} | {}".format(call_id, speaker, start, end, result_txt))
				result = SQL.insert_stt_result(mysql, meta_ser, speaker_type, speaker_ser, vr_speaker_ser, speaker, result_txt, start, end)

				stt_result['txt']=result_txt

				if result == False :
					log.critical('Insert STT_RESULT FAIL')
				else :
					stt_sq = mysql.get_last_auto_increment('STT_RESULT_SER', 'STT_RESULT')
			else :
				log.critical("[{}] {} : ({}) RECVED ".format(call_id, speaker, msg_type ))

			#send_distribute(ipc, msg_type, call_id, speaker, speaker_ser, vr_speaker_ser, speaker_type, stt_result) 
			send_msg=IPC.MAKE_Event_Collection_Distribute(call_id, msg_type, speaker, speaker_ser, vr_speaker_ser, speaker_type, stt_result, container)

			json_msg=json.dumps(send_msg, ensure_ascii=False)
			ipc.IPC_Send('midd', json_msg.encode('utf-8'))



		except Queue.Empty:
			continue
		except Exception as e:
			log.error("thread: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

	# Terminate Process
	log.critical("[%d] STOPPING.... " %(idx))
	ipc.IPC_Close()
	mysql.disconnect()
	return

#def load_config():
#	global g_var
#	global g_conf_path


#	g_var['record_backup_flag'] = 'false'
#	g_var['proc_name'] = os.path.basename(sys.argv[0])
#	proc_name=g_var['proc_name'].upper()
#	g_conf_path=os.getenv('HOME') +'/MP/etc/process_info.conf'
#	conf=ConfigParser.ConfigParser()
#	conf.read(g_conf_path)
#	items=conf.items(proc_name)
#	for name, value in items :
#		g_var[name]=value

#	#g_log_level.value=g_var['log_level']
#
#	# RECORD
#	for i in g_var :
#		print( '{} = {}'.format(i,g_var[i]))
#
#
#	return

def init_process():
	global log
	global g_var

	try:

		# Init Config
		try:
			proc_name = os.path.basename(sys.argv[0])
			g_var=load_config(proc_name, g_conf_path)
		except Exception as e:
			print("load config fail. <%s>", e)
			print(traceback.format_exc())
			return False, None, None, None

		# Init Logger
		log = logger.create_logger(os.getenv('HOME') + '/MP/logs', proc_name, g_var['log_level'], True, True)
	#	for item in g_var :
	#		log.info('{}={}'.format(item, g_var[item]))
	
		ipc=IPC.MindsIPCs(log, g_var['proc_name'])

		ret=ipc.IPC_Open()
		if ret == False :
			log.critical("IPC OPEN FAILURE")
			return False, None, None, None


		# MultiProcessing을 사용하여 Multi-Thread와 같은 구조로 동작
		queue_list = list()
		worker_list = list()
		worker_cnt = int(g_var['thr_cnt'])
		for idx in range(worker_cnt):
			q = multiprocessing.Queue()
			p = multiprocessing.Process(target=run_worker, args=(q, idx))
			p.daemon = True
			p.start()
			queue_list.append(q)
			worker_list.append(p)

	except Exception as e:
		#log.error("main: exception raise fail. <%s>", e)
		print("main: exception raise fail. <%s>", e)
		#log.error(traceback.format_exc())
		print(traceback.format_exc())
		return False, None, None, None
	else:
		#return mysql, pull_zmq, queue_list, worker_list
		return True, queue_list, worker_list, ipc

def proc_chg_log(json_msg) :
	log.critical("main: CHG_LOG_LEVEL [{}]".format(json_msg))

	#conf=ConfigParser.ConfigParser()
	#conf.read(g_conf_path)

	#proc_name=g_var['proc_name'].upper()
	#items=conf.items(proc_name)
	#for name, value in items :
	#	if name == 'log_level' :
	#		g_var['log_level']=value
	g_var['log_level'] = get_log_level(g_var['proc_name'], g_conf_path)
	logger.change_logger_level(log, g_var['log_level'])

def main():
	# Init Process
	#mysql, pull_zmq, queue_list, worker_list = init_process()
	result, queue_list, worker_list, ipc = init_process()
	if result == False :
		log.critical("init Process Failure")
		return 

	queue_idx = 0

	# Start MainLoop
	while True:
		try:
			#message = pull_zmq.recv()
			message = ipc.IPC_Recv()
			if message :
				json_msg = json.loads(message)
				result, msg_id, msg_body = IPC.get_ipc_msg(json_msg)
				if result == False :
					log.critical("Invalid MSG Recved :: {}" .format(json_msg))
					continue
			else :
				time.sleep(1/10000.0)
				continue

			if msg_id == IPC.EVENT_COLLECTION_PUBLISH :
				queue_list[queue_idx].put(json_msg)
				queue_idx = (queue_idx + 1)%int(g_var['thr_cnt'])

			elif msg_id == 'CHG_LOG_LEVEL' :
				proc_chg_log(json_msg)
			else :
				log.error("main: Unknown JSON MSG recved [{}]".format(json_msg))

		except Exception as e:
			log.error("main: exception raise fail. <%s>", e)
			log.error(traceback.format_exc())

	log.critical("main stooped...")
	
	# Terminate Process
	for p in worker_list:
		p.join()
	#pull_zmq.close()
	ipc.IPC_Close()
	#mysql.disconnect()
	log.critical('*%30s : %s -> %d threads', "STOPPED", g_var['proc_name'], g_var['thr_cnt'])

	return 

if __name__ == '__main__':
	main()
