import os
import time
import logging

logger = logging.getLogger('dvectorize')


def set_logger(log_dir, level, device):
    os.environ['TZ'] = 'Asia/Seoul'
    try:
        time.tzset()
    except AttributeError as e:
        print(e)
        print("Skipping timezone setting.")
    os.makedirs(log_dir, exist_ok=True)
    log_path = os.path.join(log_dir, '{}_{}_server.log'.format(device, time.strftime("%Y%m%d_%H%M%S")))
    logger = custom_logger(name='dvectorize', type_='file', fn=log_path)
    logger.setLevel(getattr(logging, level))
    return logger


def custom_logger(name, type_='stream', fn=None):
    fmt = '[{}|%(levelname)s|%(filename)s:%(lineno)s] %(asctime)s >>> %(message)s'.format(name)
    fmt_date = '%Y-%m-%d_%T %Z'

    if type_ == 'stream':
        handler = logging.StreamHandler()
    elif type_ == 'file':
        handler = logging.FileHandler(filename=fn, encoding='utf-8')
    else:
        logging.critical("Undefined logging type!: {}".format(type_))
        logging.info("Only 'stream', 'file' types are available.")
        exit()

    formatter = logging.Formatter(fmt, fmt_date)
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)
    return logger


def log_error(func):
    def wrapper(*args, **kwargs):
        try:
            logger.debug('start %s', func)
            return func(*args, **kwargs)
        except Exception as e:
            logger.exception(e)
            raise e
        finally:
            logger.debug('end %s', func)
    return wrapper
